// pages/wedgit/picker/picker.js

const init = function (page) {

  if (page) {

    page.showPicker = function (data) {

      data = data || {};
      data['picker.show'] = true;
      this.setData(data);

    };

    page.hidePicker = function (data) {

      data = data || {};
      data['picker.show'] = false;
      this.setData(data);

    };

    page.bindHidePicker = function () {

      this.hidePicker();

    };

    page.bindIdleHandler = function () {

    };

    // 选择
    page.bindPickerItemTap = function (e) {

      let data = e.currentTarget.dataset, picker = this.data.picker, tab = picker.child[data.index], item = tab.child[data.sub];

      tab.name = item.name;

      if (!item['_child_'] || item['_child_'].length == 0) {

        tab.index = data.sub;
        this.hidePicker({ picker: picker });
        this.pickerChangedHandler && this.pickerChangedHandler(item);

      } else if (data.index == picker.child.length - 1) {

        tab.index = data.sub;
        picker.index = data.index;
        picker.child.push({ name: '请选择', id: '', child: item['_child_'] || [], index: -1 });
        this.setData({ picker: picker });

      } else if (data.sub != tab.index) {

        tab.index = data.sub;
        picker.child[data.index + 1].child = item['_child_'];
        picker.child[data.index + 1].index = -1;
        this.setData({ picker: picker });

      }

    };

    // head
    page.bindPickerHeadTap = function (e) {

      let index = e.currentTarget.dataset.index, picker = this.data.picker, tab = picker.child[data.index], len = picker.child.length;

      if (picker.child.length < index + 3) {

        this.setData({ 'picker.index': index });

      } else if (picker.index != index) {

        picker.index = index;
        picker.child[len - 1].child = tab.child[tab.index]['_child_'];
        picker.child.splice(index + 1, picker.child.length - 2 - index);
        this.setData({ picker: picker });

      }

    }

  }

};

module.exports = {
  init: init
}