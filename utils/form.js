// form.js
/**
 * 表单
 *  { type: 'contact', value: '', contacts:[], group:'student_teacher_office_family_all' },
 *  { type: 'contacts', value: [], contacts:[], group:'student_teacher_office_family_all' },
 *  { type: 'image', value: [], files:[] },
 *  { type: 'images', value: [], files:[] },
 *  { type: 'files', value: [], files:[] },
 *  { type: 'select', value: '', options:[], selected:-1 },
 *  { type: 'rating', value: 0, stars: [0, 1, 2, 3, 4], multiplier:1 },
 *  { type: 'tags', value: [], tags: [] },
 *  { type: 'datetime', value: '', date:'', time:'' },
 *
 *  group分组
 *  { start: start index, end: end index, title: group title }
 */

let app = getApp()

let pageUtils = ({
  data: {

    formData: [],
    popSel: {}

  },

  // 保存 提交事件
  submitHandler: null,
  // 默认 弹出确认框
  needShowCmfModal: true,

  //页面加载
  onLoad: function (options) {

    app.page.onLoadHandler(this)
    this.init(options)

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () { },

  // 初始化
  init: function (options) {

    var formType = options && options.type || ''

    // page初始化
    app.page.onInitHandler(this)
    this.setData({
      options: options || {},
      formStyle: 'normal', submitFixed: true, submitText: '保存',
      popSel: { hideButtons: true, index: 0, cur: 0, values: [] }
    })
    // 初始化筛选框 - 需要改成picker
    app.page.initPopSelect(this)
    // form obj
    let formObj = {
      'article': this.initArticleForm,     // 班级圈
      'notice': this.initNoticeForm,    // 通知
      'rating': this.initRatingForm,      // 评一评
      'opinion': this.initOpinionForm,      // 评一评
      'parentsNote': this.initParentsNoteForm,    // 随手记
      'timetable': this.initTmTableForm,  // 调课
      'vacation': this.initVacationForm,  // 请假
      'editDayoff': this.initEditDayoff,  // 请假-修改
      'mailbox': this.initMailboxForm,    // 邮箱
      'message': this.initMessageForm,    // 写私信
      'feedback': this.initFeedbackForm,  // 意见反馈
      'editInfos': this.initInfosForm,    // 修改个人信息
      'schoolBased': this.initBasedClassForm,   // 兴趣班，校本课程班
      'addFamily': this.initFamilyPeople, // 新增家庭成员
    }

    formObj[formType] && formObj[formType]()

  },

  // 获取说明
  bindGetDescription: function (e) {

    let t = e.currentTarget.dataset.t
    app.navigateTo('/pages/utility/richText/richText', { src: 'description', id: t })

  },

  // 返回并刷新
  backWithFresh: function () {

    let options = this.data.options || {}, cb = options.cb || '';
    (cb == 'back') ? app.back() : app.backWithFresh();

  },

  // 错误返回
  backWithError: function (msg) {

    app.showInfoModal('', msg || '数据加载失败!', function () {
      app.back()
    })

  },

  // 远程图片解析->form
  updateFormImages: function (name, urls) {

    let formData = this.data.formData, idx = app.http.form.getFormItemIndex(formData, name),
      formItem = (idx >= 0) ? formData[idx] : null, files = [], length = app.http.getWebUrl().length,
      str = 'formData[' + idx + '].files', param = {}

    if (formItem) {

      urls.forEach(function (url) {

        // value 为相对地址
        files.push({ url: url, value: url.substr(length) })

      })

      param[str] = files
      this.setData(param)

    }

  },

  // 远程文件解析->form
  updateFormAttachments: function (name, urls) {

    let formData = this.data.formData, idx = app.http.form.getFormItemIndex(formData, name),
      formItem = (idx >= 0) ? formData[idx] : null, length = app.http.getWebUrl().length,
      files = [], value = []

    if (formItem) {
      for (var i in urls) {
        let url = urls[i], value = url.subStr(length)
        files.push({ name: wx.util.lastPathComponent(value), ext: wx.util.fileExtension(value) })
        value.push(value)
      }
      var str = 'formData[' + idx + '].files', param = {}
      param[str] = files
      str = 'formData[' + idx + '].value'
      param[str] = value
      this.setData(param)
    }

  },

  //// 班级圈 ////////////////////////////////////////////
  initArticleForm: function () {

    let options = this.data.options || {},
      formData = [
        { title: '内容', name: 'content', type: 'editor', value: '', hideTitle: true, empty: 0 },
        { title: '图片', name: 'images', type: 'images', value: [], files: [], hideTitle: true, empty: 1 }
      ]

    wx.wrap.setNavigationBarTitle('发班级圈')
    let imgIdx = app.http.form.getFormItemIndex(formData, 'images')
    if (options.img && options.img.length > 0 && imgIdx >= 0) {
      formData[imgIdx].files.push({ url: decodeURI(options.img), value: '' })
    }

    this.setData({ formData: formData, submitFixed: false, submitText: '发布' })
    // 提交事件
    this.submitHandler = this.addArticle

  },

  // 发布班级圈
  addArticle: function (formData, e) {

    let that = this
    app.http.ajxAddArticle(formData, e, function () {
      that.backWithFresh()
    })

  },

  //// 校本课程班 /////////////////////////////////////
  initBasedClassForm: function () {

    let that = this
    wx.wrap.setNavigationBarTitle('校本课程申请')
    app.showLoading(true, that)
    app.http.ajxGetBaseClassApplyAuthority(function (res) {

      that.getBasedClassTypeList()

    }, function (res) { // error = true
      res && res.message && app.showInfoModal('', res.message, function () {
        app.back()
      })
    }, function () {
      app.showLoading(false, that)
    })

  },

  //// 新增校本课程申请 ////////////////////////////////
  addBasedClass: function (formData, e) {
    let that = this
    app.http.ajxAddBaseClass(formData, e, function () {
      that.backWithFresh()
    })
  },

  //// 加载校本课程选项并展示form ////////////////////////
  getBasedClassTypeList: function () {

    let that = this, page = wx.util.getLastPageOf(1),
      baseClassObj = {
        id: 0, name: '', typeid: '',
        tpid1: { value: '', contacts: [] },
        grades: '', limitNumber: '',
        introduction: '', teacherProfile: ''
      }
    // 获取校本课程类型列表
    app.showLoading(true, this)
    app.http.ajxGetBasedClassTypeList(null, function (res) {

      if (res && res.data) {
        let typeAry = []
        for (var i in res.data) {
          typeAry.push({ id: res.data[i].id, name: res.data[i].name })
        }
        if (that.options.modify && page) {
          baseClassObj = page.getBaseClassObj(that.options.index)
        }
        baseClassObj.typeAry = typeAry

        app.http.getGradeListWithCached(function (datas) {

          if (datas && datas.length > 0) {
            that.initBasedClassFormSetValue(baseClassObj, datas)
          } else {
            app.showInfoModal('', '没有设置年级')
          }

        }, function (res) {
          app.showInfoModal('', res && res.message || '年级列表加载失败！')
        })

      } else {
        app.toastErr("没有设定校本课程的班级类别列表")
      }

    }, null, function () {
      app.showLoading(false, that)
    })

  },

  initBasedClassFormSetValue: function (baseClassObj, gradesOptions) {

    // 展示校本课程的form
    let options = this.data.options || {},
      gradesValue = this.getBaseClassGrades(gradesOptions, baseClassObj.grades),
      info = gradesValue.length == 0 ? '' : gradesValue.length == gradesOptions.length ? '全校' : (gradesValue.length + '个年级'),

      formData = [
        { title: '', name: 'id', type: 'hidden', value: baseClassObj.id, empty: 0 },
        { title: '课程名称', name: 'name', type: 'text', value: baseClassObj.name, hideTitle: true, showInfo: true, infoType: 2, empty: 0 },
        { title: '类别', name: 'typeid', type: 'select', value: baseClassObj.typeid, options: baseClassObj.typeAry, selected: this.getBaseClassTypeIndex(baseClassObj.typeAry, baseClassObj.typeid), empty: 0 },
        { title: '合作教师', name: 'tpid1', type: 'contact', value: baseClassObj.tpid1.value, contacts: baseClassObj.tpid1.contacts, group: 'teacher_office', empty: 1 },
        { title: '指定年级', name: 'grades', type: 'checkbox', value: gradesValue, list: 1, options: gradesOptions, empty: 0 },
        // { title: '指定年级', name: 'grades', type: 'grades', value: gradesValue, info: info, empty: 0 },
        { title: '限定人数', name: 'limitNumber', type: 'number', value: baseClassObj.limitNumber, empty: 0 },
        { title: '课程简介', name: 'introduction', type: 'editor', value: baseClassObj.introduction, hideTitle: true, empty: 1 },
        { title: '教师简介', name: 'teacherProfile', type: 'editor', value: baseClassObj.teacherProfile, hideTitle: true, empty: 1 }
      ]

    if (this.options.modify) {
      formData.splice(6, 0, { title: '班级限定人数', name: 'limitClassNumber', type: 'number', value: baseClassObj.limitClassNumber || '', empty: 0 })
    }

    this.setData({ formData: formData, submitText: '提交申请' })
    this.submitHandler = this.addBasedClass

  },

  // 获取类型索引
  getBaseClassTypeIndex: function (typeAry, value) {

    var index = -1

    for (var i in typeAry) {
      if (typeAry[i].id == value) {
        index = i
        break
      }
    }

    return index

  },
  // 获取年级列表
  getBaseClassGrades: function (options, value) {

    let ids = []

    for (var i in options) {

      let opt = options[i]
      if (wx.util.isContained(value, opt.value) || wx.util.isContained(value, opt.name)) {
        ids.push(opt.value)
        opt.checked = true
      }

    }

    return ids

  },

  //// 通知 ////////////////////////////////////////////
  initNoticeForm: function () {

    let that = this, options = this.data.options || {}, level = options.level || 0

    if (app.isSchoolNotice(level)) {  // 校级

      app.http.getGradeListWithCached(function (datas) {

        if (datas && datas.length > 0) {
          that.initNoticeFormWith(datas, options)
        } else {
          app.showInfoModal('', '没有设置年级')
        }

      }, function (res) {
        app.showInfoModal('', res && res.message || '年级列表加载失败！')
      })

    } else {

      that.initNoticeFormWith(null, options)

    }

  },

  // 初始化
  initNoticeFormWith: function (grades, options) {

    let that = this, level = options.level || 0, formData = []

    formData.push({ title: '通知标题', name: 'title', type: 'text', value: '', hideTitle: true, empty: 0 })
    formData.push({ title: '是否紧急通知', name: 'important', type: 'radio', list: false, value: 0, options: [{ value: 0, name: '否' }, { value: 1, name: '是' }], empty: 1 })

    // 校级通知，可以发送短信
    if (app.isSchoolNotice(level)) {
      formData.push({ title: '是否需要短信通知', name: 'issendmsg', type: 'radio', list: false, value: 0, options: [{ value: 0, name: '否' }, { value: 1, name: '是' }], empty: 1 })
      formData.push({ title: '短信通知对象', name: 'senduser', type: 'hidden', value: 0, options: [{ value: 0, name: '学生、家长' }, { value: 1, name: '教师' }, { value: 2, name: '全部' }], empty: 1 })
      formData.push({ title: '发送范围', name: 'grade', type: 'grades', value: wx.util.objectsValue(grades, 'id'), info: '全校', empty: 0 })
    }
    else {
      formData.push({ title: '', name: 'cid', type: 'hidden', value: app.userData.getClassId(), empty: 1 })
    }
    formData.push({ title: '通知内容', name: 'content', type: 'editor', value: '', hideTitle: true, empty: 0 })
    formData.push({ title: '图片', name: 'images', type: 'images', value: [], files: [], hideTitle: true, empty: 1 })
    // formData.push({ title: '附件', name: 'attachments', type: 'files', value: [], files: [], hideTitle: true, empty: 1 })
    formData.push({ title: '通知类型', name: 'type', type: 'hidden', value: level, empty: 1 })

    wx.wrap.setNavigationBarTitle('发布通知')
    this.setData({ formData: formData, submitText: '发布' })
    // 提交事件
    this.submitHandler = this.addNotice

  },

  // 发布通知
  addNotice: function (formData, e) {

    let that = this
    app.http.ajxAddNotice(formData, e, function (res) {

      that.backWithFresh()

    })

  },

  // 通知 radio items changed
  noticeRadioChanged: function (idx) {

    let formData = this.data.formData, sendmsgIdx = app.http.form.getFormItemIndex(formData, 'issendmsg')

    if (idx == sendmsgIdx) { // 是否发生短信

      let senduserIdx = app.http.form.getFormItemIndex(formData, 'senduser'), sendmsg = formData[idx].value,
        str = 'formData[' + senduserIdx + '].type', param = {}

      param[str] = (sendmsg > 0) ? 'radio' : 'hidden'
      this.setData(param)

    }

  },

  //// 作业 ////////////////////////////////////////////
  initHmworkForm: function () {

    let that = this, options = this.data.options || {}, id = options.id || 0, date = new Date(),
      today = wx.util.dateFormat(date, 'Y-M-D'), end = wx.util.dateFormat(wx.util.addDate(date, 15), 'Y-M-D'),
      hmwork = {
        id: id, title: '', course: '', courseList: {}, classid: '', date: today, content: '', images: [], isArticle: 0, start: today, end: end
      }

    wx.wrap.setNavigationBarTitle('发布作业')

    if (id) {

      this.loadHmworkDetail(id, function (data) {

        hmwork.title = data.title || ''
        hmwork.course = data.course || ''
        hmwork.courseList[data.course] = [{ cid: data.cid || 0, className: data['cid_name'] || '' }]
        hmwork.classid = data.cid || 0
        hmwork.date = data.date || ''
        hmwork.content = data.content || ''
        hmwork.isArticle = data.isArticle || 0
        data['images-source'] && (hmwork.images = app.bdp.parseFiles(data['images-source']))
        data.intime && (hmwork.start = wx.util.dateFormat(new Date(wx.util.getDateTimeStamp(data.intime)), 'Y-M-D'))

        that.initHmworkFormWith(hmwork)

      })

    } else {

      this.loadCourceAndClassList(function (data) {

        hmwork.courseList = data
        that.initHmworkFormWith(hmwork)

      })

    }

  },

  // 初始化作业表单
  initHmworkFormWith: function (hmwork) {

    let that = this, courseTags = this.parseCourseList(hmwork.courseList), courseValue = courseTags.length > 0 ? courseTags[0].title : '',

      formData = [
        { title: '标题', name: 'title', type: 'hidden', value: hmwork.title, empty: 0 },
        { title: '科目', name: 'course', type: 'tag', value: courseValue, tags: courseTags, courseList: hmwork.courseList, editable: false, theme: this.data.theme || 'green', empty: 0 },
        { title: '班级', name: 'classid', type: hmwork.id ? 'tag' : 'tags', value: hmwork.classid, tags: [], editable: false, theme: this.data.theme || 'green', empty: 0 },
        { title: '日期', name: 'date', type: 'date', value: hmwork.date, start: hmwork.start, end: hmwork.end, empty: 0 },
        { title: '作业内容', name: 'content', type: 'editor', value: hmwork.content, hideTitle: true, empty: 0 },
        { title: '图片', name: 'images', type: 'images', value: [], files: [], hideTitle: true, empty: 1 },
      ]

    if (hmwork.id) { // 修改
      formData.push({ name: 'isArticle', type: 'hidden', value: hmwork.isArticle, empty: 1 })
      formData.push({ name: 'homeworkid', type: 'hidden', value: hmwork.id, empty: 1 })
      formData.push({ name: 'action', type: 'hidden', value: 'editHomework', empty: 1 })

    } else {
      formData.push({ title: '同步到班级圈', name: 'isArticle', type: 'switch', value: hmwork.isArticle, empty: 1 })
      formData.push({ name: 'action', type: 'hidden', value: 'addHomework', empty: 1 })
    }

    this.setData({ formData: formData, submitText: hmwork.id ? '保存' : '发布' })

    if (hmwork.images.length > 0) {
      that.updateFormImages('images', hmwork.images)
    }

    this.hmworkCourseChanged(1, formData[1].value)
    this.submitHandler = this.addHmwork

  },

  // 发布作业
  addHmwork: function (formData, e) {

    let that = this

    app.http.ajxAddHomework(formData, e, function () {
      that.backWithFresh()
    })

  },

  // 获取科目列表
  parseCourseList: function (data) {

    let list = []
    for (var i in data) {
      list.push({ id: 0, title: i, selected: false })
    }
    (list.length > 0) && (list[0].selected = true)

    return list

  },

  // 更新班级列表
  hmworkCourseChanged: function (courseIdx, value) {

    let that = this, formData = this.data.formData, titleIdx = app.http.form.getFormItemIndex(formData, 'title'), classIdx = app.http.form.getFormItemIndex(formData, 'classid'),
      str = 'formData[' + classIdx + ']', param = {};

    (titleIdx >= 0) && (formData[titleIdx].value = value);
    (classIdx >= 0) && (param[str] = this.getCourseChangedClassItem(courseIdx, classIdx, value));

    this.setData(param)

  },

  // 科目变更
  getCourseChangedClassItem: function (courseIdx, classIdx, course) {

    let formData = this.data.formData, classList = formData[courseIdx].courseList[course] || [], tags = [], formItem = formData[classIdx]

    classList.forEach(function (item) {
      tags.push({ id: item.cid, title: app.bdp.removeClassYear(item.className), selected: false })
    })

    if (tags.length > 0) {
      tags[0].selected = true
    }

    formItem.value = tags.length > 0 ? tags[0].id : ''
    formItem.tags = tags
    return formItem

  },

  // 作业详情
  loadHmworkDetail: function (id, cb) {

    let that = this

    app.showLoading(true, that)
    app.http.ajxGetHomeworkData(id, function (res) {

      if (res && res.data) {
        typeof cb == "function" && cb(res.data)
      } else {
        that.backWithError('作业加载失败！')
      }

    }, function (res) {

      that.backWithError(res && res.message || '作业加载失败！')

    }, function () {
      app.showLoading(false, that)
    })

  },

  // 加载科目班级列表
  loadCourceAndClassList: function (cb) {

    let that = this, list = []

    app.showLoading(true, that)
    app.http.ajxGetCourceAndClassList(function (res) {

      if (res && res.data) {
        typeof cb == "function" && cb(res.data)
      } else {
        that.backWithError('科目列表获取失败！')
      }

    }, function (res) {
      that.backWithError(res && res.message || '科目列表获取失败！')
    }, function () {
      app.showLoading(false, that)
    })

  },

  //// 成绩 ////////////////////////////////////////////
  initExamForm: function () {

    let that = this, options = this.data.options || {}, id = options.id || 0, today = wx.util.dateFormat(new Date(), 'Y-M-D'),
      exam = {
        id: id, title: '', type: 1,
        conid: '',
        typeList: app.config.examOptions(),
        starttime: today, courseName: '', courseList: [], cid: '', course: []
      }

    wx.wrap.setNavigationBarTitle('发布成绩')

    if (id) {

      let lastPage = wx.util.getLastPageOf(1) // 获取成绩详情
      if (lastPage && lastPage.getExamDetail) {

        let data = lastPage.getExamDetail() || {}
        exam.title = data.title || ''
        exam.type = data.type || 1
        exam.courseName = data.course || ''
        exam.cid = data.cid || 0
        exam.conid = data.conid || ''
        exam.courseList[data.course] = [{ cid: data.cid || 0, className: data['className'] || '' }]
        data.starttime && (exam.starttime = wx.util.getDateString(data.starttime))
        // 更新选中状态
        exam.typeList.forEach(function (item) {
          item.selected = (item.id == exam.type)
        })

      }

      this.loadExamScore(id, function (score) {
        exam.course = score
        that.initExamFormWith(exam)
      })

    } else {

      this.loadCourceAndClassList(function (data) {

        exam.courseList = data
        that.initExamFormWith(exam)

      })

    }

  },

  // 初始化成绩表单
  initExamFormWith: function (exam) {

    let that = this, courseTags = this.parseCourseList(exam.courseList), courseValue = courseTags.length > 0 ? courseTags[0].title : '',

      formData = [
        { title: '考试题目', name: 'title', type: 'text', value: exam.title, hideTitle: true, empty: 0 },
        { title: '考试类型', name: 'type', type: 'tag', value: exam.type, tags: exam.typeList, theme: this.data.theme || 'green', empty: 0 },
        { title: '考试时间', name: 'starttime', type: 'date', value: exam.starttime, empty: 0 },
        { title: '科目', name: 'courseName', type: 'tag', value: courseValue, tags: courseTags, courseList: exam.courseList, editable: false, theme: this.data.theme || 'green', empty: 0 },
        { title: '班级', name: 'cid', type: 'tag', value: exam.cid, tags: [], editable: false, theme: this.data.theme || 'green', empty: 0 },
        { title: '成绩评分制', name: 'conid', type: 'select', value: exam.conid, options: [], selected: -1, style: 'radio', empty: 0 },
        { title: '成绩录入', name: 'course', type: 'exam', value: exam.course, max: 100, statistic: [], empty: 0 }
      ]

    if (exam.id) {
      formData.push({ name: 'examid', type: 'hidden', value: exam.id, empty: 1 })
    }

    this.setData({ formData: formData, submitText: exam.id ? '保存' : '发布' })
    this.updateExamStatics()
    if (exam.cid) {
      this.loadExamConfig(exam.cid)
    }
    this.examCourseChanged(3, formData[3].value)
    this.submitHandler = this.addExam

  },

  // 发布成绩
  addExam: function (formData, e) {

    let that = this, idx = app.http.form.getFormItemIndex(formData, 'course'), formItem = (idx >= 0) ? formData[idx] : null

    if (!formItem) {

      app.showInfoModal('错误', '没有找到科目！');

    } else if (formItem.value.length == 0 || formItem.value[0].scores.length == 0) {

      app.showInfoModal('错误', '成绩录入人数不能为空!');

    } else if (formItem.value[0].scores.length != formItem.statistic[0]) {

      app.showCfmModal('提示', '学生成绩未全部录入,确定提交吗？', function () {

        app.http.ajxAddExamScore(formData, e, function () {

          that.backWithFresh();

        });

      })

    } else {

      app.http.ajxAddExamScore(formData, e, function () {

        that.backWithFresh();

      });

    }

  },

  // 加载成绩配置
  loadExamConfigIfNeeded: function (cid, cb) {

    let that = this

    if (!this.examConfig) {
      this.examConfig = {}
    }

    if (this.examConfig[cid]) {

      typeof cb == "function" && cb(this.examConfig[cid])

    } else {

      app.showLoading(true, that)
      app.http.ajxGetExamConfig(cid || '', function (res) {

        if (res && res.data) {
          that.examConfig[cid] = res.data
          typeof cb == "function" && cb(that.examConfig[cid])
        }

      }, function (res) {
        res && res.message && app.toastErr(res && res.message)
        typeof cb == "function" && cb()
      }, function () {
        app.showLoading(false, that)
      })

    }

  },

  // 加载配置
  loadExamConfig: function (cid) {

    let that = this, values = []

    this.loadExamConfigIfNeeded(cid, function (data) {

      if (data) {

        data.forEach(function (item) {

          let infos = []

          item.infoList.forEach(function (info, i) {

            let name = info['level'] + ': '

            if (info['start_score'] == info['end_score']) {
              name += '分数 = ' + info['start_score'] + '分'
            } else {
              info['start_score'] && (name += info['start_score'] + '分 ≤ ')
              name += '分数'
              name += (i == 0 && info['end_score'] == info['max_score'] ? ' ≤ ' : ' < ') + info['end_score'] + '分'
            }

            infos.push(name)

          })
          values.push({ id: item.id, name: item.name, infos: infos, max: item['max_score'] || 100 })

        })

      }

      that.setExamConfig(values)

    })

  },

  // 设置成绩配置
  setExamConfig: function (values) {

    let that = this, formData = this.data.formData, conIdx = app.http.form.getFormItemIndex(formData, 'conid'),
      opt = wx.util.getOptionBy('id', formData[conIdx].value, values),
      str = 'formData[' + conIdx + ']', param = {}

    console.log(opt)
    param[str + '.value'] = opt.index >= 0 ? formData[conIdx].value : ''
    param[str + '.options'] = values || []
    param[str + '.selected'] = opt.index;
    (opt.index >= 0) && wx.util.extend(param, this.updateExamMaxScore(values[opt.index].max || 100))
    this.setData(param)

  },

  // 配置变更
  examConfigChanged: function (idx) {

    let that = this, formItem = this.data.formData[idx], sel = formItem.selected, item = formItem.options[sel]
    this.updateExamMaxScore(item.max || 100, true)

  },

  // 更新班级列表
  examCourseChanged: function (courseIdx, value) {

    let that = this, formData = this.data.formData, classIdx = app.http.form.getFormItemIndex(formData, 'cid'),
      str = 'formData[' + classIdx + ']', param = {}

    if (classIdx >= 0) {

      param[str] = this.getCourseChangedClassItem(courseIdx, classIdx, value)
      this.setData(param)
      this.examClassChanged(classIdx, formData[classIdx].value)

    }

  },

  // 班级变更
  examClassChanged: function (classIdx, value) {

    let that = this

    value && this.loadExamConfig(value)

  },

  // 加载成绩
  loadExamScore: function (id, cb) {

    let that = this

    app.showLoading(true, that)
    app.http.ajxGetExamScore(id, function (res) {

      if (res && res.data) {
        typeof cb == "function" && cb(res.data)
      } else {
        that.backWithError('成绩加载失败！')
      }

    }, function (res) {
      that.backWithError(res && res.message || '成绩加载失败！')
    }, function () {
      app.showLoading(false, that)
    })

  },

  // 更新统计值
  updateExamStatics: function () {

    let formData = this.data.formData, idx = app.http.form.getFormItemIndex(this.data.formData, 'course'), courses = (idx >= 0) ? formData[idx].value : '', statistic = [],
      str = 'formData[' + idx + '].statistic', param = {}

    if (idx >= 0) {

      courses.forEach(function (course) {
        var count = 0
        course.scores.forEach(function (score) {
          (score.status > 0) && (count++)
        })
        statistic.push(count)

      })

      param[str] = statistic
      this.setData(param)
    }

  },

  // 更新最大分数
  updateExamMaxScore: function (max, render) {

    let formData = this.data.formData, idx = app.http.form.getFormItemIndex(this.data.formData, 'course'), formItem = (idx >= 0) ? formData[idx] : '',
      str = 'formData[' + idx + ']', param = {}

    if (formItem) {
      param[str + '.max'] = max || 100
    }
    render && this.setData(param)
    return param

  },

  // 成绩录入
  inputScores: function (e) {

    let idx = e.currentTarget.dataset.idx, formData = this.data.formData, cIdx = app.http.form.getFormItemIndex(formData, 'cid'),
      formItem = (cIdx >= 0) ? formData[cIdx] : null

    if (formItem) {
      app.navigateTo('./score?src=exam&cid=' + formItem.value + '&index=' + idx + '&max=' + formData[idx].max || 100)
    }

  },

  // 读取成绩
  getScores: function (index) {

    let formItem = this.data.formData[index]
    return formItem.value.length > 0 ? (formItem.value[0].scores || []) : []

  },
  // 保存成绩
  setScores: function (index, scores) {

    let formData = this.data.formData, formItem = formData[index], str = 'formData[' + index + ']', param = {},
      cIdx = app.http.form.getFormItemIndex(formData, 'cid'), courseIdx = app.http.form.getFormItemIndex(formData, 'courseName'), exam = {}

    exam.scores = scores || []
    if (cIdx >= 0) {
      exam.cid = formData[cIdx].value
    }
    if (courseIdx >= 0) {
      exam.name = formData[courseIdx].value
    }
    formItem.value[0] = exam
    param[str] = formItem
    this.setData(param)

    this.updateExamStatics()

  },

  //// 班级 ////////////////////////////////////////////
  initClassForm: function () {

    let that = this

    app.http.getGradeListWithCached(function (datas) {

      if (datas && datas.length > 0) {
        that.initClassFormWith(datas, options)
      } else {
        app.showInfoModal('', '没有设置年级')
      }

    }, function (res) {
      app.showInfoModal('', res && res.message || '年级列表加载失败！')
    })

  },

  initClassFormWith: function (gradeOptions, options) {

    let classOptions = app.config.classOptions(),
      formData = [
        {
          title: '班级类型', name: 'type', type: 'radio', list: false, value: app.E_CLASS_TYPE.NORMAL, options: [
            { value: app.E_CLASS_TYPE.NORMAL, name: '普通班' },
            { value: app.E_CLASS_TYPE.TEMPL, name: '临时班' }
          ], empty: 1
        },
        { title: '班级名称', name: 'name', type: 'hidden', value: '', empty: 1 },
        { title: '年级', name: 'grade', type: 'select', value: gradeOptions[0].id, options: gradeOptions, selected: 0, empty: 0 },
        { title: '班级', name: 'class', type: 'select', value: classOptions[0].id, options: classOptions, selected: 0, empty: 0 },
      ]

    wx.wrap.setNavigationBarTitle('班级')
    this.setData({ formData: formData, submitText: '保存' })
    // 提交事件
    this.submitHandler = this.addClass

  },

  // 添加班级
  addClass: function (formData, e) {

    let that = this
    app.http.ajxAddClass(formData, e, function () {
      that.backWithFresh()
    })

  },

  // 班级类型修改
  classTypeChanged: function (idx) {

    let formData = this.data.formData, value = formData[idx].value,
      nameIdx = app.http.form.getFormItemIndex(formData, 'name'),
      gradeIdx = app.http.form.getFormItemIndex(formData, 'grade'),
      classIdx = app.http.form.getFormItemIndex(formData, 'class')

    var formItem = null, str = '', param = {}

    if (nameIdx >= 0) {

      formItem = formData[nameIdx]
      if (value == app.E_CLASS_TYPE.NORMAL) { // 普通班
        formItem.type = 'hidden', formItem.value = '', formItem.empty = 1
      } else {  // 临时班
        formItem.type = 'text', formItem.value = '', formItem.empty = 0
      }
      str = 'formData[' + nameIdx + ']'
      param[str] = formItem
    }

    if (gradeIdx >= 0) {
      str = 'formData[' + gradeIdx + '].type'
      param[str] = (value == app.E_CLASS_TYPE.NORMAL) ? 'select' : 'hidden'
    }

    if (classIdx >= 0) {
      str = 'formData[' + classIdx + '].type'
      param[str] = (value == app.E_CLASS_TYPE.NORMAL) ? 'select' : 'hidden'
    }

    if (nameIdx >= 0 || gradeIdx >= 0 || classIdx >= 0) {
      this.setData(param)
    }

  },

  //// 老师 ////////////////////////////////////////////
  initTeacherForm: function () {

    let that = this, options = this.data.options || {}, cid = options.cid || 0, cuid = options.cuid || 0,
      info = {
        cid: cid, cuid: cuid, type: 1, name: '', teacherNo: '', no: '', phone: '', level: '', levelList: [], job: [], jobList: []
      }

    wx.wrap.setNavigationBarTitle(cuid ? '修改教师信息' : '添加教师')

    // 加载职务、课程信息
    that.loadClassMetaData(info, function () {

      if (cuid) {

        // 加载详情
        that.getClassUserDetail(cuid, function (data) {

          wx.util.extend(info, data, ['name', 'no', 'teacherNo', 'phone', 'level', 'job'])
          that.updateClassUserJob(info)
          that.initTeacherFormWith(info)

        })

      }
      else {

        that.initTeacherFormWith(info)

      }

    })

  },

  // 初始化表单
  initTeacherFormWith: function (info) {

    let that = this,
      formData = [
        { title: '', name: 'cid', type: 'hidden', value: info.cid, empty: 1 }, // 班级id
        { title: '', name: 'type', type: 'hidden', value: 1, empty: 1 },  // 1-教师类型, 0-学生类型
        { title: '姓名', name: 'name', type: 'text', value: info.name, hideTitle: true, empty: 0 },
        { title: '教师证号', name: 'teacherNo', type: 'text', value: info.teacherNo, hideTitle: true, empty: 0 },
        { title: '身份证号', name: 'no', type: 'text', value: info.no, hideTitle: true, empty: 0 },
        { title: '手机号', name: 'phone', type: 'text', value: info.phone, hideTitle: true, empty: 0 },
        { title: '职位', name: 'level', type: 'radio', list: false, value: info.level, options: info.levelList, empty: 1 },
        { title: '授课科目', name: 'job', type: 'checkbox', list: true, value: info.job, options: info.jobList, empty: 1 },
      ]

    if (info.cuid) {
      formData.push({ title: '', name: 'classUserId', type: 'hidden', value: info.cuid, empty: 1 })
    }

    this.setData({ formData: formData, submitText: '保存' })
    // 提交事件
    this.submitHandler = this.addClassUser

  },

  // 添加用户
  addClassUser: function (formData, e) {

    let that = this
    app.http.ajxAddClassUser(formData, e, function () {
      that.backWithFresh()
    })

  },

  //// 学生 ////////////////////////////////////////////
  initStudentForm: function () {

    let that = this, options = this.data.options || {}, cid = options.cid || 0, cuid = options.cuid || 0,
      info = {
        cid: cid, cuid: cuid, type: 0, name: '', studentNo: '', no: '', phone: '', level: '', levelList: [], job: '', jobList: [],
        fatherName: '', fatherPhone: '', motherName: '', motherPhone: '', address: ''
      }

    wx.wrap.setNavigationBarTitle(cuid ? '修改学生信息' : '添加学生')

    // 加载职务、课程信息
    that.loadClassMetaData(info, function () {

      if (cuid) {

        // 加载详情
        that.getClassUserDetail(cuid, function (data) {

          wx.util.extend(info, data, ['name', 'no', 'studentNo', 'phone', 'level', 'job', 'fatherName', 'fatherPhone', 'motherName', 'motherPhone', 'address'])
          that.updateClassUserJob(info)
          that.initStudentFormWith(info)

        })

      }
      else {

        that.initStudentFormWith(info)

      }

    })

  },

  // 初始化表单
  initStudentFormWith: function (info) {

    let that = this,
      formData = [
        { title: '', name: 'cid', type: 'hidden', value: info.cid, empty: 1 }, // 班级id
        { title: '', name: 'type', type: 'hidden', value: 0, empty: 1 },  // 1-教师类型, 0-学生类型
        { title: '姓名', name: 'name', type: 'text', value: info.name, hideTitle: true, empty: 0 },
        { title: '学号', name: 'teacherNo', type: 'text', value: info.studentNo, hideTitle: true, empty: 0 },
        { title: '身份证号', name: 'no', type: 'text', value: info.no, hideTitle: true, empty: 0 },
        { title: '手机号', name: 'phone', type: 'text', value: info.phone, hideTitle: true, empty: 0 },
        { title: '职位', name: 'level', type: 'radio', list: false, value: info.level, options: info.levelList, empty: 1 },
        { title: '职位名称', name: 'job', type: 'text', value: info.job, empty: 1 },
        { title: '主要监护人姓名', name: 'fatherName', type: 'text', value: info.fatherName, hideTitle: true, empty: 0 },
        { title: '主要监护人电话', name: 'fatherPhone', type: 'text', value: info.fatherPhone, hideTitle: true, empty: 0 },
        { title: '次要监护人姓名', name: 'motherName', type: 'text', value: info.motherName, hideTitle: true, empty: 1 },
        { title: '次要监护人电话', name: 'motherPhone', type: 'text', value: info.motherPhone, hideTitle: true, empty: 1 },
        { title: '家庭地址', name: 'address', type: 'editor', value: info.address, hideTitle: true, empty: 1 },
      ]

    if (info.cuid) {
      formData.push({ title: '', name: 'classUserId', type: 'hidden', value: info.cuid, empty: 1 })
    }

    this.setData({ formData: formData, submitText: '保存' })
    // 提交事件
    this.submitHandler = this.addClassUser

  },


  // 获取详情
  getClassUserDetail: function (cuid, cb) {

    let that = this
    app.showLoading(true, that)
    app.http.ajxGetClassUserDetail(cuid, function (res) {

      if (res && res.data) {
        typeof cb == "function" && cb(res.data)
      }
      else {
        that.backWithError(res && res.message || '用户信息加载失败！')
      }

    }, function (res) {
      that.backWithError(res && res.message || '用户信息加载失败！')
    }, function () {
      app.showLoading(false, that)
    })

  },


  // 更新job
  updateClassUserJob: function (info) {

    let jobArr = ',' + info.job + ','

    info.jobList.forEach(function (item) {

      if (new RegExp(',' + item.value + ',').test(jobArr)) {
        item.checked = true
      }

    })

  },

  // 获取职务，课程等信息
  loadClassMetaData: function (info, cb) {

    let that = this
    app.showLoading(true, that)
    app.http.ajxGetClassMetaData(function (res) {

      if (res && res.data) {

        let levelList = info.type == 1 ? (res.data.teacherLevelList || []) : (res.data.studentLevelList || []),
          courseList = res.data.courseList || []

        for (var i in levelList) {
          info.levelList.push({ value: i, name: levelList[i] })
        }
        info.level = info.levelList.length > 0 ? info.levelList[0].value : ''

        for (var i in courseList) {
          info.jobList.push({ value: courseList[i], name: courseList[i], checked: false })
        }

        typeof cb == "function" && cb()

      }
      else {
        that.backWithError(res && res.message || '信息加载失败！')
      }

    }, function (res) {
      that.backWithError(res && res.message || '信息加载失败！')
    }, function () {
      app.showLoading(false, that)
    })

  },

  //// 个人信息 ////////////////////////////////////////////
  initInfosForm: function () {

    let options = this.data.options || {}, index = options.index || 0, page = wx.util.getLastPageOf(1)

    wx.wrap.setNavigationBarTitle('修改个人信息')
    if (page && page.getInfos) {
      this.setData({
        formData: page.getInfos(index),
        // formStyle: 'card',
        submitFixed: false,
        submitText: '保存'
      })

      // 提交事件
      this.submitHandler = this.savePeopleInfos
      this.needShowCmfModal = false
      this.startCaptchaCountdown('yzcode')

    }

  },

  // 保存
  savePeopleInfos: function (formData, e) {

    let that = this, page = wx.util.getLastPageOf(1)
    this.captchaPreHandler('yzcode')
    app.http.ajxEditPeople(formData, e, function () {
      that.backWithFresh()
    })

  },

  // 开始倒计时
  startCaptchaCountdown: function (name) {

    let formData = this.data.formData, idx = app.http.form.getFormItemIndex(formData, name)

    if (idx >= 0) {
      (formData[idx].cntd > 0) && this.captchaCountdown(idx)
    }

  },
  // 如果手机号非必填，验证码也是非必填
  captchaPreHandler: function (name) {

    let formData = this.data.formData, idx = app.http.form.getFormItemIndex(formData, name)

    if (idx >= 0) {

      let pIdx = app.http.form.getFormItemIndex(formData, formData[idx].phone)
      if (pIdx >= 0) {
        formData[idx].data = (formData[pIdx].value.length == 0) ? 'anno' : ((formData[idx].data == 'anno') ? '' : formData[idx].data)
      }

    }

  },

  //// 家庭成员 ////////////////////////////////////////////
  initFamilyPeople: function () {

    let options = this.data.options || {},
      formData = [
        { title: '', name: 'pid', type: 'hidden', value: options.pid, empty: 1 }, // pid
        { title: '与学生关系', name: 'relationName', type: 'select', value: '爸爸', options: app.config.relationOpts(), selected: 0, empty: 0 },
        { title: '选择家长', name: 'toPid', type: 'hidden', value: '', options: [], selected: 0, empty: 1 },
        { title: '家长姓名', name: 'name', type: 'text', value: '', hideTitle: false, empty: 0 },
        { title: '性别', name: 'sex', type: 'radio', value: 0, options: app.config.genderOpts('value'), empty: 0 },
        { title: '身份证号', name: 'no', type: 'text', value: '', hideTitle: false, empty: 0 },
        { title: '手机号', name: 'phone', type: 'text', value: '', hideTitle: false, empty: 0 },
        { title: '工作单位', name: 'workName', type: 'text', value: '', hideTitle: false, empty: 1 },
        { title: '学历', name: 'education', type: 'select', value: 0, options: app.config.eduOptions(), selected: 0, empty: 1 },
        { title: '政治面貌', name: 'political', type: 'select', value: 0, options: app.config.politicalOpts(), selected: 0, empty: 1 },
        { title: '邮箱', name: 'email', type: 'text', value: '', hideTitle: false, empty: 1 },
        { title: '籍贯', name: 'place', type: 'text', value: '', hideTitle: false, empty: 1 },
        { title: '住址', name: 'address', type: 'editor', value: '', hideTitle: false, empty: 1 },
      ]

    wx.wrap.setNavigationBarTitle('增加家庭成员')

    this.setData({
      formData: formData,
      submitText: '保存'
    })

    // 获取其他成员
    this.loadOtherFamilyList(options.pid)

    // 提交事件
    this.submitHandler = this.addFamilyPeople

  },

  // 添加家庭成员
  addFamilyPeople: function (formData, e) {

    let that = this

    app.http.ajxAddFamilyPeople(formData, e, function () {
      that.backWithFresh()
    })

  },

  // 获取学生没有关联的其他家长列表
  loadOtherFamilyList: function (pid) {

    let that = this, formData = this.data.formData, idx = app.http.form.getFormItemIndex(formData, 'toPid'),
      options = [], str = 'formData[' + idx + ']', param = {}

    app.http.ajxGetOtherFamilyList(pid, function (res) {

      if (res && res.data && res.data.length > 0) {

        options.push({ id: '', name: '请选择', relation: '' })
        res.data.forEach(function (item) {
          options.push({ id: item['toPid'], name: item['toPid_name'], relation: item['name'] })
        })
        param[str + '.options'] = options
        param[str + '.type'] = 'select'
        that.setData(param)

      }

    })

  },

  // 家庭成员选择项变更
  familySelectChangedHandler: function (idx) {

    let that = this, formData = this.data.formData,
      toPidIdx = app.http.form.getFormItemIndex(formData, 'toPid'),
      relIdx = app.http.form.getFormItemIndex(formData, 'relationName')

    if (idx == toPidIdx) {

      let len = formData.length, selected = formData[idx].selected,
        res = wx.util.getOptionBy('id', formData[idx].options[selected]['relation'], formData[relIdx].options)

      for (var i = idx + 1; i < len; i++) {
        !formData[i]['__type'] && (formData[i]['__type'] = formData[i]['type']);
        formData[i]['type'] = !!formData[idx].value ? 'hidden' : formData[i]['__type']
      }
      formData[relIdx].value = res.index >= 0 ? res.name : formData[relIdx].value
      formData[relIdx].selected = res.index >= 0 ? res.index : formData[relIdx].selected

      this.setData({ formData: formData })

    }

  },

  //// 活动 ////////////////////////////////////////////
  initActivityForm: function () {

    let that = this, options = this.data.options || {}

    if (app.isSchoolActivity(options.level)) {  // 校级

      app.http.getGradeListWithCached(function (datas) {

        if (datas && datas.length > 0) {
          that.initActivityFormWith(datas, options)
        } else {
          app.showInfoModal('', '没有设置年级')
        }

      }, function (res) {
        app.showInfoModal('', res && res.message || '年级列表加载失败！')
      })

    } else {

      that.initActivityFormWith(null, options)

    }

  },

  initActivityFormWith: function (grades, options) {

    let date = new Date(), today = wx.util.dateFormat(date, 'Y-M-D'), level = options.level || 0,
      formData = []

    // 活动字段
    formData.push({ title: '', name: 'type', type: 'hidden', value: level, empty: 1 })
    formData.push({ title: '封面', name: 'img', type: 'cover', value: '', files: [], empty: 0 })
    formData.push({ title: '活动名称', name: 'title', type: 'text', value: '', empty: 0, hideTitle: true, showInfo: true, infoType: 1 })
    formData.push({ title: '活动内容', name: 'content', type: 'editor', value: '', empty: 0, hideTitle: true })
    formData.push({ title: '活动图片', name: 'images', type: 'images', value: [], files: [], hideTitle: true, empty: 1 })

    // 校级类型
    if (app.isSchoolActivity(level)) {
      formData.push({ title: '活动范围', name: 'grade', type: 'grades', value: wx.util.objectsValue(grades, 'id'), info: '全校', empty: 0 })
    }
    else {
      formData.push({ title: '', name: 'cid', type: 'hidden', value: app.userData.getClassId(), empty: 1 })
    }
    // 活动类型
    formData.push({
      title: '活动类型', name: 'activityType', type: 'radio', list: true, value: 0, options: [
        { value: 0, name: '普通类' },
        { value: 1, name: '竞赛类' },
        { value: 2, name: '实践类' },
      ], empty: 1
    })
    // formData.push({
    //   title: '参与方式', name: 'joinType', type: 'radio', list: true, value: 0, options: [
    //     { value: 0, name: '线上报名' },
    //     { value: 1, name: '全校参与' },
    //     { value: 2, name: '指定人员' },
    //   ], empty: 1
    // })
    formData.push({ title: '活动开始时间', name: 'starttime', type: 'datetime', value: today + ' 08:00', date: today, time: '08:00', empty: 0 })
    formData.push({ title: '报名截止时间', name: 'enroll_endtime', type: 'datetime', value: today + ' 18:00', date: today, time: '18:00', empty: 1 })
    formData.push({ title: '推荐入围截止时间', name: 'recommend_endtime', type: 'hidden', value: today + ' 18:00', date: today, time: '18:00', empty: 1 })
    formData.push({ title: '投票(评分)截止时间', name: 'vote_endtime', type: 'hidden', value: today + ' 18:00', date: today, time: '18:00', empty: 1 })
    formData.push({ title: '活动结束时间', name: 'endtime', type: 'datetime', value: today + ' 18:00', date: today, time: '18:00', empty: 0 })
    formData.push({ title: '活动地点', name: 'address', type: 'text', value: '', empty: 0, hideTitle: true })

    wx.wrap.setNavigationBarTitle('发布活动')
    this.setData({ formData: formData, submitText: '发布' })
    // 提交事件
    this.submitHandler = this.addActivity

  },

  // 发布活动
  addActivity: function (formData, e) {

    let that = this, idx = app.http.form.getFormItemIndex(formData, 'content')

    if (!this.activityTimeValidCheck(formData)) {
      return
    }

    if (idx >= 0) {
      formData[idx].value = wx.util.toRichText(formData[idx].value)
    }

    app.http.ajxAddActivity(formData, e, function () {
      that.backWithFresh()
    })

  },

  // 活动
  activityRadioChanged: function (idx) {

    let formItem = this.data.formData[idx]

    if (formItem.name == 'activityType') { // 活动类型变更
      this.activityTypeChanged(formItem.value)
    } else if (formItem.name == 'joinType') {

    }

  },

  // 活动类型变更
  activityTypeChanged: function (activityType) {

    var formData = this.data.formData,
      regIdx = app.http.form.getFormItemIndex(formData, 'enroll_endtime'),
      rmdIdx = app.http.form.getFormItemIndex(formData, 'recommend_endtime'),
      votIdx = app.http.form.getFormItemIndex(formData, 'vote_endtime'),
      addrIdx = app.http.form.getFormItemIndex(formData, 'address'),
      configs = {}, str = '', param = {}

    configs[regIdx] = [
      { title: '报名截止时间', type: 'datetime', empty: 0 },
      { title: '作品提交截止时间', type: 'datetime', empty: 0 },
      { title: '', type: 'hidden', empty: 1 }
    ]
    configs[rmdIdx] = [
      { title: '', type: 'hidden', empty: 1 },
      { title: '推荐入围截止时间', type: 'datetime', empty: 0 },
      { title: '', type: 'hidden', empty: 1 }
    ]
    configs[votIdx] = [
      { title: '', type: 'hidden', empty: 1 },
      { title: '投票(评分)截止时间', type: 'datetime', empty: 0 },
      { title: '', type: 'hidden', empty: 1 }
    ]
    configs[addrIdx] = [
      { title: '活动地点', type: 'text', empty: 0 },
      { title: '', type: 'hidden', empty: 1 },
      { title: '活动地点', type: 'text', empty: 0 }
    ]


    for (var i in configs) {
      str = 'formData[' + i + ']'
      param[str + '.title'] = configs[i][activityType].title
      param[str + '.type'] = configs[i][activityType].type
      param[str + '.empty'] = configs[i][activityType].empty
    }

    this.setData(param)

  },

  // 活动时间检查
  activityTimeValidCheck: function (formData) {

    let startIdx = app.http.form.getFormItemIndex(formData, 'starttime'),
      endIdx = app.http.form.getFormItemIndex(formData, 'endtime'),
      regIdx = app.http.form.getFormItemIndex(formData, 'enroll_endtime'),
      rmdIdx = app.http.form.getFormItemIndex(formData, 'recommend_endtime'),
      votIdx = app.http.form.getFormItemIndex(formData, 'vote_endtime'),
      typeIdx = app.http.form.getFormItemIndex(formData, 'activityType')

    var result = true

    if (startIdx < 0 || endIdx < 0 || regIdx < 0 || rmdIdx < 0 || votIdx < 0 || typeIdx < 0) {

      app.showInfoModal('提示', '活动时间不完整！')
      result = false

    } else {

      let starttime = wx.util.getDateTimeStamp(formData[startIdx].value + ':00'),
        endtime = wx.util.getDateTimeStamp(formData[endIdx].value + ':00'),
        regtime = wx.util.getDateTimeStamp(formData[regIdx].value + ':00'),
        rmdtime = wx.util.getDateTimeStamp(formData[rmdIdx].value + ':00'),
        vottime = wx.util.getDateTimeStamp(formData[votIdx].value + ':00')

      // 开始时间不能大于结束时间
      if (starttime >= endtime) {

        app.showInfoModal('提示', '活动结束时间 必须大于 活动开始时间！')
        result = false

      }
      // 普通活动，报名时间
      else if (formData[typeIdx].value == 0) {

        if (regtime > endtime) {
          app.showInfoModal('提示', '报名截止时间不能大于活动结束时间！')
          result = false
        }

      }
      // 竞赛活动
      else if (formData[typeIdx].value == 1) {

        if (regtime > rmdtime) {

          app.showInfoModal('提示', '作品提交截止时间 不能大于 推荐入围截止时间！')
          result = false

        }
        else if (rmdtime > vottime) {

          app.showInfoModal('提示', '推荐入围截止时间 不能大于 投票(评分)截止时间！')
          result = false

        } else if (vottime > endtime) {

          app.showInfoModal('提示', '投票(评分)截止时间 不能大于 活动结束时间！')
          result = false

        }

      }

    }

    return result

  },


  //// 活动作品 ////////////////////////////////////////////
  initActivityArticleForm: function () {
    let options = this.data.options || {}, id = options.id || 0,
      formData = [
        { title: 'id', name: 'activityid', type: 'hidden', value: id, empty: 1 },
        { title: '作品名称', name: 'title', type: 'text', value: '', empty: 0, maxlength: 40, hideTitle: true },
        // { title: '作品内容', name: 'content', type: 'editor', value: '', empty: 1, hideTitle: true },
        { title: '作品图片(最多9张)', name: 'images', type: 'images', value: [], files: [], empty: 0 },
      ]

    wx.wrap.setNavigationBarTitle('提交作品')
    this.setData({ formData: formData, submitText: '保存' })
    // 提交事件
    this.submitHandler = this.addActivityArticle

  },
  // 提交作品
  addActivityArticle: function (formData, e) {

    let that = this

    app.http.ajxAddActivityArticle(formData, e, function () {
      that.backWithFresh()
    })

  },

  //// 评一评 ////////////////////////////////////////////
  initRatingForm: function () {

    let options = this.data.options || {}, imgUrl = options.img || '', cid = options.cid || 0, inalterable = options.inalterable || 0,
      formData = [
        { title: '用户', name: 'contactIds', type: 'contacts', value: [], contacts: [], group: 'student', inalterable: inalterable, empty: 0 },
        { title: '评价项', name: 'items', type: 'ratingItems', value: [], items: [], empty: 0 },
        { title: '评分', name: 'rating', type: 'rating', value: 0, stars: [1, 2, 3, 4, 5], multiplier: 1, empty: 1 },
        { title: '内容', name: 'content', type: 'editor', value: '', hideTitle: true, empty: 1 },
        { title: '图片', name: 'images', type: 'images', value: [], files: [], hideTitle: true, empty: 1 },
      ]

    wx.wrap.setNavigationBarTitle('评一评')

    // 初始数据
    let contactIdx = app.http.form.getFormItemIndex(formData, 'contactIds'),
      imgsIdx = app.http.form.getFormItemIndex(formData, 'images'),
      tagsIdx = app.http.form.getFormItemIndex(formData, 'items')
    if (cid && contactIdx >= 0) { // 已选择一个联系人
      if (formData[contactIdx].type == 'contact') {
        formData[contactIdx].value = cid
      } else {
        formData[contactIdx].value.push(cid)
      }
    }
    if (imgUrl && imgUrl.length > 0 && imgsIdx >= 0) { // 已选择图片
      formData[imgsIdx].files.push({ url: decodeURI(imgUrl), value: '' })
    }

    this.setData({ formData: formData })
    // 获取联系人信息
    if (cid && contactIdx >= 0) {
      this.getContactDetail(contactIdx)
    }
    // 提交事件
    this.submitHandler = this.addRating

  },

  // 提交评一评
  addRating: function (formData, e) {
    let that = this
    app.http.ajxAddGrowth(formData, e, function () {
      // app.back()
      that.backWithFresh()
    })
  },

  // 评一评图片增加
  ratingImagesAdded: function (idx, len) {

    let that = this
    console.log(`ratingImagesAdd: ${idx} -- ${len}`)

  },

  // 评一评图片减少
  ratingImagesDeleted: function (idx, subIdx) {

    let that = this
    console.log(`ratingImagesDeleted: ${idx} --- ${subIdx}`)

  },

  //// 评一评 ////////////////////////////////////////////
  initOpinionForm: function () {

    let options = this.data.options || {}, imgUrl = options.img || '', cid = options.cid || 0, inalterable = options.inalterable || 0,
      formData = [
        { title: '用户', name: 'contactIds', type: 'contacts', value: [], contacts: [], group: 'student', inalterable: inalterable, empty: 0 },
        { title: '印象', name: 'tags', type: 'tags', value: [], tags: [], editable: true, maxCount: 3, deleteTags: 'deleteTags', theme: this.data.theme || 'green', empty: 0 },
        { title: '内容', name: 'content', type: 'editor', value: '', hideTitle: true, empty: 1 },
        { title: '图片', name: 'images', type: 'images', value: [], files: [], hideTitle: true, empty: 1 },
        // 需要删除的标签
        { title: '', name: 'deleteTags', type: 'hidden', value: [], empty: 1 },
      ]

    wx.wrap.setNavigationBarTitle('评一评')
    let contactIdx = app.http.form.getFormItemIndex(formData, 'contactIds'),
      imgsIdx = app.http.form.getFormItemIndex(formData, 'images'),
      tagsIdx = app.http.form.getFormItemIndex(formData, 'tags')
    if (imgUrl && imgUrl.length > 0 && imgsIdx >= 0) { // 已选择图片
      formData[imgsIdx].files.push({ url: decodeURI(imgUrl), value: '' })
    }

    this.setData({ formData: formData })
    if (tagsIdx >= 0) {
      this.getTagsList(tagsIdx)
    }
    // 提交事件
    this.submitHandler = this.addOpinion

  },

  // 提交评一评
  addOpinion: function (formData, e) {
    let that = this
    app.http.ajxAddGrowthByTeacher(formData, e, function () {
      // app.back()
      that.backWithFresh()
    })
  },

  //// 随手记 ////////////////////////////////////////////
  initParentsNoteForm: function () {

    let options = this.data.options || {}, imgUrl = options.img || '',
      formData = [
        { title: '印象', name: 'tags', type: 'tags', value: [], tags: [], editable: true, maxCount: 3, deleteTags: 'deleteTags', theme: this.data.theme || 'green', empty: 0 },
        { title: '是否公开', name: 'isopen', type: 'switch', value: 1, empty: 1 },
        { title: '内容', name: 'content', type: 'editor', value: '', hideTitle: true, empty: 1 },
        { title: '图片', name: 'images', type: 'images', value: [], files: [], hideTitle: true, empty: 1 },
        // 需要删除的标签
        { title: '', name: 'deleteTags', type: 'hidden', value: [], empty: 1 },
      ]

    wx.wrap.setNavigationBarTitle('随手记')
    let imgsIdx = app.http.form.getFormItemIndex(formData, 'images'), tagsIdx = app.http.form.getFormItemIndex(formData, 'tags')
    if (imgUrl && imgUrl.length > 0 && imgsIdx >= 0) { // 已选择图片
      formData[imgsIdx].files.push({ url: decodeURI(imgUrl), value: '' })
    }

    this.setData({ formData: formData })
    if (tagsIdx >= 0) {
      this.getTagsList(tagsIdx)
    }
    // 提交事件
    this.submitHandler = this.addParentsNote

  },

  // 提交随手记
  addParentsNote: function (formData, e) {
    let that = this
    app.http.ajxAddGrowthByParent(formData, e, function () {
      // app.back()
      that.backWithFresh()
    })
  },

  // load rating tags
  getTagsList: function (idx) {

    let that = this, str = 'formData[' + idx + '].tags', param = {}

    app.showLoading(true, that)
    app.http.ajxGetTagsList(function (res) {

      param[str] = res && res.data || []
      that.setData(param)

    }, function (res) {
      that.backWithError(res && res.message || '标签信息加载失败！')
    }, function () {
      app.showLoading(false, that)
    })

  },

  //// 调课 ////////////////////////////////////////////
  initTmTableForm: function () {

    let options = this.data.options || {}, csdid = options.csdid || '', id = options.id || '',
      starttime = options.starttime || '', endtime = options.endtime || starttime, content = options.content || '', adjustment = options.adjustment || '',
      remark = options.remark || '',
      formData = [
        { title: '课程id', name: 'csdid', type: 'hidden', value: csdid, empty: 1 },
        { title: '调课记录id', name: 'id', type: 'hidden', value: id, empty: 1 },
        { title: '日期', name: 'starttime', type: 'hidden', value: starttime, empty: 1 },
        { title: '原课程', name: 'course', type: 'readonly', value: starttime + ' ' + content, empty: 1 },
        { title: '调后课程', name: 'name', type: 'text', value: adjustment || '', hideTitle: true, empty: 0 },
        { title: '结束日期', name: 'endtime', type: 'date', value: endtime, start: starttime, empty: 0 },
        { title: '备注', name: 'remark', type: 'editor', value: remark, hideTitle: true, empty: 1 },
      ]

    wx.wrap.setNavigationBarTitle('调课')
    this.setData({ formData: formData, submitFixed: false, submitText: '保存' })
    // 提交事件
    this.submitHandler = this.saveTmTable
    this.needShowCmfModal = false

  },

  // 调课
  saveTmTable: function (formData, e) {

    let that = this, options = this.data.options || {}, csid = options.csid || 0, page = wx.util.getLastPageOf(1), cid = options.cid || 0

    app.http.ajxSaveClassScheduleChangeData(formData, e, csid, cid, function () {
      page && page.loadScheduleChangeData(null, cid)
      app.back()
    })

  },

  //// 请假 ////////////////////////////////////////////
  initVacationForm: function () {

    let options = this.data.options || {}, date = new Date(), today = wx.util.dateFormat(date, 'Y-M-D'), className = app.userData.getClassName(), studentName = app.userData.getStudentName(),
      formData = [
        { title: '请假学生', name: 'name', type: 'readonly', value: studentName, empty: 1 },
        { title: '所在班级', name: 'class', type: 'readonly', value: className, empty: 1 },
        {
          title: '请假类型', name: 'type', type: 'radio', value: 1, options: [
            { value: 1, name: '病假' },
            { value: 2, name: '事假' },
          ], empty: 0
        },
        {
          title: '开始时间', name: 'starttime', type: 'date', value: today, tagName: 'startdate', tagType: 'tag', tags: [
            { id: 0, title: '上午', selected: true },
            { id: 0, title: '下午', selected: false },
          ], theme: this.data.theme || 'green', empty: 0
        },
        { name: 'startdate', type: 'hidden', value: '上午', empty: 0 },
        {
          title: '结束时间', name: 'endtime', type: 'date', value: today, tagName: 'enddate', tagType: 'tag', tags: [
            { id: 0, title: '上午', selected: false },
            { id: 0, title: '下午', selected: true },
          ], theme: this.data.theme || 'green', empty: 0
        },
        { name: 'enddate', type: 'hidden', value: '下午', empty: 0 },
        { title: '请假事由', name: 'content', type: 'editor', value: '', hideTitle: true, empty: 0 },
        { title: '图片', name: 'images', type: 'images', value: [], files: [], hideTitle: true, empty: 1 }
      ]

    wx.wrap.setNavigationBarTitle('申请请假')
    this.setData({ formData: formData })
    // 提交事件
    this.submitHandler = this.addVacation

  },

  // 时间校验
  vacationDateCheck: function (formData) {

    var result = true, stm = app.http.form.getFormItemIndex(formData, 'starttime'), etm = app.http.form.getFormItemIndex(formData, 'endtime'),
      std = app.http.form.getFormItemIndex(formData, 'startdate'), etd = app.http.form.getFormItemIndex(formData, 'enddate'),
      stamp0 = wx.util.getDateTimeStamp(formData[stm].value + ' 00:00'), stamp1 = wx.util.getDateTimeStamp(formData[etm].value + ' 00:00')

    if ((stamp0 > stamp1) || (stamp0 == stamp1) && (formData[std].value == '下午') && (formData[etd].value == '上午')) {
      app.showInfoModal('提示', '开始时间不能大于结束时间！')
      result = false
    }

    return result

  },

  // 提交请假
  addVacation: function (formData, e) {

    let that = this

    if (this.vacationDateCheck(formData)) {
      // 添加请假条的接口
      app.http.ajxAddDayOff(formData, e, function (res) {

        if (res && res.message) {

          app.showInfoModal('提示', res.message, function () {
            that.backWithFresh();
          })

        } else {
          that.backWithFresh();
        }

      })
    }

  },

  //// 请假-补充材料 ////////////////////////////////////
  initEditDayoff: function () {

    let options = this.data.options || {}, page = wx.util.getLastPageOf(1), data = page.data.vocation,
      formData = [
        { title: '', name: 'dayOffid', type: 'hidden', value: data.id, empty: 1 },
        { title: '请假学生', name: 'name', type: 'readonly', value: data.studentName || '', empty: 1 },
        { title: '所在班级', name: 'class', type: 'readonly', value: data.className || '', empty: 1 },
        { title: '请假类型', name: 'type', type: 'readonly', value: data.type || '', empty: 1 },
        { title: '开始时间', name: 'starttime', type: 'readonly', value: data.starttime + ' ' + data.startdate, empty: 1 },
        { title: '结束时间', name: 'endtime', type: 'readonly', value: data.endtime + ' ' + data.enddate, empty: 1 },
        { title: '请假事由', name: 'content', type: 'readonly', value: data.content || '', empty: 1 },
        { title: '补充材料', name: 'images', type: 'images', value: [], files: [], empty: 0 }
      ]

    wx.wrap.setNavigationBarTitle('补充请假材料')
    this.setData({ formData: formData })
    // 提交事件
    this.submitHandler = this.editVacation

  },

  // 提交请假
  editVacation: function (formData, e) {

    let that = this

    // 添加请假条的接口
    app.http.ajxEditDayOff(formData, e, function (res) {

      if (res && res.message) {

        app.showInfoModal('提示', res.message, function () {
          that.backWithFresh();
        })

      } else {
        that.backWithFresh();
      }

    })

  },

  //// 写邮件 ////////////////////////////////////////////
  initMailboxForm: function () {

    let options = this.data.options || {},
      formData = [
        { title: '', name: 'userbindId', type: 'hidden', value: '', empty: 1 },
        { title: '收件人', name: 'contactId', type: 'select', value: '', options: [], selected: -1, empty: 0 },
        { title: '内容', name: 'content', type: 'editor', value: '', hideTitle: true, empty: 0 },
        // { title: '图片', name: 'images', type: 'images', value: [], files: [], empty: 1 },
      ]

    wx.wrap.setNavigationBarTitle('写邮件')

    this.setData({ formData: formData, submitFixed: false, submitText: '发送' })
    this.loadPositionList(app.http.form.getFormItemIndex(formData, 'contactId'))
    // 提交事件
    this.submitHandler = this.addMailbox

  },

  // 新增邮件的接口
  addMailbox: function (formData, e) {
    let that = this

    app.http.ajxAddEmail(formData, e, function () {
      that.backWithFresh()
    })

  },

  // 获取写邮件对象
  loadPositionList: function (idx) {

    if (idx < 0) { return } // 无效的index

    let that = this, data = {}, formItem = this.data.formData[idx], str = 'formData[' + idx + ']', param = {}

    app.showLoading(true, that)
    app.http.ajxGetPositionList(data, function (res) {

      if (res && res.data) {

        res.data.forEach(function (data) {

          let receiver = { id: data.contactid || 0, uid: data.userbindid || 0, name: data.name }
          if (data.job) {
            receiver.name += '(' + data.job + ')'
          }
          formItem.options.push(receiver)

        })

        param[str] = formItem
        that.setData(param)

      }

    }, null, function () {
      app.showLoading(false, that)
    })
  },

  //// 私信 ////////////////////////////////////////////
  initMessageForm: function () {

    wx.wrap.setNavigationBarTitle('发私信')

    let options = this.data.options || {}, inalterable = options.inalterable || 0, // 个人首页中的发私信
      formData = [
        { title: '', name: 'contactId', type: 'receivers', value: [], contacts: [], group: 'all', bindName: 'userbindId', inalterable: inalterable, hideTitle: true, empty: 0 },
        { title: '', name: 'userbindId', type: 'hidden', value: [], empty: 1 },
        { title: '内容', name: 'content', type: 'editor', value: '', hideTitle: true, empty: 0 },
        { title: '图片', name: 'images', type: 'images', value: [], files: [], hideTitle: true, empty: 1 },
      ]

    let page = wx.util.getLastPageOf(1), contact = null
    if (page && page.messageTo) { // 个人首页中的发私信
      contact = page.messageTo()
      formData[0].contacts.push(contact)
      if (formData[0].type == 'receiver') {
        formData[0].value = contact.contactid
        formData[1].value = contact.userbindId
      } else {
        formData[0].value.push(contact.contactid)
        formData[1].value.push(contact.userbindId)
      }
    }

    this.setData({ formData: formData, submitFixed: false, submitText: '发送' })
    // 提交事件
    this.submitHandler = this.addMessage
    this.needShowCmfModal = false

  },

  // 发送私信（可多选）
  addMessage: function (formData, e) {

    let that = this

    app.showLoading(true, that)
    app.http.ajxAddMessage(formData, e, function () {
      that.backWithFresh()
    })

  },

  //// 意见反馈 ////////////////////////////////////////////
  initFeedbackForm: function () {

    let options = this.data.options || {},
      formData = [
        { title: '问题和意见', name: 'content', type: 'editor', value: '', hideTitle: true, empty: 0 },
        { title: '联系电话', name: 'phone', type: 'text', value: '', empty: 1, hideTitle: true, placeholder: '请输入您的联系电话(选填)，便于我们与您联系' },
        { title: '图片(选填，提供问题截图)', name: 'images', type: 'images', value: [], files: [], empty: 1 },
      ]

    wx.wrap.setNavigationBarTitle('意见反馈')
    this.setData({ formData: formData, submitFixed: false, })
    // 提交事件
    this.submitHandler = this.addFeedback

  },
  // 意见反馈
  addFeedback: function (formData, e) {

    var that = this
    app.http.ajxAddFeedback(formData, e, function () {

      app.showInfoModal('提交成功', '感谢您的反馈，您的支持是我们最大的动力！', function () {
        app.back()
      })

    })

  },

  //// 提交 ////////////////////////////////////////////
  formSubmit: function (e) {

    let that = this, options = this.data.options || {}, formType = options.type || '', formData = this.data.formData,
      submitHandler = this.submitHandler

    if (this.needShowCmfModal) {

      app.showCfmModal('提示', '确定提交吗？', function () {

        // app.http.form.deserialize(e, formData)
        submitHandler && submitHandler(formData, e)

      })

    } else {

      // app.http.form.deserialize(e, formData)
      submitHandler && submitHandler(formData, e)

    }

  },

  // 取消
  bindCancel: function () {
    app.back()
  },

  /********** input ********************************************/
  bindFormInput: function (e) {

    let idx = e.currentTarget.dataset.idx, formItem = this.data.formData[idx]
    formItem.value = e.detail.value

  },

  /********** phones ********************************************/
  bindPhoneInput: function (e) {

    let idx = e.currentTarget.dataset.idx, sub = e.currentTarget.dataset.sub, formItem = this.data.formData[idx]

    formItem.phones[sub] = e.detail.value
    formItem.value = formItem.phones.join(',')

  },

  // 添加手机号
  bindAddPhones: function (e) {

    let idx = e.currentTarget.dataset.idx, formItem = this.data.formData[idx],
      str = 'formData[' + idx + ']', param = {}

    formItem.phones.push('')
    param[str] = formItem
    this.setData(param)

  },

  // 删除手机号
  bindDelPhones: function (e) {

    let that = this, idx = e.currentTarget.dataset.idx, sub = e.currentTarget.dataset.sub, formItem = this.data.formData[idx],
      str = 'formData[' + idx + ']', param = {}

    app.showCfmModal('', '', function () {

      formItem.phones.splice(sub, 1)
      formItem.value = formItem.phones.join(',')
      param[str] = formItem
      that.setData(param)

    })

  },

  /********** 验证码 ********************************************/
  // 获取验证码
  bindGetCaptcha: function (e) {

    let that = this, index = e.currentTarget.dataset.idx, formData = this.data.formData, formItem = formData[index]

    if (this.data.submitting) { // 已经提交请求
      return
    }

    formItem.data = 'anno'
    if (app.http.form.emptyCheck(formData)) {

      var postData = app.http.form.serialize(formData)
      postData['action'] = formItem.action
      that.captchaHandler(index, { time: 10 })
      // 接口未定义
      // app.showLoading(true, that), app.isSubmitting(true, that)
      // app.http.ajxGetCaptcha(postData, function (res) {

      //   res && that.captchaHandler(index, res)

      // }, function (res) {
      //   app.showInfoModal('提示', res && res.message || '验证码获取错误!')
      // }, function () {
      //   app.showLoading(false, that), app.isSubmitting(false, that)
      // })

    }

  },

  // 验证码处理
  captchaHandler: function (index, res) {

    var count = parseInt(res.time || 60), timeStamp = (new Date()).getTime(),
      str = 'formData[' + index + ']', param = {}

    timeStamp += count * 1000
    param[str + '.cntd'] = count
    param[str + '.data'] = res.data || ''
    this.setData(param)
    this.captchaCountdown(index)
    wx.storage.setCaptcha(timeStamp)

  },

  // 倒计时
  captchaCountdown: function (index) {

    let that = this, formData = this.data.formData, formItem = formData[index],
      str = 'formData[' + index + '].cntd', param = {}

    if (formItem.type == 'captcha' && formItem.cntd > 0) {

      param[str] = --formItem.cntd
      this.setData(param)

      if (formItem.cntd > 0) {
        clearTimeout(that.__timeout)
        that.__timeout = setTimeout(function () {
          that.captchaCountdown(index)
        }, 1000);
      }

    }

  },

  /********** radio ********************************************/
  radioChange: function (e) {

    let idx = e.currentTarget.dataset.idx, str = 'formData[' + idx + '].value', param = {}

    param[str] = e.detail.value
    this.setData(param)
    this.radioChangeHookHandler(idx)

  },

  radioChangeHookHandler: function (idx) {

    let options = this.data.options || {}, formType = options.type || '', formData = this.data.formData

    if (formType == 'class') {
      this.classTypeChanged(idx)
    } else if (formType == 'notice') {
      this.noticeRadioChanged(idx)
    } else if (formType == 'activity') {
      this.activityRadioChanged(idx)
    }

  },

  /********** checkbox ********************************************/
  checkboxChange: function (e) {

    let idx = e.currentTarget.dataset.idx, formItem = this.data.formData[idx], options = formItem.options, values = [],
      str = 'formData[' + idx + '].value', param = {}

    for (var i in options) {
      let option = options[i]
      option.checked = wx.util.isContained(e.detail.value, option.value)
      if (option.checked) {
        values.push(option.value)
      }
    }

    param[str] = values
    this.setData(param)

  },

  checkboxChangeHookHandler: function (idx) {

  },

  /********** switch ********************************************/
  bindSwitchChange: function (e) {

    let idx = e.currentTarget.dataset.idx, str = 'formData[' + idx + '].value', param = {}
    param[str] = e.detail.value ? 1 : 0
    this.setData(param)

  },


  /********** select ********************************************/
  // init pop select
  initSelect: function (e) {

    let idx = e.currentTarget.dataset.idx, formItem = this.data.formData[idx], popSel = this.data.popSel, data = {}

    data['popSel.index'] = idx
    data['popSel.values'] = formItem.options
    data['popSel.cur'] = formItem.selected
    data['popSel.style'] = formItem.style || ''

    this.showPopSel(data)

  },
  // select item changed
  popSelChanged: function (e) {

    let cur = e.currentTarget.dataset.idx, popSel = this.data.popSel, idx = popSel.index,
      formItem = this.data.formData[idx], str = 'formData[' + idx + ']', data = {}

    data['popSel.cur'] = cur
    data[str + '.selected'] = cur
    data[str + '.value'] = formItem.options[cur].id
    this.hidePopSel(null, data)

    this.popSelChangeHookHandler(idx)

  },

  // confirm
  cfmPopSel: function (e) { },

  // 选择后的hook
  popSelChangeHookHandler: function (idx) {

    let options = this.data.options || {}, formType = options.type || '', popSel = this.data.popSel, cur = popSel.cur

    if (formType == 'mailbox') {
      this.data.formData[0].value = popSel.values[cur].uid
    }
    else if (formType == 'exam') {
      this.examConfigChanged(idx)
    } else if (formType == 'addFamily') {
      this.familySelectChangedHandler(idx)
    }

  },
  /********** Tags ********************************************/
  // 清除选中状态
  clearTagsStatus: function (tags) {
    for (var i in tags) {
      tags[i].selected = false
    }
  },
  // 标签是否已经添加
  isTagAlreadyAdded: function (tag, tags) {

    var result = false
    for (var i in tags) {
      if (tags[i].title == tag) {
        result = true
        break
      }
    }
    return result

  },
  // 添加
  bindAddTags: function (e) {

    this.addTags(e.currentTarget.dataset.idx, e.detail.value)

  },

  // 删除tag
  bindDeleteTag: function (e) {

    let that = this, idx = e.currentTarget.dataset.idx, sub = e.currentTarget.dataset.sub, formItem = this.data.formData[idx], tags = formItem.tags,
      str = 'formData[' + idx + '].tags', param = {}

    let tag = tags[sub]
    app.showCfmModal(null, null, function () {

      tags.splice(sub, 1)
      param[str] = tags
      that.setData(param)
      that.updeteTagsValue(idx, tags)

      that.deleteTags(idx, tag.id)

    })

  },
  deleteTags: function (idx, id) {

    let that = this, formData = this.data.formData, deleteTags = formData[idx].deleteTags || ''

    if (id && deleteTags) {
      let formIdx = app.http.form.getFormItemIndex(formData, deleteTags)
      if (formIdx >= 0) {
        formData[formIdx].value.push(id)
      }

    }

  },
  // tag 选中
  bindTagTap: function (e) {

    let idx = e.currentTarget.dataset.idx, sub = e.currentTarget.dataset.sub, formItem = this.data.formData[idx], tags = formItem.tags,
      maxCount = formItem.maxCount || 0, str = 'formData[' + idx + '].tags', param = {}

    // 单选
    if (formItem.type == 'tag' && tags[sub].selected) { return }

    if (formItem.type == 'tag') {
      this.clearTagsStatus(tags)
    } else if (maxCount > 0 && !tags[sub].selected && formItem.value.length >= maxCount) {
      app.toastErr('最多选择' + maxCount + '个')
      return
    }
    tags[sub].selected = !tags[sub].selected
    param[str] = tags
    this.setData(param)
    this.updeteTagsValue(idx, tags)

  },
  // 更新数据
  updeteTagsValue: function (idx, tags) {

    let formData = this.data.formData, formItem = idx < formData.length ? formData[idx] : {}, value = []

    for (var i in tags) {
      var tag = tags[i]
      if (tag.selected) {
        value.push(tag.id || tag.title)
      }
    }

    formItem.value = (formItem.type == 'tags') ? value : (value.length > 0 ? value[0] : '')
    this.tagsChangeHookHandler(idx, formItem.value)

  },

  // 日期 + tag
  bindDateTagTap: function (e) {

    let idx = e.currentTarget.dataset.idx, sub = e.currentTarget.dataset.sub, formData = this.data.formData, formItem = formData[idx],
      tags = formItem.tags, tagIdx = app.http.form.getFormItemIndex(formData, formItem.tagName),
      str = 'formData[' + idx + '].tags', param = {}

    // 单选
    if (formItem.tagType == 'tag' && tags[sub].selected) { return }

    if (formItem.tagType == 'tag') {
      this.clearTagsStatus(tags)
    }
    tags[sub].selected = !tags[sub].selected
    param[str] = tags
    this.setData(param)
    this.updeteTagsValue(tagIdx, tags)

  },

  // focus
  bindTagFocus: function (e) {

    let idx = e.currentTarget.dataset.idx, str = 'formData[' + idx + '].focus', param = {}
    param[str] = true
    this.setData(param)

  },
  bindTagBlur: function (e) {

    let idx = e.currentTarget.dataset.idx, str = 'formData[' + idx + '].focus', param = {}
    param[str] = false
    this.setData(param)

    if (e.detail.value.length > 0) {
      this.addTags(idx, e.detail.value)
    }

  },

  // 新增
  addTags: function (idx, tag) {

    let formItem = this.data.formData[idx], maxCount = formItem.maxCount || 0,
      str = 'formData[' + idx + ']', param = {}

    if (tag.length == 0) {
    } else if (/^\d+$/.test(tag)) {
      app.toastErr(formItem.title + '不能为纯数字')
    } else if (this.isTagAlreadyAdded(tag, formItem.tags)) {
      app.toastErr(tag + '已经存在')
    } else {

      if (formItem.type == 'tag') {
        this.clearTagsStatus(formItem.tags)
      }

      formItem.tags.push({
        id: 0,
        title: tag,
        selected: (maxCount > 0 && formItem.value.length >= maxCount) ? false : true,
        isDelete: true
      })

      param[str] = formItem
      this.setData(param)
      this.updeteTagsValue(idx, formItem.tags)

    }
  },

  // 标签更新后的hook
  tagsChangeHookHandler(idx, value) {

    let options = this.data.options || {}, formType = options.type || '', formData = this.data.formData

    if (formType == 'class') {

      //this.classTypeChanged(idx)

    } else if (formType == 'homework') {

      if (idx == app.http.form.getFormItemIndex(formData, 'course')) { // 课程修改，班级也需要修改
        this.hmworkCourseChanged(idx, value)
      }

    } else if (formType == 'exam') {

      if (idx == app.http.form.getFormItemIndex(formData, 'courseName')) { // 课程修改，班级也需要修改
        this.examCourseChanged(idx, value)
      }
      else if (idx == app.http.form.getFormItemIndex(formData, 'cid')) { // 班级变化
        this.examClassChanged(idx, value)
      }

    }

  },

  /********** RatingItems ********************************************/
  bindAddRatingItems: function (e) {

    let idx = e.currentTarget.dataset.idx
    app.navigateTo('/pages/utility/rating/rating?index=' + idx)

  },

  setRatingItems: function (idx, ids, items) {

    let formItem = this.data.formData[idx], str = 'formData[' + idx + ']', param = {}

    formItem.value = ids || []
    formItem.items = items || []
    param[str] = formItem
    this.setData(param)

  },

  /********** Rating ********************************************/
  ratingMultiplierChange: function (e) {

    let idx = e.currentTarget.dataset.idx, formItem = this.data.formData[idx],
      str = 'formData[' + idx + ']', param = {}

    formItem.multiplier = e.detail.value ? 1 : -1
    formItem.value = Math.abs(formItem.value) * formItem.multiplier

    param[str] = formItem
    this.setData(param)

  },

  bindRating: function (e) {

    let id = e.currentTarget.dataset.id, idx = e.currentTarget.dataset.idx, formItem = this.data.formData[id],
      value = Math.abs(formItem.value), star = formItem.stars[idx], str = 'formData[' + id + '].value', param = {}

    value = (star == value) ? (value - 1) : star
    param[str] = value * formItem.multiplier
    this.setData(param)

  },

  bindRatingMove: function (e) {

    let id = e.currentTarget.dataset.id, formItem = this.data.formData[id], value = Math.abs(formItem.value), touches = e.touches,
      str = 'formData[' + id + '].value', param = {}

    if (touches.length > 0) {

      let idx = Math.min(parseInt(touches[0].pageX / 30), formItem.stars.length - 1), star = formItem.stars[idx]
      if (star != value) {
        value = star
        param[str] = value * formItem.multiplier
        this.setData(param)
      }

    }

  },

  /********** Date ********************************************/
  bindDateChange: function (e) {

    let idx = e.currentTarget.dataset.idx, formItem = this.data.formData[idx],
      str = 'formData[' + idx + ']', param = {}

    if (formItem.type == 'datetime') {
      formItem.date = e.detail.value
      formItem.value = formItem.date + ' ' + (formItem.time || '')
    } else {
      formItem.value = e.detail.value
    }

    param[str] = formItem
    this.setData(param)

  },

  /********** Time ********************************************/
  bindTimeChange: function (e) {

    let idx = e.currentTarget.dataset.idx, formItem = this.data.formData[idx],
      str = 'formData[' + idx + ']', param = {}

    if (formItem.type == 'datetime') {
      formItem.time = e.detail.value
      formItem.value = (formItem.date || '') + ' ' + formItem.time
    } else {
      formItem.value = e.detail.value
    }

    param[str] = formItem
    this.setData(param)
  },

  /********** Contact ********************************************/
  // 添加联系人
  bindAddContact: function (e) {

    let idx = e.currentTarget.dataset.idx, formItem = this.data.formData[idx], options = this.data.options || {},
      formType = options.type || '', param = {}

    if (formItem.inalterable) { // 不能变更
      return
    }

    param.index = idx
    param.multiple = (formItem.type == 'contact' || formItem.type == 'receiver') ? 1 : 2
    param.type = (formItem.type == 'contact' || formItem.type == 'contacts') ? 'contacts' : 'receiver'
    param.group = formItem.group || 'student'

    if (formType == 'schoolBased') {
      app.navigateTo('/pages/utility/contact/search', param)
    } else {
      app.navigateTo('/pages/utility/contact/contact', param)
    }

  },
  // 删除联系人
  bindDelContact: function (e) {

    let idx = e.currentTarget.dataset.idx, subIdx = e.currentTarget.dataset.sub, formData = this.data.formData, formItem = formData[idx],
      str = 'formData[' + idx + ']', param = {}

    if (formItem.inalterable) {
      app.toastErr('不能删除！')
      return
    }

    if (formItem.type == 'contact' || formItem.type == 'receiver') {
      formItem.value = ''
      formItem.contacts = []
    } else {
      formItem.value.splice(subIdx, 1)
      formItem.contacts.splice(subIdx, 1)
    }
    param[str] = formItem

    if (formItem.type == 'receiver' || formItem.type == 'receivers') {
      wx.util.extend(param, this.delUserbindIds(app.http.form.getFormItemIndex(formData, formItem.bindName), formItem.type, subIdx))
    }
    this.setData(param)

  },
  // 获取联系人列表
  getContacts: function (idx) {

    let formData = this.data.formData
    return idx < formData.length ? formData[idx].contacts : []

  },
  // 设置联系人列表
  setContacts: function (idx, contacts) {

    let formData = this.data.formData, formItem = idx < formData.length ? formData[idx] : null,
      values = wx.util.objectsValue(contacts, 'contactid'), str = 'formData[' + idx + ']', param = {};

    if (formItem) {

      contacts = this.uniqContacts(contacts);
      param[str + '.value'] = (formItem.type == 'contact' || formItem.type == 'receiver') ? (values.length > 0 ? values[0] : '') : values
      param[str + '.contacts'] = contacts

      if (formItem.type == 'receiver' || formItem.type == 'receivers') {
        wx.util.extend(param, this.addUserbindIds(app.http.form.getFormItemIndex(formData, formItem.bindName), formItem.type, contacts))
      }

      this.setData(param)

    }

  },
  // 去重
  uniqContacts: function (contacts) {

    let formType = this.data.options.type || '', arr = (formType == 'message') ? [] : contacts, ids = [];

    if (formType == 'message') {

      contacts.forEach(function (item) {

        if (!wx.util.isContained(ids, item.userbindId)) {
          arr.push(item);
          ids.push(item.userbindId);
        }

      });

    }

    return arr;

  },
  // 添加私信接收人 userbindId
  addUserbindIds: function (idx, _type, contacts) {

    let formItem = idx >= 0 ? this.data.formData[idx] : null, str = 'formData[' + idx + '].value', param = {}

    formItem && (param[str] = wx.util.objectsValue(contacts, 'userbindId'))
    return param

  },
  // 删除私信接收人 userbindId
  delUserbindIds: function (idx, _type, subIdx) {

    let formItem = idx >= 0 ? this.data.formData[idx] : null, str = 'formData[' + idx + '].value', param = {}

    formItem && formItem.value.splice(subIdx, 1) && (param[str] = formItem.value)
    return param

  },
  // 联系人详情
  getContactDetail: function (idx) {

    let that = this, formItem = this.data.formData[idx], cid = formItem.type == 'contact' ? formItem.value : formItem.value[0],
      str = 'formData[' + idx + ']', param = {}

    app.showLoading(true, that)
    app.http.ajxGetContactDetail(cid, function (res) {

      if (res && res.data) {
        let detail = res.data.detail
        formItem.contacts[0] = { name: detail.name || '\u3000', avatar: detail.avatar || '' }
        param[str] = formItem
        that.setData(param)
      }

    }, null, function () {
      app.showLoading(false, that)
    })

  },


  /********** 年级 ********************************************/
  bindGetGrade: function (e) {

    let idx = e.currentTarget.dataset.idx, formItem = this.data.formData[idx], param = {}

    param.index = idx
    param.multiple = formItem.type == 'grades' ? 1 : null
    param.action = 'getGrade'

    app.navigateTo('./misc', param)

  },

  getGrades: function (group, index) {

    let formItem = this.data.formData[index]

    return formItem.value

  },

  setGrades: function (group, index, value, info) {

    let str = 'formData[' + index + ']', param = {}

    param[str + '.value'] = value
    param[str + '.info'] = info
    this.setData(param)

  },

  /********** file ********************************************/
  addFiles: function (e) {
    app.http.form.addFiles(e)
  },
  deleteFile: function (e) {
    app.http.form.deleteFile(e, this)
  },

  /********** images ********************************************/
  addImage: function (e) {

    let that = this, idx = e.currentTarget.dataset.idx
    app.http.form.addImage(idx, this, function (len) {
      that.imageAddHookHandler && that.imageAddHookHandler(idx, len)
    })

  },
  deleteImage: function (e) {

    let that = this, idx = e.currentTarget.dataset.idx, subIdx = e.currentTarget.dataset.sub
    app.http.form.deleteImage(idx, subIdx, this, function () {
      that.imageDeleteHookHandler && that.imageDeleteHookHandler(idx, subIdx)
    })

  },
  previewImg: function (e) {

    let idx = e.currentTarget.dataset.idx, sub = e.currentTarget.dataset.sub, formItem = this.data.formData[idx], images = []

    if (formItem.type == 'image' || formItem.type == 'images') {
      let files = formItem.files
      for (var i in files) {
        images.push(files[i].url)
      }
    }

    (images.length > 0) && wx.wrap.previewImg(sub < images.length ? images[sub] : images[0], images)

  },
  /********** image cover ********************************************/
  addImageCover: function (e) {

    let that = this, idx = e.currentTarget.dataset.idx
    app.http.form.addImage(idx, this)

  },
  /********** image handler *******************************************/
  // 图片添加
  imageAddHookHandler: function (idx, len) {

    let options = this.data.options || {}, formType = options.type || ''

    if (formType == 'rating') {
      this.ratingImagesAdded(idx, len)
    }

  },
  // 图片删除
  imageDeleteHookHandler: function (idx, subIdx) {

    let options = this.data.options || {}, formType = options.type || ''

    if (formType == 'rating') {
      this.ratingImagesDeleted(idx, subIdx)
    }

  }


})