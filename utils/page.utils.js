let pageUtils = {

  /**
   * page load handler
   * 设置导航颜色和标题
   * @param page : page reference
   * @param title : title
   */
  onLoadHandler: function(page, title) {

    let app = getApp(),
      theme = app.theme(),
      color = app.getThemeColor(theme);

    // 导航栏颜色
    color && wx.wrap.setNavigationBarColor(color);

    // 导航栏和tabBar颜色
    page = page || wx.util.getLastPageOf(0);
    page && page.setData({
      theme: theme
    });

    // 标题
    title && wx.wrap.setNavigationBarTitle(title);
    title && (page.title = title);

  },

  /**
   * page init handler
   * @param page: 页面引用
   * @param goHome: 是否显示回首页按钮
   * @param addible: 是否显示新增按钮
   * @param formType: 新增类型
   * @param formData: 新增参数
   */
  onInitHandler: function(page, goHome, addible, formType, formData) {

    let app = getApp();

    page = page || wx.util.getLastPageOf(0);

    if (page) {

      page.setData({
        goBack: true,
        goHome: goHome || 0,
        addible: addible || 0
      });

      // 返回上一页
      page.goBack = function() {
        getApp().back();
      };

      // 返回首页
      goHome && (page.goHome = function() {

        // 分享或公众号 进入
        (getApp().isNeedRelogin() || this['__sceneValue'] && this['__sceneValue']['scene_v']) ? getApp().reLogin(): getApp().goHome();

      });

      // 新增
      addible && formType && (page.goForm = function() {

        getApp().goForm(formType, formData);

      });

    }

  },

  /**
   *  搜索初始化
   *  @param page: 页面引用
   *  @param options: 搜索配置
   *    -showSearchButton: 是否显示搜索按钮
   *    -keywords: 关键字
   *    -placeholder: 输入框placeholder
   *    -focus: 获取焦点
   *    -show: 是否显示
   *    -result: 搜索结果保存
   *    -isEmpty: 结果是否为空
   */
  initSearch: function(page, options) {

    let that = this,
      app = getApp(),
      search = {
        showSearchButton: true, // 是否显示搜索按钮
        keywords: '',
        placeholder: '请输入搜索内容',
        focus: false,
        show: false,
        result: [],
        isEmpty: false
      };

    page = page || wx.util.getLastPageOf(0);

    if (page) {

      options && wx.util.extend(search, options);
      page.setData({
        search: search
      });

      // 重置搜索项
      page.resetSearch = function(show) {

        this.setData({
          'search.show': show,
          'search.result': [],
          'search.keywords': '',
          'search.focus': true,
          'search.isEmpty': false
        });

      };

      // 是否已显示搜索
      page.isSearching = function() {

        return this.data.search && this.data.search.show;

      };

      // 显示搜索
      page.bindShowSearch = function() {

        this.resetSearch(true);
        getApp().page.showLoadingMore(this, false);
        this.searchShowedHandler && this.searchShowedHandler();

      };

      // 输入框事件
      page.searchInput = function(e) {

        this.data.search.keywords = e.detail.value;
        this.searchInputHandler && this.searchInputHandler(e.detail.value);

      };

      // 输入框确定事件
      page.bindSearch = function(e) {

        this.data.search.keywords = e.detail.value;

        if (this.searchHandler) {
          this.searchHandler();
        } else {
          this.onPullDownRefresh && this.onPullDownRefresh();
        }

      };

      // 搜索按钮事件
      page.bindConfirmSearch = function(e) {

        if (this.searchHandler) {
          this.searchHandler();
        } else {
          this.onPullDownRefresh && this.onPullDownRefresh();
        }

      };

      // 取消按钮事件
      page.bindSearchCancel = function() {

        this.resetSearch(false);

        if (this.searchCancelHandler) {

          this.searchCancelHandler();

        } else {

          if (this.data.list && this.data.list.length == 0) {
            this.onPullDownRefresh && this.onPullDownRefresh();
          } else {
            getApp().page.showLoadingMore(this, this.data.page.pageno < this.data.page.pagemax);
          }

        }

      };

      // 清空搜索框
      page.bindClearSearch = function() {

        this.setData({
          'search.keywords': '',
          'search.focus': true
        });
        this.searchInputHandler && this.searchInputHandler('');

      };

    }

  },

  /**
   * 搜索+list
   * @param page: 页app.page.initSearchWithTabs()面引用
   */
  initGetterWithList: function(page) {

    let app = getApp();

    page = page || wx.util.getLastPageOf(0);

    if (page) {

      // 获取当前数据列表
      page.getListOf = function(index) {

        let list = (typeof this.isSearching == "function") && this.isSearching() ? this.data.search.result : this.data.list;
        return index == null ? list : (index < list.length ? list[index] : null);

      };

      // 获取当前数据列表 字串
      page.getListStrOf = function(index) {

        let str = (typeof this.isSearching == "function") && this.isSearching() ? 'search.result' : 'list';
        return index == null ? str : (str + '[' + index + ']');

      };

      // 是否为空 字串
      page.getEmptyStr = function() {

        return (typeof this.isSearching == "function") && this.isSearching() ? 'search.isEmpty' : 'isEmpty';

      };

    }

  },

  /**
   * 搜索+tabs
   * @param page: 页面引用
   */
  initGetterWithTabs: function(page) {

    let app = getApp();

    page = page || wx.util.getLastPageOf(0);

    if (page) {

      // 获取当前tab
      page.getTabOf = function(tabIdx) {

        tabIdx = (tabIdx == null) ? this.data.curTab : tabIdx;
        return this.data.tabs[tabIdx];

      };

      // 获取当前tab 字串
      page.getTabStrOf = function(tabIdx) {

        tabIdx = (tabIdx == null) ? this.data.curTab : tabIdx;
        return 'tabs[' + tabIdx + ']';

      };

      // return tabs[index][key]
      page.getTabDataOf = function(key, tabIdx) {

          let data = this.getTabOf(tabIdx) || {};
          return key ? data[key] : null;

        },

        // tabs[index][key].toString
        page.getTabDataStrOf = function(key, tabIdx) {

          return this.getTabStrOf(tabIdx) + '.' + key;

        },

        // tabs[index][key] = value
        page.setTabDataOf = function(key, value, tabIdx, render) {

          let data = {},
            str = this.getTabDataStrOf(key, tabIdx);

          data[str] = value;
          render && this.setData(data);
          return data;

        },

        // 获取当前数据列表
        page.getListOf = function(tabIdx, index) {

          let list = (typeof this.isSearching == "function") && this.isSearching() ? this.data.search.result : this.getTabDataOf('list', tabIdx);
          return index == null ? list : (index < list.length ? list[index] : null);

        };

      // 获取当前数据列表 字串
      page.getListStrOf = function(tabIdx, index) {

        let str = (typeof this.isSearching == "function") && this.isSearching() ? 'search.result' : this.getTabDataStrOf('list', tabIdx);
        return index == null ? str : (str + '[' + index + ']');

      };

      // 是否为空 字串
      page.getEmptyStr = function(tabIdx) {

        return (typeof this.isSearching == "function") && this.isSearching() ? 'search.isEmpty' : this.getTabDataStrOf('isEmpty', tabIdx);

      };

      page._scrollTop = {};
      page.scrollTop = function(tabIdx, value) {

        tabIdx = (tabIdx == null) ? (this.data.curTab || 0) : tabIdx;
        this._scrollTop[tabIdx] = value == null ? this._scrollTop[tabIdx] : value;
        return this._scrollTop[tabIdx];

      }

    }

  },

  /**
   * 初始化 筛选 -单条件筛选
   * @param page
   * @param param
   */
  initPopSelect: function(page, param) {

    page = page || wx.util.getLastPageOf(0);
    page && param && page.setData(param);

    // 显示
    page && !page.showPopSel && (page.showPopSel = function(data) {

      data = data || {};
      data['popSel.show'] = true;
      data['popSel.ani'] = true;
      this.setData(data);

    });

    // 隐藏
    page && !page.hidePopSel && (page.hidePopSel = function(e, data) {

      data = data || {};
      data['popSel.show'] = false;
      this.setData(data);

    });

    // 阻止冒泡
    page && !page.popSelIdle && (page.popSelIdle = function() {

    });

  },

  /**
   * 初始化 弹出输入框
   * @param page
   * @param options
   */
  initPrompt: function(page, options) {

    let prompt = {
      show: false,
      placeholder: '',
      'type': 'text',
      value: '',
      id: ''
    };

    page = page || wx.util.getLastPageOf(0);


    if (page) {

      options && wx.util.extend(prompt, options);
      page.setData({
        prompt: prompt
      });

      // 显示或隐藏 prompt
      page.showPrompt = function(show, options) {

        let prompt = this.data.prompt || {};

        options && wx.util.extend(prompt, options);
        prompt.show = show;
        this.setData({
          prompt: prompt
        });

      };

      /**
       * 隐藏
       * @param e
       */
      page.hidePrompt = function(e) {

        let act = e.currentTarget.dataset.act,
          id = e.currentTarget.dataset.id;

        this.showPrompt(false);
        (act == 'confirm') && this.promptConfirmHandler && this.promptConfirmHandler(id, util.trim(this.data.prompt.value));

      };

      // 输入事件
      page.promptInput = function(e) {

        this.data.prompt.value = e.detail.value;

      };

      /**
       * 获取value
       * @returns {string}
       */
      page.getPromptValue = function() {

        return this.data.prompt.value || '';

      };

    }

  },

  /**
   * 初始化 筛选 -多条件筛选
   * @param page
   * @param param
   */
  initFilters: function(page, param) {

    page = page || wx.util.getLastPageOf(0);
    page && param && page.setData(param);

    // 显示筛选视图
    page.showFilters = function(data) {

      let str = this.getFilterStr && this.getFilterStr() || 'filter';

      data = data || {};
      data[str + '.show'] = true;
      this.setData(data);

    };
    // 隐藏筛选视图
    page.hideFilters = function(data) {

      let str = this.getFilterStr && this.getFilterStr() || 'filter';

      data = data || {};
      data[str + '.show'] = false;
      this.setData(data);

    };
    // 筛选背景点击事件
    page.bindHideFilters = function() {

      this.hideFilters();

    };
    // 筛选视图点击事件
    page.bindFilterIdle = function() {

    };
    // 确定事件
    page.bindCfmFilters = function() {

      this.hideFilters();
      this.filterCfmHandler && this.filterCfmHandler();

    };
    // 清空选择项
    page.resetFilterOptions = function(options) {

      options.forEach(function(item) {
        item.checked = false;
      });

    };
    // 更新选择项
    page.getFilterResults = function(filters) {

      let results = [];

      filters.forEach(function(filter) {

        filter.options.forEach(function(item) {
          item.checked && results.push(item.name);
        })

      });

      return results;

    };
    // 筛选项点击事件
    page.bindFilterOptionTap = function(e) {

      let dataset = e.currentTarget.dataset,
        t = dataset.t,
        idx = dataset.idx,
        sub = dataset.sub,
        value = [],
        filter = this.getFilter && this.getFilter() || this.data.filter,
        obj = filter.filters[idx],
        checked = !obj.options[sub].checked,
        results = '',
        str = this.getFilterStr && this.getFilterStr() || 'filter',
        param = {};

      if (t == 'radio') {
        this.resetFilterOptions(obj.options);
      }
      obj.options[sub].checked = checked;
      obj.options.forEach(function(item) {
        item.checked && value.push(item.id);
      });
      obj.value = value;

      results = this.getFilterResults(filter.filters);

      filter.results && (param[str + '.results'] = results);
      param[str + '.filters[' + idx + ']'] = obj;
      param[str + '.name'] = results.join(',');
      this.setData(param);

      this.filterOptionTapHandler && this.filterOptionTapHandler();

    };

    // 清空筛选项
    page.bindClearFilters = function() {

      let that = this,
        filter = this.getFilter && this.getFilter() || this.data.filter,
        str = this.getFilterStr && this.getFilterStr() || 'filter',
        param = {};

      filter.results = [];
      filter.name = '';
      filter.filters.forEach(function(item) {
        item.value = [];
        item.open = 0;
        that.resetFilterOptions(item.options);
      });
      param[str] = filter;
      this.setData(param);

    };
    // 展开或折叠筛选列表
    page.bindFilterTitleTap = function(e) {

      let idx = e.currentTarget.dataset.idx,
        filter = (this.getFilter && this.getFilter() || this.data.filter).filters[idx],
        str = (this.getFilterStr && this.getFilterStr() || 'filter') + '.filters[' + idx + '].open',
        param = {};

      param[str] = 1 - filter.open;
      this.setData(param);

    }

  },

  /**
   * 数据加载前 设置 pageno, pagesize 信息, 并判断 pagemax
   * @param page
   * @param data
   * @param refresh
   * @param cb
   */
  loadingBeforeHandler: function(page, data, refresh, cb) {

    let param = null,
      result = true;

    if (page) {

      param = data || {};
      if (refresh) {

        param['pageno'] = 1;
        param['pagesize'] = page.pagesize;

      } else if (page.pageno >= page.pagemax) {

        result = false;

      } else {

        page.pageno++;
        param['pageno'] = page.pageno;
        param['pagesize'] = page.pagesize;

      }

    }

    typeof cb == "function" && cb(result, param || data)

  },

  /**
   * 数据加载后 更新 pagemax 信息
   * @param page
   * @param response
   * @returns {*|boolean}
   */
  loadingAfterHandler: function(page, response) {

    if (page && response) {

      response.pageno && (page.pageno = response.pageno);
      page.pagemax = response.pagemax || 1;

    }

    return (page && page.pageno < page.pagemax) || false;

  },

  /**
   * 显示加载更多
   * @param show
   */
  showLoadingMore: function(page, show) {

    page = page || wx.util.getLastPageOf(0);
    page && page.setData({
      hasMore: show
    });

  },

  /**
   * 判断分享的文章是否和当前身份是同一个班级
   * @param page
   * @returns {boolean|*}
   */
  isSameClass: function(page) {

    let app = getApp(),
      _page = page || wx.util.getLastPageOf(0),
      sceneValue = _page && _page['__sceneValue'] || {},
      scene_v = sceneValue['scene_v'],
      scene_i = sceneValue['scene_i'];

    return !scene_v || (scene_i && scene_i == app.userData.getClassId());

  },


  /**
   * 保存分享参数
   * @param options
   * @param page
   */
  saveShareValue: function(options, page) {

    let app = getApp(),
      _page = page || wx.util.getLastPageOf(0),
      data = options || {},
      scene_v = data.scene_v || '',
      scene_i = data.scene_i || 0;

    if (_page && scene_v) {
      _page['__sceneValue'] = {
        scene_v: scene_v,
        scene_i: scene_i
      };
    }

  },

  /**
   * 创建分享参数
   * @param page
   * @returns {string}
   */
  shareSceneValue: function(page) {

    let app = getApp(),
      _page = page || wx.util.getLastPageOf(0),
      sceneValue = {
        scene_v: 'share',
        scene_i: app.userData.getClassId() || 0
      };

    _page && (wx.util.extend(sceneValue, _page['__sceneValue'] || {}));

    return 'scene_v=' + sceneValue['scene_v'] + '&scene_i=' + sceneValue['scene_i'];

  }

};

module.exports = {

  page: pageUtils

};