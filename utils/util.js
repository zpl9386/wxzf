// const formatTime = date => {
//   const year = date.getFullYear()
//   const month = date.getMonth() + 1
//   const day = date.getDate()
//   const hour = date.getHours()
//   const minute = date.getMinutes()
//   const second = date.getSeconds()

//   return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
// }

// const formatNumber = n => {
//   n = n.toString()
//   return n[1] ? n : '0' + n
// }

// module.exports = {
//   formatTime: formatTime
// }

(() => {

  // 常用wx.fn 封装
  wx.wrap = {

    /**
     * 获取微信用户信息
     */
    getUserInfo: function (fnSucc, fnFail) {

      //调用接口
      wx.getUserInfo({

        success: function (res) {

          typeof fnSucc == "function" && fnSucc(res);

        },
        fail: function () {

          typeof fnFail == "function" && fnFail();

        }

      })

    },

    /**
     * 获取用户的当前设置
     */
    getSetting: function (fnSucc, fnFail, fnErr) {

      if (wx.getSetting) { // 兼容测试

        wx.getSetting({
          success: (res) => {
            typeof fnSucc == "function" && fnSucc(res.authSetting);
          },
          fail: (res) => {
            typeof fnFail == "function" && fnFail('wx.getSetting接口调用失败!');
          }
        });

      } else { // 接口不可用

        typeof fnErr == "function" && fnErr('wx.getSetting接口不可用，请更新微信版本!');

      }

    },

    /**
     * 授权界面
     */
    openSetting: function (scope, fnSucc, fnFail, fnErr) {

      if (wx.openSetting) { // 兼容测试

        wx.openSetting({

          success: (res) => {
            if (res.authSetting[scope]) { // 授权
              typeof fnSucc == "function" && fnSucc();
            }
          },
          fail: (res) => {
            typeof fnFail == "function" && fnFail('wx.openSetting接口调用失败!');
          }

        })

      } else { // 接口不可用

        typeof fnErr == "function" && fnErr('wx.openSetting接口不可用，请更新微信版本!');

      }

    },

    /**
     * 兼容测试
     * @param f
     * @returns {boolean}
     */
    canIUse: function (f) {

      return wx.canIUse ? wx.canIUse(f) : false;

    },

    /**
     * 拨打电话
     */
    makePhoneCall: function (phone) {

      wx.makePhoneCall({
        phoneNumber: phone
      });

    },

    /**
     * showToast
     * @param message
     */
    showToast: function (message, icon, image, duration) {

      let data = {};

      data.title = message || '';
      data.icon = icon || 'none';
      image && (data.image = image);
      duration && (data.duration = duration);
      wx.showToast(data);

    },

    /**
     * 设置标题
     * @param title
     */
    setNavigationBarTitle: function (title) {

      wx.setNavigationBarTitle({
        title: title || ''
      });

    },

    /**
     * 设置导航栏 颜色
     * @param color
     */
    setNavigationBarColor: function (color) {

      wx.setNavigationBarColor && wx.setNavigationBarColor({

        frontColor: '#ffffff',
        backgroundColor: color,
        animation: null

      });

    },

    /**
     * navigateTo
     * @param url
     * @param fnSucc
     * @param fnFail
     * @param fnComp
     */
    navigateTo: function (url, fnSucc, fnFail, fnComp) {

      wx.navigateTo({
        url: url,
        success: fnSucc,
        fail: fnFail,
        complete: fnComp
      });

    },

    /**
     * redirectTo
     * @param url
     * @param fnSucc
     * @param fnFail
     * @param fnComp
     */
    redirectTo: function (url, fnSucc, fnFail, fnComp) {

      wx.redirectTo({
        url: url,
        success: fnSucc,
        fail: fnFail,
        complete: fnComp
      });

    },

    /**
     * switchTab
     * @param url
     * @param fnSucc
     * @param fnFail
     * @param fnComp
     */
    switchTab: function (url, fnSucc, fnFail, fnComp) {

      wx.switchTab({
        url: url,
        success: fnSucc,
        fail: fnFail,
        complete: fnComp
      });

    },

    /**
     * navigateBack
     * @param num
     */
    back: function (num) {

      wx.navigateBack({
        delta: num ? parseInt(num) : 1
      });

    },

    /**
     * 图片预览
     * @param current
     * @param urls
     */
    previewImg: function (current, urls) {

      wx.previewImage({
        current: current,
        urls: urls
      });

    },

    /**
     * wx.scanCode 封装
     * @param success
     * @param fail
     * @param complete
     */
    scanQRcode: function (success, fail, complete) {

      wx.scanCode({

        success: success,
        fail: fail,
        complete: complete

      });

    },

    /**
     * 图片选取
     * @param count
     * @param source
     * @param size
     * @param fnSucc
     * @param fnFail
     */
    chooseImage: function (count, source, size, fnSucc, fnFail) {

      let sourceType = [],
        sizeType = [];

      // 相册或相机
      if (source == 'album' || source == 'camera') {
        sourceType[0] = source;
      } else {
        sourceType = ['album', 'camera'];
      }

      // 原图或压缩
      if (size == 'original' || size == 'compressed') {
        sizeType[0] = size;
      } else {
        sizeType = ['original', 'compressed'];
      }

      wx.chooseImage({
        count: count || 1, // 默认9
        sizeType: sizeType, // 可以指定是原图还是压缩图，默认二者都有
        sourceType: sourceType, // 可以指定来源是相册还是相机，默认二者都有
        success: function (res) {
          // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
          typeof fnSucc == "function" && fnSucc(res.tempFilePaths)
        },
        fail: function () {
          typeof fnFail == "function" && fnFail('图片选取失败');
        }
      });

    },

    /**
     * 保存文件
     * @param filePath
     * @param fn
     * @param fail
     * @param fnComp
     */
    saveFile: function (filePath, fn, fail, fnComp) {

      wx.saveFile({

        tempFilePath: filePath,
        success: function (res) {
          typeof fn == "function" && fn(res.savedFilePath);
        },
        fail: function () {
          typeof fail == "function" && fail();
        },
        complete: function () {
          typeof fnComp == "function" && fnComp();
        }

      })

    },

    /**
     * 打开本地文件
     * @param filePath
     * @param fn
     * @param fail
     * @param fnComp
     */
    openDocument: function (filePath, fn, fail, fnComp) {

      wx.openDocument({

        filePath: filePath,
        success: function () {
          typeof fn == "function" && fn();
        },
        fail: function () {
          typeof fail == "function" && fail();
        },
        complete: function () {
          typeof fnComp == "function" && fnComp();
        }

      })

    },

    /**
     * 删除本地文件
     * @param filePath
     * @param fn
     * @param fail
     * @param fnComp
     */
    removeSavedFile: function (filePath, fn, fail, fnComp) {

      wx.removeSavedFile({

        filePath: filePath,
        success: function () {
          typeof fn == "function" && fn()
        },
        fail: function () {
          typeof fail == "function" && fail()
        },
        complete: function () {
          typeof fnComp == "function" && fnComp()
        }

      })

    },

    /**
     * 获取本地文件列表
     * @param fn
     * @param fail
     * @param fnComp
     */
    getSavedFileList: function (fn, fail, fnComp) {

      wx.getSavedFileList({

        success: function (res) {
          typeof fn == "function" && fn(res.fileList)
        },
        fail: function () {
          typeof fail == "function" && fail()
        },
        complete: function () {
          typeof fnComp == "function" && fnComp()
        }

      })

    }

  };

  // util 封装
  wx.util = {

    // 基础库比较
    compareVersion: function (v1, v2) {

      v1 = v1.split('.');
      v2 = v2.split('.');

      let len = Math.max(v1.length, v2.length);

      while (v1.length < len) {
        v1.push('0');
      }
      while (v2.length < len) {
        v2.push('0');
      }

      for (let i = 0; i < len; i++) {

        let num1 = parseInt(v1[i]);
        let num2 = parseInt(v2[i]);

        if (num1 > num2) {
          return 1;
        } else if (num1 < num2) {
          return -1;
        }
      }

      return 0;

    },

    /**
     * 上{{n}}页
     * @param n
     * @returns {*}
     */
    getLastPageOf: function (n) {

      let pages = getCurrentPages(), len = pages.length, m = n || 0;
      return len > m ? pages[len - m - 1] : null;

    },

    /**
     * 清空画布
     * @param canvasID
     * @param width
     * @param height
     */
    clearCanvas: function (canvasID, width, height) {

      let ctx = wx.createCanvasContext(canvasID);

      if (ctx) {

        ctx.clearRect(0, 0, width, height);
        ctx.draw();

      }

    },

    // 数组中是否包含str
    isContained: function (arr, str) {

      let regx = new RegExp(',' + String(str) + ',');
      return regx.test(',' + arr.toString() + ',');

    },

    // 是否全部选中
    isAllChecked: function (arr) {

      if (!arr || arr.length == 0) {
        return false
      }

      return arr.every(function (item) {
        return item.disabled || item.checked
      })

    },

    // 对象数组 key对应value 的数组
    objectsValue: function (objs, key) {

      let values = [];

      if (objs && key) {
        for (let i in objs) {
          values.push(objs[i][key] || '');
        }
      }
      return values;

    },

    /**
     * object是否相等
     */
    isEqual: function () {

      let length = arguments.length,
        target = length > 0 ? arguments[0] : '',
        src = length > 1 ? arguments[1] : '',
        isEqual = false;

      if (target instanceof Array && src instanceof Array) {

        for (let i in src) {

          isEqual = this.isEqual(target[i], src[i]);
          if (!isEqual) {
            break;
          }

        }

      } else if (target instanceof Object && src instanceof Object) {

        isEqual = JSON.stringify(target) == JSON.stringify(src)

      } else if (target instanceof Array || target instanceof Array || src instanceof Array || src instanceof Array) {

        isEqual = false;

      } else {

        isEqual = String(target) == String(src);

      }

      return isEqual;

    },

    /**
     * get options
     * @param key
     * @param value
     * @param options
     * @returns {{name: string, index: number}}
     */
    getOptionBy: function (key, value, options) {

      let res = {
        name: '',
        index: -1
      };

      for (let i in options) {

        if (options[i][key] == value) {
          res.name = options[i].name, res.index = i;
          break;
        }

      }

      return res;

    },

    /**
     * url 参数格式化
     * @param param
     * @param head
     * @param mid
     */
    encodeParam: function (param, head, mid) {

      let str = '',
        symbolStr = head || '';

      if (param) {

        for (let key in param) {
          str += (symbolStr + key + '=' + param[key]);
          symbolStr = mid || '&';
        }

      }

      return str;

    },

    /**
     * 浅拷贝
     * arg0 target
     * arg1 source
     * arg2 keys
     *
     * 如果是数组,将source追加到target
     * 如果是对象,将source追加或替换到target, 如果指定keys, 则将source中的keys追加或替换到target
     */
    extend: function () {

      let length = arguments.length,
        target = length > 0 ? arguments[0] : '',
        src = length > 1 ? arguments[1] : '',
        keys = length > 2 ? arguments[2] : '';

      if (target && src) {

        if (target instanceof Array && src instanceof Array) { // 数组

          target.push.apply(target, src);

        } else if (target instanceof Object && src instanceof Object) { // obj

          let objs = keys || src,
            isArr = (objs instanceof Array);

          for (let i in objs) {
            let key = isArr ? objs[i] : i;
            target[key] = src[key];
          }

        }

      }

    },

    /**
     * 并集
     * arg0 target
     * arg1 source
     * 将source中值不为空的 替换或追加到target中
     */
    extendByUnion: function () {

      let length = arguments.length;

      if (length > 1) {

        let target = arguments[0],
          src = arguments[1];

        if (target instanceof Object && src instanceof Object) {

          for (let i in src) {
            src[i] && (target[i] = src[i]);
          }

        }

      }

    },

    /**
     * 交集
     * arg0 target
     * arg1 source
     * 将source中值不为空的 替换或追加到target中
     */
    extendByIntersection: function () {

      let length = arguments.length;

      if (length > 1) {

        let target = arguments[0],
          src = arguments[1];
        if (target instanceof Object && src instanceof Object) {

          for (let i in target) {
            target[i] = src[i] ? src[i] : target[i];
          }

        }

      }

    },

    /**
     * 深拷贝
     * arg0 target
     * arg1 source
     * arg2 keys
     */
    copy: function () {

      let length = arguments.length,
        target = length > 0 ? arguments[0] : '',
        src = length > 1 ? arguments[1] : '',
        keys = length > 2 ? arguments[2] : '';

      if (target && src) {

        if (target instanceof Array && src instanceof Array) { // 数组

          target = JSON.parse(JSON.stringify(src));

        } else if (target instanceof Object && src instanceof Object) { // 对象

          let objs = keys || src,
            isArr = (objs instanceof Array);

          for (let i in objs) {

            let key = isArr ? objs[i] : i;
            if (src[key] instanceof Array || src[key] instanceof Object) { // 值是数组、对象

              target[key] = JSON.parse(JSON.stringify(src[key]));

            } else {

              target[key] = src[key];

            }
          }

        }

      }

    },


    ////////字符相关/////////////////////////////
    /**
     * 正则匹配
     * @param strType
     * @returns {*}
     */
    getExp: function (strType) {

      if (!strType) {
        return null;
      }

      switch (strType) {

        case 'safe':
          return /^[\u4E00-\u9FFFa-zA-Z0-9~@#%!_\.+-]+$/;
          break; //安全字符串
        case 'zh':
          return /^[\u4E00-\u9FFF]+$/;
          break; //纯中文
        case 'email':
          return /^[0-9a-z][a-z0-9\._-]{1,}@[a-z0-9-]{1,}[a-z0-9]\.[a-z\.]{1,}[a-z]$/;
          break; //邮箱
        case 'website':
          return /^(http|https|ftp):\/\/[A-za-z0-9\.\-_~@#?=&:\/]+$/;
          break; //网页url
        case 'number':
          return /^[0-9\.]+$/;
          break; //纯数字
        case 'id':
          return /^(\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$/; //身份证
        case 'tel':
          return /^[0-9\#\-\+\s]{6,20}$/;
          break; //电话号码
        case 'phone':
        case 'mobile':
          return /^1[3-9]\d{9}$/;
          break; //手机号码
        case 'username':
          return /^[A-Za-z]{1}[a-zA-Z0-9~@#%!_+-\.]{5,50}$/ && !/^[A-Za-z]$/ && !/^[0-9]$/;
          break; //用户名
        case 'password':
          return /^[A-Za-z]{1}[a-zA-Z0-9~@#%!_+-\.]{5,50}$/;
          break; //密码
        default:
          if (/\/([\s\S]+?)\/([A-Za-z]+)?/.test(strType)) {
            let option = strType.substr(strType.lastIndexOf('/') + 1),
              regexp = strType.substr(1, strType.lastIndexOf('/') - 1);
            return new RegExp(regexp, option);
          } else {
            return new RegExp('^' + strType + '$');
          }
          break;

      }

    },

    /**
     * 格式验证
     * @param strTest
     * @param strType
     * @returns {*}
     */
    test: function (strTest, strType) {

      let EXP = this.getExp(strType);
      return EXP ? EXP.test(strTest) : true;

    },

    /**
     * 删除两边空格
     * @param str
     * @returns {*}
     */
    trim: function (str) {

      if (!str) {

        return str;

      } else if (str.toString().match(/^\s*$/)) { // 全空

        return '';

      } else {

        return str.toString().replace(/(^\s*)|(\s*$)/g, "") || str;

      }

    },

    /**
     * 删除左边空格
     * @param str
     */
    ltrim: function (str) {

      return str && str.toString().replace(/(^\s*)/g, "") || str;

    },

    /**
     * 删除右边空格
     * @param str
     */
    rtrim: function (str) {

      return str && str.toString().replace(/(\s*$)/g, "") || str;

    },

    /**
     * 去除括号
     * @param str
     * @returns {string}
     */
    removeBrace: function (str) {

      return String(str).replace(/\([^\)]*\)/g, "");

    },

    /**
     * 去除尖括号
     * @param str
     * @returns {string}
     */
    removeBrackets: function (str) {

      return String(str).replace(/<[^>]*>/g, "");

    },

    /**
     * 是否有html标签
     * @param str
     * @returns {boolean}
     */
    hasHtmlTag: function (str) {

      return (/<[^>]+>/.test(str));

    },

    /**
     * 转成全角
     * @param str
     * @returns {string}
     */
    toSBCCase: function (str) {

      return str ? String(str).replace(/[ ]{3}/g, '\u3000').replace(/(&nbsp;\s?){2}/g, '\u3000').replace(/(&nbsp;\s)/g, '\u3000').replace(/(\s?&nbsp;\s?)/g, '') : '';

    },

    /**
     * 字符串截取，中间以...显示
     * @param str
     * @param maxLen
     * @returns {string}
     */
    hideString: function (str, maxLen) {

      let len = str.length,
        chars = str.match(/[\u4e00-\u9fa5]/g),
        halfLen = 0;

      maxLen = maxLen || 27;
      chars && (maxLen -= chars.length);
      halfLen = Math.floor((maxLen - 2) / 2);

      return len > maxLen ? (str.substr(0, halfLen) + '...' + str.substr(-halfLen)) : str;

    },

    /**
     * 字符串隐藏
     * headLen: 头部长度
     * endLen: 尾部长度
     * mask:  显示字符
     */
    asterisk: function (str, headLen, endLen, mask) {

      if (!str || (typeof str != 'string') || str.length == 0) {
        return str
      }

      str = str.replace(/(^\s*)|(\s*$)/g, "");
      if (str.length <= headLen + endLen) {
        return str;
      }

      let replacement = '',
        length = str.length - headLen - endLen,
        regexp = new RegExp('^(.{' + headLen + '})(.+)(.{' + endLen + '})$');

      mask = mask || '*';
      for (let i = 0; i < length; i++) {
        replacement += mask;
      }
      return str.replace(regexp, '$1' + replacement + '$3');

    },

    // 简单封装成富文本
    toRichText: function (str) {

      let strArr = str.replace(/[ ]/g, '&nbsp;').replace(/\r/g, "").split('\n'),
        arr = [];

      strArr.forEach(function (ele) {

        !ele && (ele = '<br/>');
        arr.push('<p>' + ele + '</p>');

      });

      return arr.join('');

    },

    /**
     * 去掉秒数
     * @param str
     */
    removeSeconds: function (str) {

      return (!str || (typeof str != 'string') || str.length == 0) ? str : str.replace(/(\d+:\d+):\d+$/, '$1');

    },

    /**
     * 获取年月日
     * @param str
     */
    getDateString: function (str) {

      return (!str || (typeof str != 'string') || str.length == 0) ? str : str.replace(/^(\d+-\d+-\d+)[\s\S]*/, '$1');

    },

    ////////文件相关/////////////////////////////
    /**
     * 文件名
     * @param filePath
     * @returns {string}
     */
    lastPathComponent: function (filePath) {

      return (typeof filePath == 'string') ? (filePath.substr(filePath.lastIndexOf('/') + 1)) : '';

    },

    /**
     * 扩展名
     * @param fileName
     * @returns {string}
     */
    fileExtension: function (fileName) {

      return (typeof fileName == 'string') ? (fileName.replace(/^[\s\S]+?\.([A-Za-z0-9]+)$/, '$1')) : '';

    },

    /**
     * 文件名大小
     * @param size
     * @returns {string}
     */
    formatSize: function (size) {

      let sizeStr = size + 'B';

      if (size > 1024) {
        size /= 1024;
        sizeStr = size.toFixed(1) + 'KB';
      }
      if (size > 1024) {
        size /= 1024;
        sizeStr = size.toFixed(1) + 'MB';
      }
      if (size > 1024) {
        size /= 1024;
        sizeStr = size.toFixed(1) + 'GB';
      }
      if (size > 1024) {
        size /= 1024;
        sizeStr = size.toFixed(1) + 'TB';
      }

      return sizeStr;
    },

    /**
     * 文件名
     * @param fileName
     * @returns {string}
     */
    getFileName: function (fileName) {

      return (typeof fileName == 'string') ? (fileName.replace(/^([\s\S]+)?\.[A-Za-z0-9]+$/, '$1')) : '';

    },

    /**
     * 图片类型判断
     * @param ext
     * @returns {boolean}
     */
    isImageFile: function (ext) {

      return !!(/(jpg|jpeg|png|gif|bmp)$/.test(ext));

    },

    /**
     * 打开文件类型判断
     * @param ext
     * @returns {boolean}
     */
    canOpenFile: function (ext) {

      let result = false;

      if (/(doc|xls|ppt|pdf|docx|xlsx|pptx)$/.test(ext)) {
        result = true;
      }

      return result;

    },

    /**
     * 打开文件
     * @param filePath
     */
    openFile: function (filePath) {

      let that = this, ext = this.fileExtension(filePath);

      if (that.isImageFile(ext)) {

        wx.wrap.previewImg(null, [filePath]);

      } else if (that.canOpenFile(ext)) {

        wx.wrap.openDocument(filePath, null, function () {
          wx.wrap.showToast('打开失败！', '', '/images/warn.png');
        });

      } else {

        wx.wrap.showToast('不支持文件类型！', '', '/images/warn.png');

      }

    },

    ////////日期相关/////////////////////////////
    /**
     * 时间是否过期
     * @param strDate
     * @param days
     * @returns {boolean}
     */
    isExpired: function (strDate, days) {

      let timestamp = (new Date()).getTime(),
        expired = this.getDateTimeStamp(strDate);

      days && (expired += days * 24 * 60 * 60 * 1000);

      return timestamp > expired;

    },

    /**
     * 获取timeStamp
     * @param dateStr
     * @returns {*}
     */
    getDateTimeStamp: function (dateStr) {

      return (typeof dateStr == 'string') ? (Date.parse(dateStr.replace(/-/gi, "/"))) : dateStr;

    },

    /**
     * 格式化日期
     * @param date
     */
    formatTime(date) {

      let year = date.getFullYear();
      let month = date.getMonth() + 1;
      let day = date.getDate();

      let hour = date.getHours();
      let minute = date.getMinutes();
      let second = date.getSeconds();

      return [year, month, day].map(this.formatNumber).join('-') + ' ' + [hour, minute, second].map(this.formatNumber).join(':');

    },

    /**
     * 数字补零
     * @param n
     * @returns {*}
     */
    formatNumber: function (n) {

      n = n.toString();
      return n[1] ? n : '0' + n;

    },

    /**
     * 日期格式化
     * @param date
     * @param format
     */
    dateFormat: function (date, format) {

      let Y = date.getFullYear(),
        y = String(Y).replace(/[\s\S]+(\d{2})$/, '$1'),
        m = date.getMonth() + 1,
        d = date.getDate(),
        h = date.getHours(),
        i = date.getMinutes(),
        s = date.getSeconds(),
        N = date.getMilliseconds(),
        T = date.getTime(),
        W = date.getDay();

      let style = format ? format : 'Y-M-D H:I',
        formats = {

          'Y': Y,
          'y': y,

          'M': this.formatNumber(m),
          'm': m,

          'D': this.formatNumber(d),
          'd': d,

          'P': h > 12 ? 'PM' : 'AM',
          'p': h > 12 ? 'pm' : 'am',

          'A': h > 12 ? '下午' : '上午',

          'H': this.formatNumber(h),
          'h': h > 12 ? h - 12 : h,

          'I': this.formatNumber(i),
          'i': i,

          'S': this.formatNumber(s),
          's': s,

          'W': '\u661f\u671f' + '\u65e5\u4e00\u4e8c\u4e09\u56db\u4e94\u516d'.charAt(W),

          'N': N,

          'T': T
        };

      for (let i in formats) {
        if (new RegExp("(" + i + ")").test(style)) {
          style = style.replace(RegExp.$1, formats[i]);
        }
      }

      return style;

    },

    // date 加减
    addDate: function (date, days) {

      date = date || new Date();
      days = (days == null) ? 1 : days;
      date.setDate(date.getDate() + days);

      return date

    },

    /**
     * 日期格式
     * @param dateStr
     * @returns {{}}
     */
    dateStringFormat: function (dateStr) {

      let publishTimeStamp = this.getDateTimeStamp(dateStr),
        publishTime = new Date(publishTimeStamp),
        nowTime = new Date(),
        nowTimeStamp = nowTime.getTime(),
        diffSeconds = (nowTimeStamp - publishTimeStamp) / 1000,
        fmtTime = '',
        fmtPre = '',
        fmtSuf = '',
        fmtYear = '';

      let one_minute = 60,
        one_hour = one_minute * 60,
        one_day = one_hour * 24,
        dif_minutes = parseInt(diffSeconds / one_minute),
        dif_hours = parseInt(diffSeconds / one_hour),
        dif_days = parseInt(diffSeconds / one_day);

      if (dif_days > 1) {
        fmtPre = this.dateFormat(publishTime, 'D');
        fmtSuf = this.dateFormat(publishTime, 'm月');
      } else if (dif_days > 0) {
        fmtTime = '昨天';
      } else if (dif_hours > 0) {
        fmtPre = dif_hours;
        fmtSuf = '小时前';
      } else if (dif_minutes > 0) {
        fmtPre = dif_minutes;
        fmtSuf = '分钟前';
      } else {
        fmtTime = '刚刚';
      }

      if (publishTime.getFullYear() != nowTime.getFullYear()) {
        fmtYear = this.dateFormat(publishTime, 'y年');
      }

      return {
        time: fmtTime,
        preTime: fmtPre,
        sufTime: fmtSuf,
        year: fmtYear
      };

    }

  };

  // storage 封装
  wx.storage = {

    /**
     * 保存字段列表
     */
    KEYS: {

      PID: 'userId',
      IDENTITYID: 'identityId',
      ADMINID: 'adminId',
      SCHOOLID: 'schoolId',
      ADMININFO: 'adminInfo',
      CACHES: 'caches',
      CAPTCHA: 'captcha',
      MINISDK: 'miniSDK'

    },

    /**
     * 保存数据{key,vale}到storage
     * @param key
     * @param value
     * @param err
     */
    setStorage: function (key, value, err) {

      try {

        wx.setStorageSync(key, value);

      } catch (e) {

        err = err || 0;
        (err < 2) && this.setStorage(key, value, ++err);

      }

    },

    /**
     * 读取storage数据(key)
     * @param key
     * @param err
     * @returns {*}
     */
    getStorage: function (key, err) {

      let data = null;

      try {

        data = wx.getStorageSync(key);

      } catch (e) {

        err = err || 0;
        (err < 2) && (data = this.getStorage(key, ++err));

      }

      return data;
    },

    /**
     * 删除storage数据(key)
     * @param key
     * @param err
     */
    removeStorage: function (key, err) {

      try {

        wx.removeStorageSync(key);

      } catch (e) {

        err = err || 0;
        (err < 2) && this.removeStorage(key, ++err);

      }

    },

    /**
     * 清空storage
     * @param err
     */
    clearStorage: function (err) {

      try {

        wx.clearStorageSync();

      } catch (e) {

        err = err || 0;
        (err < 2) && this.clearStorage(++err);

      }

    },


    /**
     * 保存缓存
     * message, circle
     */
    setCache: function (key, data) {

      let caches = this.getStorage(this.KEYS.CACHES) || {};

      key && (caches[key] = data);
      this.setStorage(this.KEYS.CACHES, caches);

    },

    /**
     * 读取缓存
     */
    getCache: function (key) {

      return key ? (this.getStorage(this.KEYS.CACHES) || {})[key] : null;

    },

    /**
     * 删除缓存
     */
    removeCache: function () {

      return this.removeStorage(this.KEYS.CACHES);

    },


    /**
     * 获取验证码失效时间
     * @returns
     */
    getCaptchaCountdown: function () {

      let cnt = 0, localCnt = parseInt(this.getCaptcha()), timeStamp = (new Date()).getTime();

      if (localCnt && localCnt > timeStamp) {

        cnt = Math.ceil((localCnt - timeStamp) / 1000);

      }

      return cnt;

    },

    // 验证码时间戳
    getCaptcha: function () {

      return this.getStorage(this.KEYS.CAPTCHA) || 0;

    },

    setCaptcha: function (timeStamp) {

      this.setStorage(this.KEYS.CAPTCHA, timeStamp);

    },

    // 管理员账号信息
    getAdminInfo: function () {

      return this.getStorage(this.KEYS.ADMININFO);

    },

    setAdminInfo: function (info) {

      this.setStorage(this.KEYS.ADMININFO, info);

    },

    // userid
    getUserID: function () {

      return this.getStorage(this.KEYS.PID) || 0;

    },

    setUserID: function (id) {

      this.setStorage(this.KEYS.PID, id);

    },

    // identity id
    getIdentityID: function () {

      return this.getStorage(this.KEYS.IDENTITYID) || 0;

    },

    setIdentityID: function (id) {

      this.setStorage(this.KEYS.IDENTITYID, id);

    },

    // admin id
    getAdminID: function () {

      return this.getStorage(this.KEYS.ADMINID) || 0;

    },

    setAdminID: function (id) {

      this.setStorage(this.KEYS.ADMINID, id);

    },

    // school id
    getSchoolID: function () {

      return this.getStorage(this.KEYS.SCHOOLID) || 0;

    },

    setSchoolID: function (id) {

      this.setStorage(this.KEYS.SCHOOLID, id);

    },

    // miniSDK
    getMiniSDK: function () {

      return this.getStorage(this.KEYS.MINISDK) || '';

    },

    setMiniSDK: function (sdk) {

      this.setStorage(this.KEYS.MINISDK, sdk);

    }
  };

})();
