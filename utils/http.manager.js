let httpManager = {

  /**
   * 接口基地址
   */
  hostUrl: 'https://api.zpl9386.com',

  /**
   * 接口基地址
   */
  baseUrl: 'https://api.chinaanno.com',

  /**
   * 图片，附件资源url
   */
  webUrl: 'https://api.chinaanno.com',

  /**
   * 接口版本
   */
  apiVersion: '1.0',

  authorization: '',

  userId: '',

  /**
   * 接口地址列表
   */
  urls: {
    login: '/userinfo_login', // 登陆
    bind: '/userinfo_binding', // 绑定
    bindVerify: '/verification_code', // 绑定_获取验证码
    reqPaperList: '/invitation_list', //首页帖子列表
    reqLeaseList: '/invitation_rent_list', //直租帖子列表
    reqPaperDetail: '/invitation_info/', //帖子详情
    reqPaperReply: '/invitation_reply_list/', //获取帖子留言
    commentPaper: '/invitation_leave_word', //留言
    replyComment: '/invitation_leave_word_reply', //回复留言
    publicPaper: '/invitation_save', //发布帖子
    upload: '/uploading_file', //上传文件
    reqMessage:'/user_invitation_reply_list/',
  },

  /**
   * 初始化
   */
  init: function(hostUrl) {

    // hostUrl && (this.hostUrl = this.webUrl = hostUrl);

  },

  /**
   * 设置baseURL
   * @param baseUrl
   */
  setBaseUrl: function(baseUrl) {

    this.baseUrl = this.webUrl = baseUrl;

  },

  /**
   * 设置接口版本
   * @param apiVersion
   */
  setApiVersion: function(apiVersion) {

    this.apiVersion = apiVersion;

  },

  setAuthorization: function(authorization) {
    this.authorization = authorization;
  },

  setUserId: function(userId) {
    this.userId = userId;
  },

  /**
   * 读取webUrl
   * @returns {string}
   */
  getWebUrl: function() {

    return this.webUrl;

  },

  // 获取正式地址
  getRegularUrl: function() {

    return this.webUrl.replace(/.com.cn$/, '.com').replace(/.cn$/, '.com');

  },

  /**
   * 地址校验
   * @param url
   * @returns {*}
   */
  urlVerify: function(url) {

    if (/^(https|http|ftp|rtsp|mms):\/\//.test(url)) {

      return url;

    } else {

      return this.webUrl + '/' + url.replace(/^\//, '');

    }

  },

  /**
   * 地址校验
   * @param files
   */
  urlsVerify: function(files) {

    for (let i in files) {

      files[i] = this.urlVerify(files[i]);

    }

  },


  //// Http Request //////////////////////////////////////
  /*******************************************************
   *  login 接口
   *  获取用户信息
   *******************************************************/
  /**
   * 获取用户信息
   * @param data: wx.login().code + wx.getUserInfo()
   * @param fnSucc
   * @param fnFail
   * @param fnComp
   */
  login: function(code, fnSucc, fnFail, fnComp) {
    var dataReq = {};
    dataReq.code = code;
    this.POST(this.urls.login, dataReq, fnSucc, fnFail, fnComp);

  },

  bindUser: function(data, fnSucc, fnFail, fnComp) {

    this.POST(this.urls.bind, data, fnSucc, fnFail, fnComp);

  },

  bindVerify: function(data, fnSucc, fnFail, fnComp) {

    this.POST(this.urls.bindVerify, data, fnSucc, fnFail, fnComp);

  },

  /*******************************************************
   *  帖子 接口
   *  获取、发布、详情。。。
   *******************************************************/

  requestPaperlist: function(reqData, fnSucc, fnFail, fnComp) {
    var req = {
      pageIndex: '',
      title: '',
      city: '',
      type: ''
    }
    this.POST(this.urls.reqPaperList, reqData, fnSucc, fnFail, fnComp);
  },

  requestLeaselist: function(reqData, fnSucc, fnFail, fnComp) {
    var req = {
      pageIndex: '',
      title: '',
      city: '',
    }
    this.POST(this.urls.reqLeaseList, reqData, fnSucc, fnFail, fnComp);
  },

  requestPaperDetail: function(id, fnSucc, fnFail, fnComp) {
    var url = this.urls.reqPaperDetail + id;
    this.WX_GET(url, '', fnSucc, fnFail, fnComp);
  },

  reqestPaperReply: function(id, index, fnSucc, fnFail, fnComp) {
    var url = this.urls.reqPaperReply + id + "/" + index;
    this.WX_GET(url, '{}', fnSucc, fnFail, fnComp);
  },

  commentPaper: function(reqData, fnSucc, fnFail, fnComp) {
    console.log(JSON.stringify(reqData));
    this.POST(this.urls.commentPaper, reqData, fnSucc, fnFail, fnComp);
  },

  replyComment: function(reqData, fnSucc, fnFail, fnComp) {
    this.POST(this.urls.commentPaper, reqData, fnSucc, fnFail, fnComp);
  },

  reqMessage: function (pageIndex, fnSucc, fnFail, fnComp){
    var url = this.urls.reqMessage + this.userId + "/" + pageIndex;
    this.WX_GET(url, '{}', fnSucc, fnFail, fnComp);
  },

  publicPaper: function(reqData, images, fnSucc, fnFail, fnComp) {
    //数据示例
    // var req = {
    //   user_id: '',
    //   title: '',
    //   invitation_type: '',
    //   type: '',
    //   rental: '',
    //   latitude: '',
    //   longitude: '',
    //   describe: '',
    //   pictures: '', //拼接
    //   province: '',
    //   city: '',
    //   district: '',
    //   namme: '', //小区名
    //   area: '', //面积
    //   house_type_room: '', //房
    //   house_type_office: '',
    //   house_type_defend: '',
    //   payment_method: '',
    //   orientation: '',
    //   content: '' 
    // }
    var that = this;
    reqData.user_id = that.userId;
    var imgUrl = this.hostUrlVerify(this.urls.upload);
    that.uploadImgs(imgUrl, images, 0, 'data', [], function(imgUrls) {
      if (imgUrls && imgUrls.length > 0) {
        var pictures = "";
        for (var i = 0; i < imgUrls.length; i++) {
          pictures = pictures + that.hostUrl + imgUrls[i];
          if (i < imgUrls.length - 1) {
            pictures += ",";
          }
          // reqData.pictures =reqData.pictures+ that.hostUrl + imgUrls[i] + (i + 1) < imgUrls.length ? "," : "";
        }
        console.log(pictures);
        reqData.pictures = pictures;
        console.log(JSON.stringify(reqData));
        that.POST(that.urls.publicPaper, reqData, fnSucc, fnFail, fnComp);
      } else {
        typeof fnFail == 'function' && fnFail({
          'msg': '上传图片失败'
        });
      }
    }, function(res) {
      typeof fnFail == 'function' && fnFail({
        'msg': '上传图片失败'
      });
    });
  },


  /*******************************************************
   *  main 接口
   *  获取身份列表，菜单配置。。。
   *******************************************************/
  /**
   * 获取身份列表、菜单配置
   * @param id: identityId身份id
   * @param fnSucc
   * @param fnFail
   * @param fnComp
   */



  /*******************************************************
   *  file 接口
   *  云文件相关
   *******************************************************/
  /**
   * 云文件列表
   * @param data -folderid, page
   * @param fnSucc
   * @param fnFail
   * @param fnComp
   */
  ajxGetFileList: function(data, fnSucc, fnFail, fnComp) {

    let param = data || {};

    param['action'] = 'getFileList';
    this.POST(this.urls.file, param, fnSucc, fnFail, fnComp);

  },

  /**
   * 创建文件夹
   * @param id -父目录id
   * @param name -名称
   * @param fnSucc
   * @param fnFail
   * @param fnComp
   */
  ajxAddFolder: function(id, name, fnSucc, fnFail, fnComp) {

    this.POST(this.urls.file, {
      action: 'addFolder',
      folderid: id,
      name: name
    }, fnSucc, fnFail, fnComp);

  },

  /**
   * 删除文件
   * @param ids -文件ids
   * @param fnSucc
   * @param fnFail
   * @param fnComp
   */
  ajxDelFile: function(ids, fnSucc, fnFail, fnComp) {

    this.POST(this.urls.file, {
      action: 'delFile',
      fileids: ids
    }, fnSucc, fnFail, fnComp);

  },

  /**
   * 文件重命名
   * @param id
   * @param name
   * @param fnSucc
   * @param fnFail
   * @param fnComp
   */
  ajxRenameFile: function(id, name, fnSucc, fnFail, fnComp) {

    this.POST(this.urls.file, {
      action: 'rename',
      fileid: id,
      name: name
    }, fnSucc, fnFail, fnComp);

  },

  /**
   * 云文件上传
   * @param id -父目录id
   * @param files -文件列表
   * @param fnSucc
   * @param fnFail
   * @param fnComp
   */
  ajxUploadFile: function(id, files, fnSucc, fnFail, fnComp) {

    let param = {};

    param['action'] = 'upload';
    param['folderid'] = id;
    this.syncUpload(this.urls.file, files, 0, {
      action: 'upload',
      folderid: folderid
    }, function() {

      typeof fnSucc == "function" && fnSucc();
      typeof fnComp == "function" && fnComp();

    }, function() {

      typeof fnFail == "function" && fnFail();
      typeof fnComp == "function" && fnComp();

    });

  },

  /*******************************************************
   *  qrcode 接口
   *  二维码相关
   *******************************************************/
  /**
   * 解析二维码
   * @param qrcode
   * @param fnSucc
   * @param fnFail
   * @param fnComp
   */
  ajxDecodeQRCode: function(qrcode, fnSucc, fnFail, fnComp) {

    this.POST(this.urls.qrcode, {
      qrcode: qrcode
    }, fnSucc, fnFail, fnComp);

  },



  /*******************************************************
   * 表单对象
   *******************************************************/
  form: {

    /**
     * 获取图片上传后的远程地址
     * @param files
     * @returns {Array}
     */
    getImageUrls: function(files) {

      let values = [];

      files && files.forEach(function(file) {

        if (file.value.length > 0) {
          values.push(file.value);
        }

      });

      return values;

    },

    /**
     * 序列化
     * @param formData
     * @returns {{}}
     */
    serialize: function(formData) {

      let that = this,
        result = {};

      formData && formData.forEach(function(item) {

        if (item.type == 'image' || item.type == 'cover') { // 单张图片

          let values = that.getImageUrls(item.files);
          result[item.name] = values.length > 0 ? values[0] : '';

        } else if (item.type == 'images') { // 多张图片

          result[item.name] = that.getImageUrls(item.files);

        } else if (item.type == 'exam' || item.type == 'evaluation') {

          result[item.name] = JSON.stringify(item.value);

        } else if (item.type == 'text' || item.type == 'password' || item.type == 'number' || item.type == 'captcha' || item.type == 'phone') {

          result[item.name] = wx.util.trim(item.value);

        } else if (item.type == 'readonly') {

        } else {

          item.name && (result[item.name] = item.value);

        }

      });

      return result;

    },

    /**
     * 反序列化
     * @param e
     * @param formData
     */
    deserialize: function(e, formData) {

      let values = e.detail.value;

      formData && formData.forEach(function(item) {

        if (item.type == 'text' || item.type == 'password' || item.type == 'number' || item.type == 'captcha' || item.type == 'phone') { // 去除空格

          item.value = wx.util.trim(values[item.name]);

        } else if (item.type == 'editor' || item.type == 'textarea') {

          item.value = values[item.name];

        }

      });

    },

    /**
     * 表单空检查
     * @param formData
     * @returns {boolean}
     */
    emptyCheck: function(formData) {

      let that = this,
        result = true,
        errMsg = '';

      formData && formData.forEach(function(item) {

        if (result) {

          // 非空
          if (item.empty == 0) {
            errMsg = that.formItemEmptyCheck(item);
          }

          // 手机格式验证
          if (item.type == 'phones') {
            errMsg = that.verifyPhones(item);
          }

          result = !!!errMsg;
          errMsg && getApp().showInfoModal('提示', errMsg);

        }

      });

      return result;

    },

    /**
     * 表单项空检查
     * @param item
     * @returns {string}
     */
    formItemEmptyCheck: function(item) {

      let errMsg = '';

      switch (item.type) {

        case 'image': // 图片
        case 'images':
        case 'cover':
          errMsg = (item.files.length > 0) ? '' : (item.title + '不能为空！');
          break;

        case 'captcha': // 验证码
          errMsg = (item.data == 'anno' || item.value == 'anno') ? '' : (item.value.length > 0) && (wx.util.trim(item.value) == item.data) ? '' : (item.title + '不正确！');
          break;

        case 'phone': // 手机号
          errMsg = (item.value.length > 0) ? (wx.util.test(item.value, 'phone') ? '' : (item.title + '不正确！')) : (item.title + '不能为空！');
          break;

        default: // 其它
          errMsg = ((wx.util.trim(String(item.value))).length > 0) ? '' : (item.title + '不能为空！');
          break;

      }

      return errMsg;

    },

    /**
     * 验证手机号
     * @param item
     * @returns {string}
     */
    verifyPhones: function(item) {

      let errMsg = '',
        phones = item.phones || [];

      phones.forEach(function(value) {

        if (!errMsg) {

          errMsg = (value.length > 0) ? (wx.util.test(value, 'phone') ? '' : (item.title + '格式不正确！')) : '';

        }

      });

      return errMsg;

    },

    /**
     * 添加文件
     * @param e
     */
    addFiles: function(e) {

      let idx = e ? e.currentTarget.dataset.idx : null;
      (idx != null) && getApp().navigateTo('/pages/utility/cloud/cloud?action=cloudFiles&index=' + idx);

    },

    /**
     * 选中文件
     * @param files
     * @param index
     */
    setSelectedFiles: function(files, index) {

      let page = wx.util.getLastPageOf(1),
        formData = page ? page.data.formData : null,
        str = 'formData[' + index + ']',
        param = {};

      if (formData && files && files.length > 0) {

        let formItem = formData[index];

        files.forEach(function(file) {

          formItem.files.push({
            id: file.id,
            name: file.name,
            ext: file.ext
          });
          formItem.value.push(file.directory);

        });

        param[str] = data;
        page.setData(param);

      }

    },

    /**
     * 删除文件
     * @param e
     * @param page
     */
    deleteFile: function(e, page) {

      let idx = e.currentTarget.dataset.idx,
        subIdx = e.currentTarget.dataset.sub,
        _page = page || wx.util.getLastPageOf(0),
        formData = _page && _page.data.formData || null,
        str = 'formData[' + idx + ']',
        param = {};

      if (formData) {

        formData[idx].files.splice(subIdx, 1);
        formData[idx].value.splice(subIdx, 1);
        param[str] = formData[idx];

        _page.setData(param);

      }

    },

    /**
     * 添加图片
     * @param idx - 表单项索引
     * @param page
     * @param cb
     */
    addImage: function(idx, page, cb) {

      let app = getApp(),
        _page = page || wx.util.getLastPageOf(0),
        formData = _page && _page.data.formData || null,
        str = 'formData[' + idx + ']',
        param = {};

      if (formData && idx < formData.length) {

        let formItem = formData[idx],
          len = formItem.type == 'images' ? 9 - formItem.files.length : 1;

        if (len == 0) {

          app.showInfoModal('', '最多只能选9张照片');

        } else {

          wx.wrap.chooseImage(len, 'all', 'all', function(urls) {

            if (formItem.type != 'images') {
              formItem.files = [];
            }

            urls && urls.forEach(function(url) {

              formItem.files.push({
                url: url,
                value: ''
              });

            });

            param[str] = formItem;
            _page.setData(param);

            typeof cb == "function" && cb(urls.length);

          });

        }

      }

    },

    /**
     * 删除图片
     * @param idx -表单项索引
     * @param subIdx -图片索引
     * @param page
     * @param cb
     */
    deleteImage: function(idx, subIdx, page, cb) {

      let _page = page || wx.util.getLastPageOf(0),
        formData = _page && _page.data.formData || null,
        str = 'formData[' + idx + ']',
        param = {};

      if (formData && idx < formData.length) {

        let formItem = formData[idx];

        formItem.files.splice(subIdx, 1);
        param[str] = formItem;
        _page.setData(param);

        typeof cb == "function" && cb();

      }

    },

    /**
     * 获取formItem index
     * @param formData
     * @param name
     * @returns {number}
     */
    getFormItemIndex: function(formData, name) {

      let idx = -1;

      for (let i in formData) {

        if (formData[i].name == name) {
          idx = i;
          break;
        }

      }

      return idx;
    }

  },

  // submit
  formSubmit: function(url, formData, e, data, fnSucc, fnFail, fnComp) {

    let that = this,
      app = getApp();

    app.showLoading(true), app.isSubmitting(true);
    // 空检查
    if (that.form.emptyCheck(formData)) {

      // // test
      // if (true) {
      //   console.log(formData);
      //   console.log(that.form.serialize(formData));
      //   typeof fnSucc == "function" && fnSucc({ error: false });
      //   typeof fnComp == "function" && fnComp();
      //   return;
      // }

      // 上传文件
      that.uploadFormFiles(formData, 0, {}, function() {

        let postData = that.form.serialize(formData);

        data && wx.util.extend(postData, data);
        e && (postData.formId = e.detail.formId || 0);

        that.POST(url, postData, function(res) {

          typeof fnSucc == "function" && fnSucc(res);

        }, function(res) {

          app.showInfoModal('提示', res && res.message || '保存失败!');
          typeof fnFail == "function" && fnFail();

        }, function() {

          app.isSubmitting(false), app.showLoading(false);
          typeof fnComp == "function" && fnComp();

        });

      }, function(res) {

        let msg = res && res.message ? res.message : '图片上传失败!';

        app.showInfoModal('提示', msg);
        app.isSubmitting(false), app.showLoading(false);
        typeof fnFail == "function" && fnFail();

      });

    } else {

      app.isSubmitting(false), app.showLoading(false);
      typeof fnFail == "function" && fnFail();

    }

  },

  ///////////Request//////////////////////////////////////////////////////////////////////////////////////////
  /**
   * url 完整校验
   * @param url
   */
  hostUrlVerify: function(url) {

    return this.hostUrl + url;


  },

  /**
   * url 完整校验
   * @param url
   */
  baseUrlVerify: function(url) {

    if (/^(https|http|ftp|rtsp|mms):\/\//.test(url)) {
      return url;
    } else {
      return this.baseUrl + url;
    }

  },

  /**
   * post 请求
   * url： url地址
   * data： 参数
   * fnSucc: 成功回调
   * fnFail: 失败回调
   * fnComp: 完成回调
   */
  POST: function(url, data, fnSucc, fnFail, fnComp) {

    let that = this,
      app = getApp(),
      param = app.getGlobalIds();

    // wx.util.extend(param, data || {});
    // this.WX_POST(url, param, function (res) {
    this.WX_POST(url, data, function(res) {


      res && res.weburl && (that.webUrl = res.weburl);
      if (res && res.error) {

        typeof fnFail == "function" ? fnFail(res) : getApp().toastErr(res && res.message || '数据返回错误!');

      } else if (res && res.purview == false) {

        getApp().goError('权限不足！', app.E_ERROR.AUTHORITY);

      } else {

        typeof fnSucc == "function" && fnSucc(res)

      }

    }, function(res) {

      (typeof fnFail == "function") ? fnFail(res): getApp().toastErr(res && res.message || '数据请求失败!');

    }, fnComp);

  },

  /**
   * POST请求
   * @param url
   * @param data
   * @param fnSucc
   * @param fnFail
   * @param fnComp
   * @constructor
   */
  WX_POST: function(url, data, fnSucc, fnFail, fnComp) {

    let that = this;

    wx.showNavigationBarLoading();
    console.log("reqData" + JSON.stringify(data));
    wx.request({

      url: that.hostUrlVerify(url),
      data: data || {},
      method: 'POST', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
      header: { // 设置请求的 header
        // 'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'content-type': 'application/json; charset=UTF-8',

        // 'apiVersion': this.apiVersion
        'Authorization': this.authorization,
      },
      // success
      success: function(res) {
        console.log("httpPostResponse: " + JSON.stringify(res.data));

        if (res && res.data && res.data.status && res.data.status == 200) {
          try {
            let response = res.data.data_list;
            if (typeof response == 'string') {
              response = response.replace('\ufeff', '');
              response = JSON.parse(response);
              typeof fnSucc == "function" && fnSucc(response);
            } else {
              typeof fnSucc == "function" && fnSucc(response);
            }
          } catch (e) {
            typeof fnSucc == "function" && fnSucc('');
          }
        } else {
          typeof fnFail == "function" && fnFail({
            'error': true,
            code: res.data.code || -1,
            'message': '接口调用失败',
            'msg': res.data.msg || '内部错误',
          });
        }

      },
      // fail
      fail: function(res) {

        console.log(arguments);
        let errMsg = (res && res.errMsg) ? res.errMsg : '服务器连接超时!';
        typeof fnFail == "function" && fnFail({
          'error': true,
          'message': errMsg,
          'msg': '服务器错误'
        });

      },
      // complete
      complete: function() {

        wx.hideNavigationBarLoading();
        typeof fnComp == "function" && fnComp();

      }

    });

  },

  /**
   * GET请求
   * @param url
   * @param data
   * @param fnSucc
   * @param fnFail
   * @param fnComp
   * @constructor
   */
  WX_GET: function(url, data, fnSucc, fnFail, fnComp) {

    let that = this;

    wx.showNavigationBarLoading();
    console.log("reqData" + JSON.stringify(data));

    wx.request({

      url: that.hostUrlVerify(url),
      data: data || {},
      method: 'GET', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
      header: { // 设置请求的 header
        // 'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'content-type': 'application/json; charset=UTF-8',
        'Authorization': this.authorization,
      },
      // success
      success: function(res) {
        console.log("httpPostResponse: " + JSON.stringify(res.data));

        if (res && res.data && res.data.status && res.data.status == 200) {
          try {
            let response = res.data.data_list;
            if (typeof response == 'string') {
              response = response.replace('\ufeff', '');
              response = JSON.parse(response);
              typeof fnSucc == "function" && fnSucc(response);
            } else {
              typeof fnSucc == "function" && fnSucc(response);
            }
          } catch (e) {
            typeof fnSucc == "function" && fnSucc('');
          }
        } else {
          typeof fnFail == "function" && fnFail({
            'error': true,
            code: res.data.code || -1,
            'message': '接口调用失败',
            'msg': res.data.msg || '内部错误',
          });
        }
      },
      // fail
      fail: function(res) {

        console.log(arguments);
        let errMsg = (res && res.errMsg) ? res.errMsg : '服务器连接超时!';
        typeof fnFail == "function" && fnFail({
          'error': true,
          'message': errMsg
        });

      },
      // complete
      complete: function() {

        wx.hideNavigationBarLoading();
        typeof fnComp == "function" && fnComp();

      }

    });

  },

  /**
   * 上传表单文件
   * @param formData
   * @param index
   * @param data
   * @param fnSucc
   * @param fnFail
   */
  uploadFormFiles: function(formData, index, data, fnSucc, fnFail) {

    let that = this,
      idx = index || 0,
      len = formData && formData.length,
      files = [];

    // 表单为空或已经全部上传
    if (!formData || formData.length == 0 || idx >= formData.length) {

      typeof fnSucc == "function" && fnSucc();
      return;

    }

    for (; idx < len; idx++) {

      let formItem = formData[idx];

      if (formItem.type == 'image' || formItem.type == 'images' || formItem.type == 'cover') {

        files = formItem.files;
        break;

      }

    }

    if (files && files.length > 0) {

      that.syncUpload(that.urls.upload, files, 0, data, function() {

        that.uploadFormFiles(formData, idx + 1, data, fnSucc, fnFail); // 上传下一个

      }, fnFail);

    } else {

      typeof fnSucc == "function" && fnSucc();

    }

  },

  /**
   * 多文件上传
   * @param webUrl
   * @param files
   * @param index
   * @param data
   * @param fnSucc
   * @param fnFail
   */
  syncUpload: function(webUrl, files, index, data, fnSucc, fnFail) {

    let that = this,
      idx = index || 0;

    if (!files || files.length == 0 || idx >= files.length) { // 已全部上传

      typeof fnSucc == "function" && fnSucc();

    } else if (files[idx].value && files[idx].value.length > 0) { // 已上传, 上传下一个文件

      that.syncUpload(webUrl, files, idx + 1, data, fnSucc, fnFail);

    } else {

      this.uploadFile(webUrl, files[idx].url, null, data, function(url) { // 上传

        files[idx].value = url || '';
        url ? that.syncUpload(webUrl, files, idx + 1, data, fnSucc, fnFail) : (typeof fnFail == "function" && fnFail());

      }, function() {

        typeof fnFail == "function" && fnFail();

      })

    }

  },

  /**
   * 文件上传
   * @param url
   * @param path
   * @param name
   * @param data
   * @param fnSucc
   * @param fnFail
   */
  uploadFile: function(url, path, name, data, fnSucc, fnFail) {

    let that = this,
      app = getApp();
    var formData = {
      user_id: this.userId
    }

    wx.uploadFile({

      url: that.baseUrlVerify(url || this.urls.upload),
      filePath: path,
      name: 'name[]',
      header: {
        'content-type': 'multipart/form-data',
        'Authorization': this.authorization
      },
      formData: formData,
      success: function(res) {
        console.log(JSON.stringify(res));
        let response = res.data;

        if (!response) {

          typeof fnFail == "function" && fnFail({
            'error': true,
            'message': '接口返回空值',
            purview: true
          });

        } else if (typeof response == 'string') {

          // try {

          response = response.replace('\ufeff', '');
          response = JSON.parse(response);
          if (response && response.status == 200) {
            typeof fnSucc == "function" && fnSucc(response.data_list);
          } else {
            typeof fnFail == "function" && fnFail();
          }

          // } catch (e) {
          //   typeof fnFail == "function" && fnFail();
          // }

        } else {

          typeof fnSucc == "function" && fnSucc(response.data);

        }

      },
      fail: function(res) {
        console.log(res);
        typeof fnFail == "function" && fnFail();
      }
    });

  },

  /**
   * 下载
   * @param webUrl
   * @param fn
   * @param fail
   * @param fnComp
   */
  downloadFile: function(webUrl, fn, fail, fnComp) {

    wx.downloadFile({

      url: webUrl,
      header: {
        'content-type': 'multipart/form-data',
        'apiVersion': this.apiVersion
      },
      success: function(res) {
        typeof fn == "function" && fn(res.tempFilePath);
      },
      fail: function() {
        typeof fail == "function" && fail();
      },
      complete: function() {
        typeof fnComp == "function" && fnComp();
      }

    });

  },

  /**
   * 多文件上传
   * @param webUrl
   * @param files
   * @param index
   * @param data
   * @param fnSucc
   * @param fnFail
   */
  uploadImgs: function(webUrl, files, index, data, fileUrls, fnSucc, fnFail) {

    let that = this,
      idx = index || 0;

    if (!files || files.length == 0 || idx >= files.length) { // 已全部上传

      typeof fnSucc == "function" && fnSucc(fileUrls);

    } else if (files[idx].value && files[idx].value.length > 0) { // 已上传, 上传下一个文件

      that.uploadImgs(webUrl, files, idx + 1, data, fnSucc, fnFail);

    } else {

      this.uploadFile(webUrl, files[idx], null, data, function(data) { // 上传

        // files[idx].value = url || '';
        if (data && data.file_list && data.file_list.length > 0 && data.file_list[0]) {
          fileUrls[idx] = data.file_list[0];
          that.uploadImgs(webUrl, files, idx + 1, data, fileUrls, fnSucc, fnFail)

        } else {
          (typeof fnFail == "function" && fnFail());
        }
      }, function() {

        typeof fnFail == "function" && fnFail();

      })

    }

  },

};

module.exports = {

  http: httpManager

};