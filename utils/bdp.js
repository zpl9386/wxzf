/**
 * business data process
 */
let bdp = {

  /**
   * 阅读统计
   * @param result
   * @param data
   */
  parseReadNum: function (result, data) {

    let readedNum = data['readedNum'] || 0,
      unReadNum = data['unReadNum'] || 0,
      totalNum = readedNum + unReadNum;

    totalNum && (result.readStat = {
      readedNum: readedNum,
      unReadNum: unReadNum,
      totalNum: totalNum
    });

  },

  /**
   * 班级列表
   * @param datas
   * @returns {*}
   */
  parseClassName: function (datas) {

    let that = this;

    datas.forEach(function (item) {

      item.name && (item.name = that.removeClassYear(item.name));
      item.cid_name && (item.cid_name = that.removeClassYear(item.cid_name));

    });

    return datas

  },

  /**
   * 解析文件
   * @param str
   * @returns {Array}
   */
  parseFiles: function (str) {

    let app = getApp(), files = [];

    str = wx.util.trim(str);

    if (str.length > 0) {

      files = str && str.split(/[,;|]+/);
      app.http.urlsVerify(files);

    }

    return files;

  },

  /**
   * 图片解析
   * @param data
   * @param target
   * @returns {*|{}}
   */
  parseImages: function (data, target) {

    let result = target || {};

    data['images'] && (data['images'].length > 0) && (result.images = this.parseFiles(data['images']));
    data['images-source'] && (data['images-source'].length > 0) && (result.originalImages = this.parseFiles(data['images-source']));

    return result;

  },

  /**
   * 解析附件
   * @param str
   * @returns {Array}
   */
  parseAttachments: function (str) {

    let that = this, files = this.parseFiles(str), attachments = [];

    files.forEach(function (file) {

      attachments.push({
        url: file,
        name: wx.util.lastPathComponent(file),
        ext: wx.util.fileExtension(file).toLowerCase()
      })

    });

    return attachments;

  },

  /**
   * 数据解析
   * @param data
   */
  parseData: function (data, canFavorite, statis) {

    let app = getApp(), result = {};

    // id
    result.id = data.id || '';
    result.cid = data.cid || 0;
    result.pid = data.pid || 0;
    data['pid_name'] && (result['pid_name'] = data['pid_name'] || '');
    result.cuid = data.vduid || 0;
    result.type = data.type || 0;

    // 权限
    result.delete = !!data.delete;
    result.editor = !!data.editor;
    result.isExistNoRead = data.isExistNoRead || 0;

    // 是否收藏
    result.isFavorite = !!data.isfavorite;
    result.canFavorite = canFavorite && !app.isAdminOrSuper();

    // content
    result.title = data.title || '';
    result.content = wx.util.toSBCCase(data.content || '');
    result.name = data.name || '';
    result.avatar = data.avatar || '';
    result.time = data.uptime || '';

    data.date && (result.date = data.date || '');
    data.intime && (result.intime = data.intime || '');
    data.starttime && (result.starttime = wx.util.removeSeconds(data.starttime || ''));
    data.endtime && (result.endtime = wx.util.removeSeconds(data.endtime || ''));

    data.address && (result.address = data.address || '');
    data.course && (result.course = data.course || '');
    data.userName && (result.userName = data.userName || '');

    // 班级名称
    data['cid_name'] && (result.className = this.removeClassYear(data['cid_name'] || ''));
    data['className'] && (result.className = this.removeClassYear(data['className'] || ''));
    // 学期
    data['termid_name'] && (result.term = data['termid_name'] || '');
    // 学年
    data['grade'] && (result.grade = data['grade'] || '');
    // 图片
    this.parseImages(data, result);
    // 附件
    data.attachments && (data.attachments.length > 0) && (result.attachments = this.parseAttachments(data.attachments));

    // 已读未读统计
    statis && !app.isStudentOrFamily() && this.parseReadNum(result, data);

    return result;

  },

  /**
   * 作业解析
   * @param data
   * @returns {*}
   */
  parseHomework(data) {

    let result = this.parseData(data, true, true);

    result.info = (data['name'] || '') + '\u3000' + result.className;
    result.week = result.title.replace(/([\s\S]+)<([\s\S]+)?>([\s\S]+)/, '$2');

    result.marker = data['scoreLevel'] || data['marker'] || '';
    result.markerWord = data['markerWord'] || '';  // 评语
    result.scoreLevel = data['scoreLevel'] || '';  // 评分
    result.assess = data['assess'] || '';

    return result;

  },

  /**
   * 作业删除 修改 收藏操作
   * @param data
   * @returns {*}
   */
  parseHomeworkActions(data) {

    let app = getApp(), actions = null;

    if (data.assess) {
      actions = actions || [];
      actions.push({ id: 'assess', name: '去评分' });
      actions.push({ id: 'remark', name: '写评语' });
    }

    if (data.editor) {
      actions = actions || [];
      actions.push({ id: 'editor', name: '修改' });
    }

    if (data.delete) {
      actions = actions || [];
      actions.push({ id: 'delete', name: '删除' });
    }

    if (!app.isAdminOrSuper()) {
      actions = actions || [];
      actions.push({ id: 'favorite', name: '收藏' });
    }

    return actions;

  },

  /**
   * 作业列表解析
   * @param datas
   * @returns {Array}
   */
  parseHomeworkList: function (datas) {

    let that = this, results = [];

    datas && datas.forEach(function (data) {

      let result = that.parseHomework(data);
      result.actions = that.parseHomeworkActions(result);
      result.actionHandler = 'bindActionHandler';
      results.push(result);

    });

    return results;

  },

  /**
   * 解析班级圈文章
   * @param data
   * @returns {*}
   */
  parseArticle: function (data) {

    let result = this.parseData(data, true);

    result.overflow = result.content && result.content.length > 150;
    result.expand = false;

    // 点赞
    result.approve = data.approve;
    result.approveCount = data.approveCount;
    result.approveList = data.approveList || [];
    result.approveList.reverse();

    // 评论
    result.commentCount = data.commentCount || [];
    result.commentList = data.commentList || [];
    result.showAllComment = (result.commentList.length < 8);
    // article.commentList.reverse()
    result.active = false;
    result.cuid = data['vduid'] || 0;
    result.bindid = data['pid'] || 0;

    return result;

  },

  /**
   * 解析班级圈文章列表
   * @param datas
   * @returns {Array}
   */
  parseArticleList: function (datas) {

    let that = this, results = [];

    datas && datas.forEach(function (data) {

      results.push(that.parseArticle(data));

    });

    return results;

  },

  /**
   * 解析通知 文章
   * @param data
   * @returns {{}}
   */
  parseNotice: function (data) {

    let app = getApp(), result = this.parseData(data, true, true);

    result.bindid = data.pid || 0;
    result.type = result.cid == 0 ? app.E_NOTICE_TYPE.SCHOOL : result.type;
    result.important = data.important || 0;  // 是否紧急通知
    result.issendmsg = data.issendmsg || 0;  // 是否需要短信通知

    // iconStyle
    result.iconStyle = 'background-color:' + (result.type == app.E_NOTICE_TYPE.SCHOOL ? '#FF9700' : '#99CC01');

    return result;

  },

  /**
   * 解析通知列表
   * @param datas
   * @returns {Array}
   */
  parseNoticeList: function (datas) {

    let that = this, results = [];

    datas && datas.forEach(function (data) {

      results.push(that.parseNotice(data));

    });

    return results;

  },

  /**
   * 解析活动
   * @param data
   * @returns {*}
   */
  parseActivity: function (data) {

    let result = this.parseData(data, true);

    // 是否已报名
    result.apply = data.apply || 0;

    // 类型
    result['activityType'] = data['activityType'] || 0;
    result['joinType'] = data['joinType'] || 0;

    // 是否可以参加
    result.isApply = !!data.isApply;
    // 评价权限
    result['ratingAuthor'] = data['ratingAuthor'] || 0;    // 评价组
    result['rating'] = data['rating'] || 0;                // 评价项
    result['ratingScore'] = data['ratingScore'] || 0;      // 评价结果

    // 时间
    result['enrollEndtime'] = wx.util.removeSeconds(data['enroll_endtime'] || '');
    result['recommendEndtime'] = wx.util.removeSeconds(data['recommend_endtime'] || '');
    result['voteEndtime'] = wx.util.removeSeconds(data['vote_endtime'] || '');

    // 封面
    result.cover = data['img'] || '';
    result.originalCover = data['img-source'] || '';
    if (!result.cover && result.images) {
      result.cover = result.images.length > 0 ? result.images[0] : '';
      result.originalCover = result.originalImages.length > 0 ? result.originalImages[0] : '';
    }

    // 更新状态
    this.updateActivityStatus(result, data);

    return result

  },

  /**
   * 解析活动列表
   * @param datas
   * @returns {Array}
   */
  parseActivityList: function (datas) {

    let that = this, results = [];

    datas && datas.forEach(function (data) {

      results.push(that.parseActivity(data));

    });

    return results;

  },

  /**
   * 更新活动状态
   * @param activity
   * @param data
   */
  updateActivityStatus: function (activity, data) {

    let startDateStamp = wx.util.getDateTimeStamp(data['starttime']),
      endDateStamp = wx.util.getDateTimeStamp(data['endtime']),
      nowStamp = wx.util.getDateTimeStamp(data['currentDate']),
      statusStr = '', active = 0;

    if (nowStamp < startDateStamp) {

      statusStr = '活动未开始';

    } else if (nowStamp < endDateStamp) {

      statusStr = '活动进行中';
      active = 1;

    } else {

      statusStr = '活动已结束';

    }

    activity.statusStr = statusStr;
    activity.active = active;

  },

  /**
   * 活动奖品
   * @param idx
   * @returns {string}
   */
  getPrizeSting: function (idx) {

    let prizes = ['', '金奖', '银奖', '铜奖', '人气奖', '幸运奖'];
    return idx < prizes.length ? prizes[idx] : prizes[0];

  },

  /**
   * 解析活动作品
   * @param data
   * @param flag
   * @param canIRecommend
   * @param deletable
   * @returns {{}}
   */
  parseActivityArticle: function (data, flag, canIRecommend, deletable) {

    let app = getApp(), isAdminOrSuper = app.isAdminOrSuper(), result = this.parseData(data);

    result.activityId = data.activityid || 0;
    result.delete = deletable && result.delete;

    result.luckyNumber = data['luckyNumber'] || ''; // 编号
    result.isWinners = data['isWinners'] || 0;     // 是否入围
    result.prizeStatus = data['prizeStatus'] || 0;  // 奖项
    result.prizeStr = this.getPrizeSting(result.prizeStatus);
    result.scoreCount = data['scoreCount'] || 0;    // 总分
    result.voteCount = data['voteCount'] || 0;      // 总票数

    result.recommend = !!(data['recommend'] || 0);             // 推荐
    result.cancelRecommend = !!(data['cancelRecommend'] || 0);  // 取消推荐
    result.canIRecommend = !isAdminOrSuper && canIRecommend;     // 是否可以推荐
    result.score = (!isAdminOrSuper && data['score']) || 0;      // 是否可以评分
    result.vote = (!isAdminOrSuper && data['vote']) || 0;        // 是否可以投票

    result.currentScore = data['currentScore'] || 0;  // 当前分数
    result.currentVote = data['currentVote'] || 0;    // 是否已投

    result.activityTitle = data['activityid_title'] || '';    // 活动标题
    result.flag = flag;
    result.showStars = app.isTeacher() || app.isSchoolOffice();  // 教师或校务组可以评分

    return result;

  },

  /**
   * 解析作品列表
   * @param datas
   * @param flag
   * @param canIRecommend
   * @param deletable
   * @returns {Array}
   */
  parseActivityArticleList: function (datas, flag, canIRecommend, deletable) {

    let that = this, results = [];

    datas && datas.forEach(function (data) {

      results.push(that.parseActivityArticle(data, flag, canIRecommend, deletable));

    });

    return results;

  },

  /**
   * 成绩类型
   * @param id
   * @returns {*|string}
   */
  getExamTypeName: function (id) {

    let app = getApp(), result = wx.util.getOptionBy('id', id, app.config.examOptions());
    return result.name;

  },

  /**
   * 解析成绩
   * @param data
   * @returns {{}}
   */
  parseExam: function (data) {

    let result = this.parseData(data);

    result.conid = data['conid'] || '';
    result.typeName = this.getExamTypeName(result.type);

    result.score = data['score'] || ''; // 成绩
    result.order = data['order'] || ''; // 排名
    result.range = data['range'] || ''; // 评价

    result.scoreavg = data['scoreavg'] || ''; // 平均分
    result.maxScore = data['maxScore'] || ''; // 最高分
    result.maxScorePname = data['maxScorePname'] || ''; // 最高分
    result.minScore = data['minScore'] || ''; // 最低分
    result.minScorePname = data['minScorePname'] || ''; // 最低分
    result.num = data['num'] || '';           // 人数

    result.starttime = wx.util.getDateString(result.starttime);


    return result;

  },

  /**
   * 成绩列表
   * @param datas
   * @returns {Array}
   */
  parseExamList: function (datas) {

    let that = this, results = [];

    datas && datas.forEach(function (data) {

      results.push(that.parseExam(data));

    });

    return results;

  },

  /**
   * 请假解析
   * @param data
   * @returns {*}
   */
  parseVacation: function (data) {

    this.parseImages(data, data);
    return data;

  },

  /**
   * 请假列表
   * @param datas
   * @returns {Array}
   */
  parseVacationList: function (datas) {

    let that = this, results = [];

    datas && datas.forEach(function (data) {

      results.push(that.parseVacation(data));

    });

    return results;

  },

  /**
   * 成长记录
   * @param data
   * @returns {*}
   */
  parseGrowthData: function (data) {

    data.name = data.name || '\u3000';
    data.content && (data.content = data.content.replace(/\\n/g, '\u000a'));
    data['intime'] && (data.date = wx.util.dateFormat(new Date(wx.util.getDateTimeStamp(data['intime'])), 'Y.M.d W'));
    data['ratingValue'] = data['ratingValue'] || data['ratingCount'] || 0;
    data.delete = !!data.delete;

    this.parseImages(data, data);

    return data

  },

  /**
   * 成长记录列表
   * @param datas
   * @returns {Array}
   */
  parseGrowthDataList: function (datas) {

    let that = this, results = [];

    datas && datas.forEach(function (data) {

      if ((datas['type'] == 2 || datas['type'] == 3) && (/(未批改|未录入)$/.test(datas['content']))) {

      } else {

        results.push(that.parseGrowthData(data));

      }

    });

    return results;

  },

  ///////////入驻学校///////////////////////////
  /**
   * 解析学校
   * @param data
   * @returns {*}
   */
  parseEnrollSchool: function (data) {

    let address = data.province || '';

    if (address != data.city) {
      address += (data.city || '');
    }
    address += (data.address || '');
    data.address = address;
    data.intime = wx.util.removeSeconds(data.intime);
    this.parseImages(data, data);

    data.statusString = (data.status == 1) ? '通过' : (data.status == 2) ? '不通过' : '待审核';
    data.statusColor = (data.status == 1) ? 'cl-green' : (data.status == 2) ? 'cl-red' : 'cl-steelblue';

    return data;

  },

  /**
   * 解析学校列表
   * @param datas
   * @returns {Array}
   */
  parseEnrollSchoolList: function (datas) {

    let that = this, results = [];

    datas && datas.forEach(function (data) {

      results.push(that.parseEnrollSchool(data));

    });

    return results;

  },

  /**
   * 校本课程 -状态样式
   * @param status
   * @returns {string}
   */
  getBaseClassStatusStyle: function (status) {

    let style = '';

    switch (parseInt(status)) {
      case -1:  // 不通过
        style = 'color:#e64340';
        break;
      case 9:   // 已开课
        style = 'color:#09bb07';
        break;
      default:  // 其它状态
        style = 'color:#5292dc';
        break
    }

    return style;

  },

  /**
   * 校本课程 -学生报名样式
   * @param status
   * @returns {string}
   */
  getBaseClassUserStyle: function (status) {

    let style = '';

    switch (parseInt(status)) {
      case 0:   // 待加入
        style = 'color:#5292dc';
        break;
      case 1:   // 已加入
        style = 'color:#09bb07';
        break;
      case 2:   // 已拒绝
        style = 'color:#e64340';
        break;
      default:
        break;
    }

    return style;

  },

  /**
   * 新闻数据解析
   * @param data
   * @returns {{}}
   */
  parseSchoolNews: function (data) {

    let result = this.parseData(data);

    result.time = data.intime || '';

    return result;

  },

  /**
   * 新闻列表
   * @param datas
   * @returns {Array}
   */
  parseSchoolNewsList: function (datas) {

    let that = this, results = [];

    datas && datas.forEach(function (data) {

      results.push(that.parseSchoolNews(data));

    });

    return results;

  },

  /**
   * 出勤率
   * @param data
   * @returns {{dayRate: number, monthRate: number, weekRate: number}}
   */
  parseAttendanceStatis: function (data) {

    let statis = { dayRate: 0, monthRate: 0, weekRate: 0 }, statisItems = ['idle', 'dayRate', 'monthRate', 'weekRate'];

    data.forEach(function (item) {

      let _type = item.type, id = _type < statisItems.length ? statisItems[_type] : statisItems[0];

      statis[id] = item['attendanceRate'] || 0;
      if (id == 'dayRate') {
        statis.title = (item.attendanceNum || 0) + '/' + (item.allNum || 0)
      }

    });

    return statis;

  },

  /**
   * 出勤详情
   * @param datas
   * @returns {*}
   */
  parseAttendanceDatas: function (datas) {

    let that = this;

    datas.forEach(function (data) {

      that.parseImages(data, data);
      data.time = data.attendanceTime.replace(/[\s\S]*?(\d+:\d+):\d+$/, '$1')

    });

    return datas;

  },

  /**
   * 学生家长列表
   * @param students
   * @returns {Array}
   */
  getFamilies(students) {

    let families = [];

    students && students.forEach(function (student) {

      wx.util.extend(families, student.relationList || []);

    });

    families.forEach(function (item) {

      let name = item.name;
      item.name = item.job;
      item.job = name;

    });

    return families;

  },

  /**
   * 2016级二[一]班
   * @param str
   * @returns {string}
   */
  removeClassYear: function (str) {

    return String(str).replace(/^\d+级([\s\S]+)/, "$1");

  },

  /**
   * 文件信息
   * @param data
   * @param id
   * @param index
   * @returns {{}}
   */
  formatFile: function (data, id, index) {

    let file = {};

    file.filePath = data.filePath;
    file.name = wx.util.hideString(wx.util.lastPathComponent(data.filePath));
    file.ext = wx.util.fileExtension(data.filePath).toLowerCase();
    file.intime = wx.util.formatTime(new Date(data.createTime * 1000));
    file.filesize = wx.util.formatSize(data.size);
    file.checked = false;
    file.delete = true;
    file.type = 0;
    file.id = id;
    file.index = index;

    return file;

  }

};

module.exports = {

  bdp: bdp

};
