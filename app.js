//app.js
require('./utils/util.js'); // util 公共方法

let {
  http
} = require('./utils/http.manager.js'); // 接口管理
let {
  bdp
} = require('./utils/bdp.js'); // 数据解析
let {
  config
} = require('./app.config.js'); // app.configs
let {
  page
} = require('./utils/page.utils.js'); // 数据解析



App({
  // utils
  config, // 配置
  http, // http接口管理
  bdp, // 数据处理
  page, // 页面相关

  /**
   * 颜色配置
   */
  E_THEME_COLOR: {

    'RED': '#c6010c',
    'GREEN': '#5ebc44',
    'BLUE': '#29bcf1',
    'YELLOW': '#fdbb03'

  },

  /**
   * 配置: 菜单和颜色
   */
  configs: {},

  /**
   * 全局变量
   */
  globalData: {},

  onLaunch: function() {
    // 回复默认配置
    this.resetConfigs();

    // 展示本地存储能力
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
      }
    })
    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              this.globalData.userInfo = res.userInfo

              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
            }
          })
        }
      }
    })
  },
  globalData: {
    userInfo: null
  },

  //////// 配置相关 /////////////////////////////////////////////
  /**
   * 默认配置
   */
  resetConfigs: function() {

    this.configs = {

      // 所有菜单功能
      // menus: this.config.getAllMenus(),
      // 主题
      theme: 'green' // blue:29BCF1, red:C6010C , green:5ebc44, yellow:#FDBB03

    }

  },
  /**
   * get or set theme
   * @param theme
   */
  theme: function(theme) {

    if (theme != null) {
      this.configs.theme = theme;
    }

    return this.configs.theme;

  },

  /**
   * 根据theme 获取颜色值
   * @param theme
   * @returns {E_THEME_COLOR|{RED, GREEN, BLUE, YELLOW}}
   */
  getThemeColor: function(theme) {

    return theme == null ? this.E_THEME_COLOR : this.E_THEME_COLOR[String(theme).toLocaleUpperCase()];

  },

  /**
   * 设置底部tabBar主题颜色
   * @param theme
   */
  setTabBarTheme: function(theme) {

    this.config.setTabBarTheme(theme || this.configs.theme || 'green');

  },

  /**
   * 更新首页tabBar角标
   */
  updateHomeTabbar: function() {

    let badge = this.globalData.badges || {};
    this.config.setHomePageBadge(badge);

  },

  /**
   * 更新班级圈角标
   */
  updateArticleTabbar: function() {

    let article = this.globalData.noReadArticles || {};
    this.config.setArticlePageBadge(article);

  },

  /**
   * 更新我 角标
   */
  updateMineTabbar: function() {

    let sysNotice = this.globalData.sysNoRead || {};
    this.config.setMinePageBadge(sysNotice);

  },

  /**
   * 缓存角标
   * @param badge
   */
  cacheBadges: function(badge) {

    if (badge) {
      this.globalData.badges = badge;
      this.config.setHomePageBadge(badge);
    }

    return this.globalData.badges || {};

  },

  /**
   * menu id badge -= 1
   */
  reduceMenuBadge: function(menuId, num) {

    let badges = this.cacheBadges();

    if (badges[menuId] && badges[menuId] > 0) {
      badges[menuId] -= (num == null ? 1 : num);
      badges[menuId] = (badges[menuId] <= 0) ? 0 : badges[menuId];
      this.cacheBadges(badges);
    }

    return true;

  },

  /**
   * menu id badge -= value
   */
  setMenuBadge: function(menuId, value) {

    let badges = this.cacheBadges();

    badges[menuId] = value;
    this.cacheBadges(badges);

    return true;

  },

  ////globalData/////////////////////////////////////
  getSystemInfo: function(cb) {

    var that = this,
      sysInfo = that.globalData.sysInfo;

    if (!sysInfo) {
      wx.getSystemInfo({
        success: function(res) {
          that.globalData.sysInfo = res;
          typeof cb == "function" && cb(res);
        }
      });
    } else {
      typeof cb == "function" && cb(sysInfo);
    }

  },
  getSystemInfoSync: function() {

    if (!this.globalData.sysInfo) {
      this.globalData.sysInfo = wx.getSystemInfoSync();
    }
    return this.globalData.sysInfo;

  },
  getWindowWidth: function() {

    let sysInfo = this.getSystemInfoSync();
    return sysInfo && sysInfo.windowWidth || 375;

  },
  getWindowHeight: function() {

    let sysInfo = this.getSystemInfoSync();
    return sysInfo && sysInfo.windowHeight || 603;

  },

  /**
   * get global ids
   */
  getGlobalIds: function() {
    let parameter = {
      'userId': 1,
      'name': 'hu'
    };

    // let experience = this.experience,
    //   schoolID = this.userData.getSchoolID(),
    //   adminID = this.userData.getAdminID(),
    //   userID = this.userData.getUserID(),
    //   identityID = this.userData.getIdentityID(),
    //   param = { userid: userID, adminid: adminID, identityId: 0 };

    // experience && wx.util.extend(param, experience);
    // !adminID && (param['identityId'] = identityID);
    // schoolID && (param['scid'] = schoolID);

    return parameter;

  },

  ///////////身份信息////////////////////////////////////////
  /**
   * 获取用户信息
   * @param force
   * @param fn
   * @param fail
   */
  getUserInfo: function(force, fn, fail) {

    let that = this;

    if (force || !that.globalData.userInfo) { // 强制 或 缓存为空

      //调用接口
      wx.wrap.getUserInfo(function(res) {

        that.globalData.userInfo = res.userInfo;
        typeof fn == "function" && fn(res.userInfo);

      }, fail);

    } else {

      typeof fn == "function" && fn(that.globalData.userInfo);

    }

  },

  /**
   * 授权界面
   */
  openSetting: function(scope, fn) {

    let that = this;

    wx.wrap.openSetting(scope, fn, function(message) { // fail

      message && that.showInfoModal('', message);

    }, function(message) { // error

      message && that.showInfoModal('', message);

    })

  },

  // 如果授权，获取个人信息，否则返回null
  getUserInfoIfOAuthed: function(fn) {

    let that = this;

    // 检查是否已授权
    wx.wrap.getSetting(function(authSetting) { // 接口可用

      if (authSetting['scope.userInfo']) { // 授权

        that.getUserInfo(false, fn);

      } else { // 未授权

        typeof fn == "function" && fn(null);

      }

    }, function() { // 调用失败

      typeof fn == "function" && fn(null);

    }, function() { // 接口不可用

      that.getUserInfo(false, fn);

    })

  },

  // 获取code
  getUserCode: function(fn, fail) {

    let that = this;

    wx.login({

      success: function(res) {

        // console.log(res)
        if (res.code) {

          that.globalData.userCode = res.code;
          typeof fn == "function" && fn(res.code);

        } else {

          typeof fail == "function" && fail('获取用户登录态失败!');

        }

      },
      fail: function() {

        // console.log('登陆异常!')
        typeof fail == "function" && fail('登陆异常!');

      }

    })

  },

  //获取UserID
  getUserID: function(fn, fail) {

    var that = this,
      userId = this.userData.getUserID();

    //未登陆
    if (!userId) {

      that.getUserCode(function(code) {

        // 如果已经授权-获取个人信息
        that.getUserInfoIfOAuthed(function(userInfo) {

          userInfo = userInfo || {};
          userInfo['code'] = code;

          //提交登陆信息到服务器
          that.http.ajxGetUserID(userInfo, function(response) {

            if (response && response.pid) {

              that.userData.setUserID(response.pid);
              typeof fn == "function" && fn(response.pid);

            } else {

              typeof fail == "function" && fail(response.message || '');

            }

          }, function(res) {

            // console.log(res && res.message || '登陆请求超时!')
            typeof fail == "function" && fail(res && res.message || '登陆请求超时!');

          });

        });

      }, function(msg) {

        typeof fail == "function" && fail(msg);

      });

      //已登录
    } else {

      typeof fn == "function" && fn(userId);

    }

  },

  /**
   * render 数据
   * @param refresh
   * @param str
   * @param start
   * @param datas
   * @returns {{}}
   */
  getExtendData: function(refresh, str, start, datas) {

    let data = {};

    if (refresh) { // 刷新

      data[str] = datas || [];

    } else { // 追加

      datas.forEach(function(item, i) {
        data[str + '[' + (start + i) + ']'] = item;
      })

    }

    return data

  },

  ///////////Common UI/////////////////////////////
  /**
   * 显示或隐藏loading
   * @param show
   * @param page
   * @param title
   */
  showLoading: function(show, page, title) {

    // console.log(`showLoading:${show}`)
    if (wx.showLoading) {

      if (show) {

        wx.showLoading({
          title: title || 'Loading',
          mask: true
        }); // bug - iOS上会卡主

      } else {

        wx.hideLoading();

      }

    } else {

      page = page || wx.util.getLastPageOf(0);
      page && page.setData({
        showLoading: show,
        loadingTitle: title || 'Loading'
      });

    }

  },

  /**
   * toast 错误信息
   */
  toastErr: function(msg) {

    wx.wrap.showToast(msg);

  },

  // 删除确认
  showCfmModal: function(title, content, fnCfm, fnCancel) {

    wx.showModal({

      title: title || '警告',

      content: content || '确定执行删除么？',

      success: function(res) {

        if (res.confirm) {
          typeof fnCfm == "function" && fnCfm();
        } else {
          typeof fnCancel == "function" && fnCancel();
        }

      },

      fail: function() {

        typeof fnCancel == "function" && fnCancel();

      }

    });

  },

  // 提示信息
  showInfoModal: function(title, content, cb) {

    wx.showModal({

      title: title || '提示',

      content: content || '',

      showCancel: false,

      success: function(res) {

        if (res.confirm) {
          typeof cb == "function" && cb()
        }

      }

    });

  },

  // 上拉菜单
  showActionSheet: function(itemList, cb) {

    wx.showActionSheet({

      itemList: itemList,

      itemColor: '#1472E0',

      success: function(res) {

        if (!res.cancel) {
          typeof cb == "function" && cb(res.tapIndex, itemList[res.tapIndex])
        }

      }

    });

  },

  // 收藏、删除
  showCustomActionSheet: function(obj, cb) {

    let that = this,
      itemList = [];

    obj.canFavorite && itemList.push('收藏');
    obj['delete'] && itemList.push('删除');

    (itemList.length > 0) && this.showActionSheet(itemList, function(tapIndex, itemName) {

      if (itemName == '收藏') {

        typeof cb == "function" && cb('favorite');

      } else if (itemName == '删除') {

        that.showCfmModal('', '', function() {

          typeof cb == "function" && cb('delete');

        });

      }

    })

  },

  /**
   * wx.showLoading()的蒙层有时候无效，不知道哪次更新就会出现...
   * 所以添加了一个标志来禁用Button
   */
  isSubmitting: function(submitting, page) {

    page = page || wx.util.getLastPageOf(0);
    page && page.setData({
      submitting: submitting
    });

  },

  /**
   * 最低版本库检查
   */
  miniSDKCheck: function() {

    let that = this,
      miniSDK = this.config.miniSDK,
      cache = wx.storage.getMiniSDK();

    miniSDK && (miniSDK != cache) && this.getSystemInfo(function(info) {

      console.log(info)
      if (wx.util.compareVersion(info.SDKVersion, miniSDK) < 0) {

        wx.showModal({

          title: '提示',
          content: '您的微信版本比较低，请更新到最新版本！',
          cancelText: '下次再说',
          confirmText: '不再提醒',
          success: function(res) {

            if (res.confirm) {
              wx.storage.setMiniSDK(miniSDK);
            }

          }

        });

      } else {

        wx.storage.setMiniSDK(miniSDK);

      }

    });

  },

  // 列表数据是否为空
  isDataValueEmpty: function(data) {

    let result = true;

    if (data) {

      for (let i in data) {

        if (data[i]) {
          result = false;
          break;
        }

      }

    }

    return result;

  },

  // 删除
  deleteHandler: function(list, idx, str, emptyStr) {

    let data = {};

    list[idx] = '';
    if (this.isDataValueEmpty(list)) {
      data[str] = [];
      emptyStr && (data[emptyStr] = true);
    } else {
      data[str + '[' + idx + ']'] = '';
    }

    return data;

  },

  isNothing: function(e) {
    // var a = !e;
    // var b = typeof(e) == "undefined";
    // var c = isNaN(e);
    // var e1 = e == undefined;
    // var f = e == null;
    // var g = e == '';
    if (!e || typeof(e) == "undefined" || e == undefined || e == null || e == '') {
      return true;
    }
    return false;
  },

  isObjNothing: function(e) {
    var isNt = e === 0 || e === false || e === null || e === undefined || Object.keys(e).length === 0 || /^\s*$/gim.test(e.toString());
    return isNt;
  },


})