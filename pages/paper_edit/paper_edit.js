// pages/paper_edit/paper_edit.js
var app=getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    user_id: '',
    title: '',
    invitation_type: '',
    type: '',
    rental: '',
    latitude: '',
    longitude: '',
    describe: '',
    pictures: '',
    province: '',
    city: '',
    district: '',
    namme: '',
    area: '',
    house_type_room: '',
    house_type_office: '',
    house_type_defend: '',
    payment_method: '',
    orientation: '',

    Pickertypes: ['房东直租', '转租', '合租', '求整租'], //1:房东直租/2:转租/3:合租/4:求合租/5:求整租
    iPickertypes: 0,
    Pickerdistricts: ['保安', '罗湖', '南山'],
    iPickerdistricts: 0,
    Pickerrooms: ['1房', '2房', '3房', '4房', '5房', '多房'],
    iPickerrooms: 0,
    Pickeroffices: ['无厅', '1厅', '2厅', '3厅', '4厅'],
    iPickeroffices: 0,
    Pickerdefends: ['无卫生间', '1卫生间', '2卫生间', '3卫生间'],
    iPickerdefends: 0,
    Pickerpayments: ['押一付一', '押一付三', '押一付六', '押一付全年'], //1:押一付一/2:押一付三/3:押一付六/4:押一付全年
    iPickerpayments: 0,
    Pickerorientations: ['朝东', '朝南', '朝西', '朝北'],
    iPickerorientations: 0,
    imgList: [],
    modalName: null,
    textareaAValue: '',
  },

  Pickertype(e) {
    console.log(e);
    this.setData({
      iPickertypes: e.detail.value
    })
  },

  Pickerdistrict(e) {
    console.log(e);
    this.setData({
      iPickerdistricts: e.detail.value
    })
  },

  Pickerroom(e) {
    console.log(e);
    this.setData({
      iPickerrooms: e.detail.value
    })
  },

  Pickerdefend(e) {
    console.log(e);
    this.setData({
      iPickerdefends: e.detail.value
    })
  },

  Pickeroffice(e) {
    console.log(e);
    this.setData({
      iPickeroffices: e.detail.value
    })
  },

  Pickerpayment(e) {
    console.log(e);
    this.setData({
      iPickerpayments: e.detail.value
    })
  },

  Pickerorientation(e) {
    console.log(e);
    this.setData({
      iPickerorientations: e.detail.value
    })
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    app.page.onLoadHandler(this, '编辑');
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  PickerChange(e) {
    console.log(e);
    this.setData({
      index: e.detail.value
    })
  },
  MultiChange(e) {
    this.setData({
      multiIndex: e.detail.value
    })
  },

  ChooseImage() {
    wx.chooseImage({
      count: 4, //默认9
      sizeType: ['original', 'compressed'], //可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album'], //从相册选择
      success: (res) => {
        if (this.data.imgList.length != 0) {
          this.setData({
            imgList: this.data.imgList.concat(res.tempFilePaths)
          })
        } else {
          this.setData({
            imgList: res.tempFilePaths
          })
        }
      }
    });
  },
  ViewImage(e) {
    wx.previewImage({
      urls: this.data.imgList,
      current: e.currentTarget.dataset.url
    });
  },
  DelImg(e) {
    wx.showModal({
      title: '召唤师',
      content: '确定要删除这段回忆吗？',
      cancelText: '再看看',
      confirmText: '再见',
      success: res => {
        if (res.confirm) {
          this.data.imgList.splice(e.currentTarget.dataset.index, 1);
          this.setData({
            imgList: this.data.imgList
          })
        }
      }
    })
  },
  textareaAInput(e) {
    this.setData({
      textareaAValue: e.detail.value
    })
  },
  textareaBInput(e) {
    this.setData({
      textareaBValue: e.detail.value
    })
  },

  writePaper: function(e) {
    var pageData = this.data;
    var inputData = e.detail.value;
    inputData.type = parseInt(pageData.iPickertypes) + 1;
    inputData.district = pageData.Pickerdistricts[pageData.iPickerdistricts];
    // inputData.house_type_room = pageData.Pickerrooms[pageData.iPickerrooms];
    // inputData.house_type_office = pageData.Pickeroffices[pageData.iPickeroffices];
    // inputData.house_type_defend = pageData.Pickerdefends[pageData.iPickerdefends];
    inputData.house_type_room = pageData.iPickerrooms;
    inputData.house_type_office = pageData.iPickeroffices;
    inputData.house_type_defend = pageData.iPickerdefends;
    inputData.payment_method = parseInt(pageData.iPickerpayments) + 1;
    inputData.orientation = pageData.Pickerorientations[pageData.iPickerorientations];
    inputData.describe = pageData.textareaAValue;

// 缺省字段
    inputData.invitation_type=1;
    inputData.latitude=27.001;
    inputData.longitude=29.0001;
    inputData.province="广东";
    inputData.city="深圳";
    inputData.restrict=1;
    inputData.name="nam";

    console.log(JSON.stringify(inputData));
    app.http.publicPaper(inputData, pageData.imgList, function(res) {
      wx.showToast({
        title: '发布成功',
      })
    }, function(res) {
      wx.showToast({
        title: '发布失败' + res.msg,
      })
    }, function(res) {
      // wx.showToast({
      //   title: '发布失败' + res.msg,
      // })
    });
    var req = {
      user_id: '',
      title: '',
      invitation_type: '',
      type: '',
      rental: '',
      latitude: '',
      longitude: '',
      describe: '',
      pictures: '',
      province: '',
      city: '',
      district: '',
      namme: '',
      area: '',
      house_type_room: '',
      house_type_office: '',
      house_type_defend: '',
      payment_method: '',
      orientation: '',
    }
  }
})