// pages/post_paper_type/post_paper_type.js
var app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    app.page.onLoadHandler(this, '发帖类型选择');
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  // 身份选择
  bindUser: function (e) {

    let id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/pages/post_paper_edit_abstract/post_paper_edit_abstract',
      type:id,
    })
    // app.navigateTo('/pages/post_paper_edit_abstract/post_paper_edit_abstract', { 'type': id });

  },


  // /**
  // * 菜单配置
  // * @param cb
  // */
  // getMenuConfigs: function (cb) {

  //   let configs = app.userData.getMenuConfig(),
  //     menus1 = this.parseMenus(configs['type1'] || [], ''),
  //     menus2 = this.parseMenus(configs['type2'] || [], 'background-'),
  //     menus3 = this.parseMenus(configs['type3'] || [], 'background-');

  //   // 保存
  //   app.globalData.menuIDs = wx.util.objectsValue(menus3, 'menuName');
  //   this.updateMenuConfigs(menus1);

  //   typeof cb == "function" && cb(menus1, menus2, menus3);

  // },

  // /**
  //    * 所有菜单
  //    */
  // getAllMenus: function () {

  //   return {
  //     timetable: {  // 1-
  //       menu: { menuName: 'timetable', name: '房东直租', icon: 'ft-q-timetable', badge: 0 },
  //       style: 'color:#5292dc'
  //     },
  //     homework: { // 2-
  //       menu: { menuName: 'homework', name: '转租', icon: 'ft-q-homework', badge: 0 },
  //       style: 'color:#5EBB44'
  //     },
  //     exam: { // 3-
  //       menu: { menuName: 'exam', name: '和租', icon: 'ft-q-exam', badge: 0 },
  //       style: 'color:#32ABE2'
  //     },
  //     vacation: { // 4-
  //       menu: { menuName: 'vacation', name: '求整租', icon: 'ft-q-dayoff', badge: 0 },
  //       style: 'color:#25A7FD'
  //     }
  //   }

  // },

  // /**
  //  * 更新菜单配置
  //  * @param menus
  //  */
  // updateMenuConfigs: function (menus) {

  //   let cfgMenus = app.configs.menus;

  //   menus.forEach(function (item) {

  //     let name = item['menuName'], menu = cfgMenus[name];
  //     if (menu) {
  //       menu.menu.name = item.name;
  //       menu.menu.icon = item.icon;
  //       menu.style = item.style;
  //     }

  //   });
  //   app.configs.menus = cfgMenus;

  // },

  // /**
  //  * 解析菜单列表
  //  * @param configs
  //  * @param preStyle
  //  * @returns {Array}
  //  */
  // parseMenus: function (configs, preStyle) {

  //   let menus = [];

  //   configs.forEach(function (item) {

  //     let name = item['menuName'], config = app.configs.menus[name], menu = {};

  //     if (config && app.isMobilePlatform(item.platform)) {

  //       item['icon'] && (config['menu'][icon] = item['icon']);
  //       // item['style'] && (config['style'] = item['style'])
  //       wx.util.extend(menu, config['menu']);
  //       wx.util.extend(menu, { style: preStyle + config['style'] });
  //       wx.util.extendByUnion(menu, item);
  //       menus.push(menu);

  //     }

  //   });

  //   return menus;

  // },
})