// pages/paper_detail/paper_detail.js
var app = getApp();

Page({

  data: {
    cur: -1,
    maintitle: 'maintitle',
    id: '',
    article: '{}',
    comment: '{}',
    prompt: {
      show: false,
      type: 'text',
    },
    promptValue: '',
    commentData: {
      invitation_id: '',
      privacy: 1,
      user_id: '',
      reply_id: '', //消息id
      content: '',
      type: '', //类型（评论1，回复2）
      reply_user_id: ''
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    app.page.onLoadHandler(this, '帖子详情');
    this.data.id = options.id;
    // this.initForTest();
    this.requestPaperDetail(options.id);
    this.requestComment(options.id, 1);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  requestPaperDetail: function(id) {
    let that = this;
    app.http.requestPaperDetail(id, function(res) {
      if (res) {
        that.importData(res);
        // wx.showToast({
        //   title: '成功获取帖子列表',
        // })
      } else {
        wx.showToast({
          title: '没有符合条件的帖子',
        })
      }

    }, function(res) {
      wx.showToast({
        title: '获取帖子列表失败',
      })
    }, function(res) {

    })
  },

  importData: function(res) {
    if (res) {
      var refresh = true;
      var that = this,
        param = {};
      var userType = '';
      switch (res.role) {
        //1:房东/2:中介/3:二房东/4/租客
        case 1:
          userType = '房东';
          break;
        case 2:
          userType = '中介';
          break;
        case 3:
          userType = '二房东';
          break;
        case 4:
          userType = '租客';
          break;
        default:
          userType = '未知';

      }

      var restrictDisp = '';
      switch (res.restrict) {
        //1-不限，2-男，3-女
        case 1:
          restrictDisp = '不限男女';
          break;
        case 2:
          restrictDisp = '限男生';
          break;
        case 3:
          restrictDisp = '限女生';
          break;
        default:
          restrictDisp = '不限';
      }

      var article = {};
      res.head_image = res.img_url + '/' + res.head_image;
      res.typeDisp = res.invitation_type == 1 ? '出租' : '租房';
      res.roleDisp = userType;
      res.rental = app.isNothing(res.rental) ? '未知' : res.rental;
      res.restrictDisp = restrictDisp;
      var r = app.isNothing(res.house_type_room) ? '0房' : res.house_type_room + '房';
      var o = app.isNothing(res.house_type_office) ? '0厅' : (res.house_type_office + 1 + '厅');
      var d = app.isNothing(res.house_type_defend) ? '0卫' : (res.house_type_defend + 1 + '卫');
      res.roomDisp = r + '.' + o + '.' + d;
      if (res.pictures) {
        res.images = [];
        var imgs = res.pictures.split(',');
        for (var i = 0; i < imgs.length; i++) {
          res.images.push(res.img_url + '/' + imgs[i]);
        }
        // res.images.push('http://www.chinaanno.com/static/index/images/yjx_07.jpg');
        // res.images.push('http://www.chinaanno.com/static/index/images/yjx_07.jpg');
        // res.images.push('http://api.zpl9386.com/uploads/img/20190512/9d8968eb924d786b70ff31c2f7b3e26c.jpg');
        // res.images.push('http://api.zpl9386.com/uploads/img/20190512/9d8968eb924d786b70ff31c2f7b3e26c.jpg');

      }

      if (res.payment_method) {
        switch (res.payment_method) {
          case 1:
            res.paymentDisp = '压1付1';
            break;
          case 2:
            res.paymentDisp = '压2付1';
            break;
          case 3:
            res.paymentDisp = '压3付1';
            break;
          case 4:
            res.paymentDisp = '压6付1';
            break;
          case 5:
            res.paymentDisp = '压1年付1';
            break;
          default:
            res.paymentDisp = '压1付1';
            break;
        }
      } else {
        res.paymentDisp = '压1付1';
      }

      res.area = app.isNothing(res.area) ? '未知' : res.area ;

      res.name = app.isNothing(res.name) ? '未知' : res.name;

      res.orientation = app.isNothing(res.orientation) ? '未知' : res.orientation;

      that.setData({
        article: res,
        subname: 'subname'
      });
      console.log(JSON.stringify(that.data));
    }
  },

  requestComment: function(id, index) {
    var that = this;
    app.http.reqestPaperReply(id, index, function(res) {
      that.setData({
        comment: res
      })
    }, function(res) {

    }, function(res) {

    });
  },

  replyInput: function(replyUid) {

  },

  onClickComment: function(res) {
    this.data.commentData.reply_id = res.currentTarget.dataset.id;
    this.data.commentData.reply_user_id = res.currentTarget.dataset.uid;
    this.setData({
      prompt: {
        show: true,
        type: 'text',
      }
    });
    console.log(JSON.stringify(this.data.commentData));
  },

  reply: function(id) {
    var that = this;
    that.data.commentData.invitation_id = that.data.id;
    that.data.commentData.user_id = app.globalData.data_list.id;
    that.data.commentData.type = that.data.commentData.reply_id == 0 ? 1 : 2;

    app.http.replyComment(that.data.commentData, function(res) {
      wx.showToast({
        title: '回复成功',
      });
      //todo update
    }, function(res) {
      wx.showToast({
        title: '回复失败',
      });
    }, function(res) {

    });
  },

  promptInput: function(res) {
    this.data.commentData.content = res.detail.value;
  },

  hidePrompt: function(res) {
    var act = res.currentTarget.dataset.act;
    var id = res.currentTarget.dataset.id;
    // promptValue = res.detail.value;
    // this.data.commentData.content = promptValue;


    this.setData({
      prompt: {
        show: false,
      }
    });
    if (act == "confirm" && this.data.commentData.content) {
      this.reply(id)
    }
  }

})