// pages/authentication/authentication.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    radioItems: [{
        name: '租客',
        value: '1',
        checked: true
      },
      {
        name: '房东',
        value: '2'
      },
      {
        name: '中介',
        value: '3'
      },
      {
        name: '二房东',
        value: '4'
      }
    ],
    user_id: '',
    phone: '',
    code: '',
    role: 1,
  },

  /**
   * 生命周期函数--监听页面加载 
   */
  onLoad: function(options) {
    app.page.onLoadHandler(this, '完善资料');
    this.setData({
      phone: '',
      user_id: app.globalData.data_list.id
    })
    console.log("user_id: " + this.data.user_id + " # " + this.data.phone);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  initData: function(options) {

  },

  getPhoneValue: function(e) {
    console.log("requestValid: res : " + JSON.stringify(e.detail));
    this.setData({
      phone: e.detail.value,
    });
    console.log("code: " + this.data.code);

  },

  getVerifyCode: function(e) {
    console.log("requestValid: res : " + JSON.stringify(e.detail));
    this.setData({
      code: e.detail.value,
    });
    console.log("user_id: " + this.data.user_id + " # " + this.data.phone);

  },

  requestValid: function(res) {
    var reqdata = {};
    reqdata.phone = this.data.phone;
    reqdata.user_id = this.data.user_id;
    app.http.bindVerify(reqdata, function(res) {
      wx.showToast({
        title: '成功获取验证码，请查看您的短信',
        icon: 'success',
        duration: 1500,
      })
    }, function(res) {
      wx.showToast({
        title: '获取验证码失败，' + res.msg,
        icon: 'warn',
        duration: 1500,
      })
    }, function(res) {

    })
  },

  bindUser: function(res) {
    // console.log("requestValid: res : " + JSON.stringify(res));
    // var resValue = res.detail.value;
    var reqData = {
      phone: this.data.phone,
      code: this.data.code,
      role: this.data.role,
      user_id: this.data.user_id
    };

    app.http.bindUser(reqData, function(res) {
      console.log("success" + JSON.stringify(res));
      wx.switchTab({
        url: '/pages/home/home'
      })
    }, function(res) {
      console.log("failure" + JSON.stringify(res));
      wx.showToast({
        title: '获取验证码失败，' + res.msg,
        icon: 'warn',
        duration: 1500,
      })

    }, function(res) {
      console.log("compile" + JSON.stringify(res));

    })
  },

  radioChange: function(e) {
    console.log('radio发生change事件，携带value值为：', e.detail.value);

    var radioItems = this.data.radioItems;
    for (var i = 0, len = radioItems.length; i < len; ++i) {
      radioItems[i].checked = radioItems[i].value == e.detail.value;
    }

    this.setData({
      radioItems: radioItems,
      role: e.detail.value
    });
  },
})