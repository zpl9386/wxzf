// pages/message/message.js
var app = getApp();

Page({

  data: {
    isAdminOrSuper:true,
    showAllComment:true,
    cur: -1

  },

  // 评论、回复 自动获取焦点 延时
  __timeout: null,

  // longtap会同时触发tap事件
  __tapLock: false,

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    app.page.onLoadHandler(this, '消息');
    this.requestMessage(1);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },


  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   * @param options
   */
  onShow: function() {

    let that = this;

    app.page.onLoadHandler(this);
    that.init();
    that.initForTest();
  },

  /**
   * 初始化
   * @param options
   */
  init: function() {

    var that = this;
    // var isAdmin = app.isAdmin(), canIAddArticle = app.canIAddArticle(), canIAddActivity = app.canIAddActivity();

    // page初始化
    // app.page.onInitHandler(this, null, isAdmin && canIAddActivity || canIAddArticle, canIAddArticle ? 'article' : 'activity');
    // 搜索初始化
    // app.page.initSearch(this, { placeholder: '请输入活动标题' });
    app.page.initGetterWithList(this);

    this.setData({

      list: [], // 数据列表
      page: {
        pageno: 1,
        pagemax: 1,
        pagesize: 10
      }, // 分页
      comment: {
        ani: '',
        value: '',
        idx: 0,
        placeholder: ''
      },
      isActivity: true,
      isAdminOrSuper: false

    });

    app.page.showLoadingMore(that, true);
    this.onPullDownRefresh();

  },

  // 更新未读消息
  updateNoReadArticles: function(refresh) {

    let noReadArticles = app.getNoReadArticles();

    if (!app.isAdmin()) {

      this.setData({
        noReadArticles: noReadArticles
      });
      noReadArticles.article && refresh && this.onPullDownRefresh();

    }

  },

  // 下拉刷新 请求数据前执行
  prePullDownRefreshHandler: function() {

    let noReadArticles = this.data.noReadArticles;

    if (noReadArticles && noReadArticles.article) {

      noReadArticles.article = '';
      this.data.noReadArticles = noReadArticles;
      app.setNoReadArticles(noReadArticles);

    }

  },

  /**
   * 下拉刷新
   */
  onPullDownRefresh: function() {

    let that = this,
      isActivity = this.data.isActivity,
      res = true;

    // if (this.isSearching()) { // 搜索

    //   if (this.data.search.keywords.length == 0) { // 关键字为空

    //     res = false;
    //     app.showLoading(false, that);
    //     app.toastErr('搜索框不能为空');
    //     wx.stopPullDownRefresh();

    //   } else
    //    if (this.data.list.length > 0) {  // 清空list

    //     this.setData({ list: [] });


    // }

    // 刷新前执行
    !isActivity && this.prePullDownRefreshHandler();

    res && app.page.loadingBeforeHandler(this.data.page, null, true, function(result, data) {

      // if (isActivity) {
      //   that.loadActivityList(true, data);
      // } else {
      //   that.loadArticleList(true, data);
      // }
      that.initForTest;
    })

  },

  /**
   * 上拉加载更多
   */
  onReachBottom: function() {

    let that = this,
      isActivity = this.data.isActivity,
      res = !this.isSearching() || this.data.search.keywords.length > 0;

    res && app.page.loadingBeforeHandler(this.data.page, null, false, function(result, data) {

      if (result) {

        if (isActivity) {
          that.loadActivityList(false, data);
        } else {
          that.loadArticleList(false, data);
        }

      }

    })

  },

  initForTest: function() {
    var refresh = true;
    var that = this,
      list = refresh ? [] : (this.getListOf() || []),
      isEmpty = !!!list.length,
      str = this.getListStrOf(),
      param = {};
    refresh && (param[str] = []);

    var picUrl = 'https://dg-fd.zol-img.com.cn/t_s2000x2000/g5/M00/09/08/ChMkJ1ggMQyIIFU_AAAd8r0Clp0AAXjFwCVGk0AAB4K408.jpg';
    var picImageSmall1 = 'http://www.chinaanno.com/static/index/images/yjx_07.jpg';
    var picImageSmall2 = 'http://www.chinaanno.com/static/index/images/yjx_09.jpg';
    var artList = [];
    for (var i = 0; i < 3; i++) {
      var artItem = {};
      artItem.name = 'hu';
      artItem.avatar = picUrl;
      artItem.cuid = 'cl-dodgerblue';
      artItem.canFavorite = true;
      artItem.delete = false;
      artItem.index = i;
      artItem.title = 'title' + i;
      artItem.content = 'content: ' + i;
      artItem.overflow = true;
      artItem.expand = true;
      artItem.images = [];
      artItem.images.push(picImageSmall1);
      artItem.images.push(picImageSmall2);
      artItem.intime = '4月8日';
      artItem.commentList = [];
      for (var j = 0; j < 5; j++) {
        var sub = {};
        sub.name = 'author' + j;
        sub.rename = 'reauthor' + j;
        sub.content = 'good ,i like ' + i;
        artItem.commentList.push(sub);
      }
      artList.push(artItem);
    }

    // param = app.getExtendData(refresh, str, list.length, app.bdp.parseArticleList(artList));
    param = {};
    param.list = artList;
    isEmpty && (isEmpty = !!!artList.length);
    param['isEmpty'] = isEmpty;
    param['hasMore'] = false;
    that.setData(param);
  },

  /**
   * 获取班级圈数据
   * @param refresh
   * @param data
   */
  loadArticleList: function(refresh, data) {

    var that = this,
      list = refresh ? [] : (this.getListOf() || []),
      isEmpty = !!!list.length,
      str = this.getListStrOf(),
      param = {};

    // data.search = that.data.search.keywords || ''
    app.http.ajxGetArticleList(data, function(res) {

      refresh && (param[str] = []);
      if (res && res.data) {

        param = app.getExtendData(refresh, str, list.length, app.bdp.parseArticleList(res.data));
        isEmpty && (isEmpty = !!!res.data.length);

      }
      param['isEmpty'] = isEmpty;
      param['hasMore'] = app.page.loadingAfterHandler(that.data.page, res);
      that.setData(param);

    }, null, function() {
      app.showLoading(false, that);
      wx.stopPullDownRefresh();
    })

  },

  initTestData: function() {

  },

  /**
   * 图片预览
   * @param e
   */
  previewImg: function(e) {

    var id = e.currentTarget.dataset.id,
      idx = e.currentTarget.dataset.idx,
      articles = this.getListOf(),
      images = articles[id].originalImages || articles[id].images;

    wx.wrap.previewImg(images[idx], images);

  },

  /**
   * 未读消息
   */
  bindNoReadList: function() {

    let noReadArticles = this.data.noReadArticles;

    if (noReadArticles) {

      noReadArticles.avatar = '';
      noReadArticles.noReadNum = 0;
      app.setNoReadArticles(noReadArticles);
      this.updateNoReadArticles(false);

    }

    app.navigateTo('/pages/utility/article/messages/messages');

  },

  /**
   * 更多操作
   * @param e
   */
  bindMorTapHandler: function(e) {

    let that = this,
      idx = e.currentTarget.dataset.idx,
      article = this.getListOf(idx);

    app.showCustomActionSheet(article, function(action) {

      if (action == 'favorite') {
        app.addFavorite(article, 'article');
      } else if (action == 'delete') {
        that.delArticle(idx);
      }

    })

  },

  /**
   * 删除
   * @param idx
   */
  delArticle: function(idx) {

    let that = this,
      list = this.getListOf(),
      str = this.getListStrOf(),
      empStr = this.getEmptyStr();

    app.showLoading(true, that);
    app.http.ajxDelArticle(list[idx].id, function() {

      that.setData(app.deleteHandler(list, idx, str, empStr));

    }, null, function() {
      app.showLoading(false, that);
    })

  },

  /**
   * 展开或折叠内容
   * @param e
   */
  bindExpand: function(e) {

    let idx = e.currentTarget.dataset.idx,
      article = this.getListOf(idx),
      str = this.getListStrOf(idx),
      param = {}

    param[str + '.expand'] = !article.expand;
    this.setData(param);

  },

  /**
   * 显示所有评论
   * @param e
   */
  bindShowAllComment: function(e) {

    let idx = e.currentTarget.dataset.idx,
      article = this.getListOf(idx);

    app.navigateTo('/pages/utility/article/detail/detail?id=' + article.id)

  },

  /**
   * 点赞或取消点赞
   * @param e
   */
  bindPraise: function(e) {

    let that = this,
      idx = e.currentTarget.dataset.idx,
      article = this.getListOf(idx);

    this.hideActionsMenu();

    if (article.approve) {

      app.http.ajxUnApproveArticle(article.id, function(res) {

        // 更新点赞列表
        that.updatePraiseList(idx, false);

      })

    } else {

      app.http.ajxApproveArticle(article.id, function(res) {

        // 更新点赞列表
        that.updatePraiseList(idx, true);

      })

    }

  },

  /**
   * 评论点击事件
   * @param e
   */
  bindComment: function(e) {

    let idx = e.currentTarget.dataset.idx;

    this.setData({
      comment: {
        ani: 'In',
        value: '',
        idx: idx,
        placeholder: '评论'
      }
    });
    this.dialogFocus();
    this.hideActionsMenu();

  },

  // 获取焦点
  dialogFocus: function() {

    clearTimeout(this.__timeout);

    this.__timeout = setTimeout(() => {

      this.setData({
        'comment.focus': true
      });

    }, 500);

  },

  /**
   * 回复点击事件
   * @param e
   */
  bindReply: function(e) {

    let idx = e.currentTarget.dataset.idx,
      sub = e.currentTarget.dataset.sub,
      article = this.getListOf(idx),
      comment = article.commentList[sub];

    // 不能参与
    if (this.data.isAdminOrSuper) {
      return;
    }

    // 自己不能回复自己
    // if (comment.pid == app.userData.getUserID()) { return }

    if (this.__tapLock) {
      this.__tapLock = false
    } else {
      this.replyComment(idx, comment)
    }

  },

  // 回复
  replyComment: function(index, comment) {

    this.setData({
      comment: {
        ani: 'In',
        value: '',
        idx: index,
        placeholder: '回复 ' + comment.name,
        cid: comment.id
      }
    });
    this.dialogFocus();

  },

  // 评论内容
  dialogInputChanged: function(e) {

    this.data.comment.value = e.detail.value;

  },

  // 发表
  dialogSendHandler: function(e) {

    let that = this,
      comment = this.data.comment,
      idx = comment.idx,
      article = this.getListOf(idx),
      id = article.id,
      cid = comment.cid || 0;

    this.dialogCancelHandler();

    if (comment.value.length > 0) {

      app.http.ajxAddComment(id, cid, comment.value, function(res) {

        // 更新评论列表
        that.updateCommentList(idx, res.commentData);

      })

    } else {
      app.toastErr('评论不能为空！');
    }

  },
  // 隐藏dialog
  dialogCancelHandler: function(e) {

    this.setData({
      'comment.ani': 'Out',
      'comment.focus': false
    })

  },

  // 长按事件
  bindReplyLongTap: function(e) {

    let that = this,
      idx = e.currentTarget.dataset.idx,
      sub = e.currentTarget.dataset.sub,
      article = this.getListOf(idx),
      comment = article.commentList[sub],
      itemList = [];

    // 不能参与
    if (this.data.isAdminOrSuper) {
      return
    }

    // 自己不能回复自己
    // if (comment.pid != app.userData.getUserID()) {
    itemList.push('回复');
    // }

    // 是否可删除
    if (comment.delete) {
      itemList.push('删除');
    }

    if (itemList.length > 0) {
      // lock tap event
      this.__tapLock = true;

      app.showActionSheet(itemList, function(tapIndex, itemName) {

        if (itemName == '回复') {
          that.replyComment(idx, comment);
        } else {
          that.deleteReply(idx, sub, comment);
        }

      })
    }

  },

  // 删除评论
  deleteReply: function(index, sub, comment) {

    let that = this,
      article = this.getListOf(index),
      comments = article.commentList,
      str = this.getListStrOf(index) + '.commentList',
      param = {};

    app.showCfmModal(null, null, function() {

      app.showLoading(true, that);
      app.http.ajxDelComment(comment.id, function() {

        comments.splice(sub, 1);
        param[str] = comments;
        that.setData(param);

      }, null, function() {
        app.showLoading(false, that);
      })

    })

  },

  // 点赞列表
  updatePraiseList: function(idx, approve) {

    let that = this,
      article = this.getListOf(idx),
      str = this.getListStrOf(idx),
      param = {};

    app.showLoading(true, that);
    app.http.ajxGetArticleApproveList(article.id, function(res) {

      if (res && res.data) {

        article.approveList = res.data || [];
        article.approveList.reverse();
        article.approve = approve;
        param[str] = article;
        that.setData(param);
      }

    }, null, function() {
      app.showLoading(false, that);
    })

  },

  // 评论列表
  updateCommentList: function(idx, data) {

    let that = this,
      article = this.getListOf(idx),
      str = this.getListStrOf(idx),
      param = {};

    data && article.commentList.push(data);
    param[str] = article;
    that.setData(param);

  },

  // 弹出actions菜单
  popupActionsMenu: function(e) {

    let idx = e.currentTarget.dataset.idx,
      cur = this.data.cur;

    if (cur != -1) {
      this.showActions(cur, false);
    }
    cur = idx == cur ? -1 : idx;
    if (cur != -1) {
      this.showActions(cur, true);
    }

    this.data.cur = cur;

  },
  // 隐藏actions菜单
  hideActionsMenu: function() {

    let cur = this.data.cur;

    if (cur != -1) {
      this.showActions(cur, false);
    }
    this.data.cur = -1;

  },

  // 显示或隐藏 点赞、评论弹窗
  showActions: function(idx, show) {

    let that = this,
      article = this.getListOf(idx),
      str = this.getListStrOf(idx),
      param = {};

    article.active = show;
    param[str] = article;
    this.setData(param);

  },

  // 用户头像或姓名点击 事件
  bindUserTap: function(e) {

    let cid = e.currentTarget.dataset.id;

    if (this.__tapLock) {
      this.__tapLock = false;
    } else {
      cid && app.navigateTo('/pages/utility/homepage/homepage?cid=' + cid);
    }

  },

  requestMessage: function(index) {
    var that = this;
    app.http.reqMessage(index, function(res) {
      that.importData(res);
    }, function(res) {

    }, function() {

    });
  },

  importData(message) {
    this.setData({
      dataCount: message.dataCount,
      paperList: message.data
    });
    console.log(JSON.stringify(this.data.paperList));
  }
})