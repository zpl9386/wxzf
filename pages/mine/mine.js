// pages/mine/mine.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    app.page.onLoadHandler(this, '个人中心');
    this.init()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  // 初始化
  init: function () {

    var configs = [];

    let myConfig = [] 
    myConfig.push({ id: 'article', url: '/pages/utility/article/article', content: '我的发布', icon: 'ft-write', iconStyle: 'color:#8B8989' })

    myConfig.push({ id: 'article', url: '/pages/utility/article/article', content: '我的收藏', icon: 'ft-favor', iconStyle: 'color:#8B8989' })

    myConfig.push({ id: 'article', url: '/pages/utility/article/article', content: '联系我们', icon: 'ft-setting', iconStyle: 'color:#8B8989' })

    myConfig.push({ id: 'article', url: '/pages/paper_detail/paper_detail', content: '反馈', icon: 'ft-zuoye', iconStyle: 'color:#反馈' })

    myConfig.push({ id: 'article', url: '/pages/post_paper_type/post_paper_type', content: '发布', icon: 'ft-attendance', iconStyle: 'color:#8B8989' })
    configs.push(myConfig)

    this.setData({
      // userInfo: userInfo,
      configs: configs,
    })

  },
})