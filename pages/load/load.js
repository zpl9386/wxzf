const app = getApp();
Page({
  data: {
    //判断小程序的API，回调，参数，组件等是否在当前版本可用。  
    canIUse: wx.canIUse('button.open-type.getUserInfo')
  },
  onLoad: function() {
   this.getUserOnLoad();
  },

  getUserOnLoad: function() {
    if (app.globalData.userInfo) {
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true
      });
      this.login();
    } else if (this.data.canIUse) {
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        });
        this.login();
      }

    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          app.globalData.userInfo = res.userInfo
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          });
          this.login();
        }
      })
    }
  },

  bindGetUserInfo: function(e) {
    if (e.detail.userInfo) {
      //用户按了允许授权按钮 
      var that = this;
      //插入登录的用户的相关信息到数据库
      wx.request({
        url: app.globalData.urlPath + 'user/add',
        data: {
          openid: getApp().globalData.openid,
          nickName: e.detail.userInfo.nickName,
          avatarUrl: e.detail.userInfo.avatarUrl,
          province: e.detail.userInfo.province,
          city: e.detail.userInfo.city
        },
        header: {
          'content-type': 'application/json'
        },
        success: function(res) {
          //从数据库获取用户信息
          that.queryUsreInfo();
          console.log("插入小程序登录用户信息成功！");
        }
      });
      //授权成功后，跳转进入小程序首页
      wx.switchTab({
        url: '/pages/index/index'
      })
    } else {
      //用户按了拒绝按钮
      wx.showModal({
        title: '警告',
        content: '您点击了拒绝授权，将无法进入小程序，请授权之后再进入!!!',
        showCancel: false,
        confirmText: '返回授权',
        success: function(res) {
          if (res.confirm) {
            console.log('用户点击了“返回授权”')
          }
        }
      })
    }
  },
  //获取用户信息接口
  queryUsreInfo: function() {
    wx.request({
      url: app.globalData.urlPath + 'user/userInfo',
      data: {
        openid: app.globalData.openid
      },
      header: {
        'content-type': 'application/json'
      },
      success: function(res) {
        console.log(res.data);
        getApp().globalData.userInfo = res.data;
      }
    });
  },

  getUserInfo: function(e) {
    console.log(e)
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  },

  home: function() {
    wx.showToast({
      title: '登录成功',
    });
    wx.switchTab({
      url: '/pages/home/home'
    })
  },

  goMain:function(){
    wx.showToast({
      title: '登录成功',
    });
  },

  login: function() {
    wx.login({
      success(res) {
        if (res) {
          // 发起网络请求
          app.http.login(res.code, function(res) {
            if (res) {
              app.globalData.data_list = res;
              app.http.setAuthorization(res.session_key);
              app.http.setUserId(res.id);
              if (res.user_status == '0') { 
                wx.redirectTo({
                  url: '/pages/authentication/authentication',
                })
                return;
              } else {
                wx.switchTab({
                  url: '/pages/home/home'
                })
              }
            } else {
              //todo 
              wx.showToast({
                title: '登录失败',
              })
            }
          
          }, function(res) {
            wx.showToast({
              title: '请求失败',  
            })
          }, function(res) {

          })
        } else {
          console.log('登录失败！' + res.errMsg)
        }
      }
    })

  },

})