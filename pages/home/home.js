// pages/home/home.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    paperList: [],
    data_list: {
      img_url: "https://api.zpl9386.com",
      pageIndex: "1",
      pageCount: "1",
      dataCount: "0",
      data: [{
        id: "1",
        user_id: "1",
        title: "我想在宝安找个房间",
        invitation_type: "1",
        type: "1",
        rental: "6000",
        latitude: "114.018957",
        longitude: "22.651442",
        restrict: "1",
        pictures: "uploads/img/20190512/9d8968eb924d786b70ff31c2f7b3e26c.jp",
        create_time: "2019.05.12 16:07",
        province: "广东省",
        city: "深圳市",
        district: "龙华新区",
        nick_name: "霜霜",
        head_image: "",
        role: "3"
      }]
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    app.page.onLoadHandler(this, '房猪猪');

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.requestData();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {},

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  requestData: function(index) {
    var that=this;
    var req = {
      pageIndex: 0,
      title: '',
      city: '',
      // type: 1,//1:房东直租/2:转租/3:合租/4:求合租/5:求整租）
    }
    app.http.requestPaperlist(req, function(res) {
      if (res && res.data && res.data.length > 0) {
        that.importData(res);
        wx.showToast({
          title: '成功获取帖子列表',
        })
      } else {
        wx.showToast({
          title: '没有符合条件的帖子',
        })
      }

    }, function(res) {
      wx.showToast({
        title: '获取帖子列表失败',
      })
    }, function(res) {

    })
  },

  initTestData: function() {
    var data_list = {
      img_url: "https://api.zpl9386.com",
      pageIndex: "1",
      pageCount: "1",
      dataCount: "0",
      data: [{
        id: "1",
        user_id: "1",
        title: "我想在宝安找个房间",
        invitation_type: "1",
        type: "1",
        rental: "6000",
        latitude: "114.018957",
        longitude: "22.651442",
        restrict: "1",
        pictures: "uploads/img/20190512/9d8968eb924d786b70ff31c2f7b3e26c.jp",
        create_time: "2019.05.12 16:07",
        province: "广东省",
        city: "深圳市",
        district: "龙华新区",
        nick_name: "霜霜",
        head_image: "",
        role: "3"
      }]
    };

    // this.setData({
    //   data_list: data_list
    // });
    this.importData(data_list);

  },

  importData: function(data) {
    if (data && data.data && data.data.length > 0) {
      for (var i = 0; i < data.data.length; i++) {
        var roleDisp = "房东";
        var role = data.data[i].role;
        if (role == 1) {
          roleDisp = "房东";
        } else if (role == 2) {
          roleDisp = "中介";
        } else if (role == 3) {
          roleDisp = "二房东";
        } else if (role == 4) {
          roleDisp = "租客";
        }
        data.data[i].roleDisp = roleDisp;

        data.data[i].pic = [];
        // data.data[i].pic.push(data.img_url + data.data[i].pictures); //需加null检查
        for (var j = 0; j < 4; j++) {
          data.data[i].pic.push("https://ossweb-img.qq.com/images/lol/web201310/skin/big1000" + (j + 1) + ".jpg"); //测试，上行为正式
        }

        data.data[i].timeDisp = data.data[i].create_time.substring(5);
        data.data[i].head_image = data.img_url + data.data[i].head_image;
      }
      this.setData({
        paperList: Object.assign([], this.data.paperList, data.data),
      });
      console.log(JSON.stringify(this.data.paperList));

    } else {
      console.log("data isEmpty");
    }
  },

  onClickItem: function(res) {
    var id = res.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../paper_detail/paper_detail?id=' + id,
    })
  },

  publish: function() {
    wx.navigateTo({
      // url: '../post_paper_type/post_paper_type',
      url:'../paper_edit/paper_edit'
    });
  }

})