// pages/rent/rent.js
var app = getApp();
Page({

  /**
   * 页面的初始数据 
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    app.page.onLoadHandler(this, '直租');
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
this.requestData();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  requestData: function (index) {
    var req = {
      pageIndex: 1,
      title: '',
      city: '',
      // type: ''
    }
    var that=this;
    app.http.requestLeaselist(req, function (res) {
      if (res && res.data && res.data.length > 0) {
        that.importData(res);
        wx.showToast({
          title: '成功获取帖子列表',
        })
      } else {
        wx.showToast({
          title: '没有符合条件的帖子',
        })
      }

    }, function (res) {
      wx.showToast({
        title: '获取帖子列表失败',
      })
    }, function (res) {

    })
  },

  importData: function (data) {
    if (data && data.data && data.data.length > 0) {
      for (var i = 0; i < data.data.length; i++) {
        var roleDisp = "房东";
        var role = data.data[i].role;
        if (role == 1) {
          roleDisp = "房东";
        } else if (role == 2) {
          roleDisp = "中介";
        } else if (role == 3) {
          roleDisp = "二房东";
        } else if (role == 4) {
          roleDisp = "租客";
        }
        data.data[i].roleDisp = roleDisp;

        data.data[i].pic = [];
        // data.data[i].pic.push(data.img_url + data.data[i].pictures); //需加null检查
        for (var j = 0; j < 4; j++) {
          data.data[i].pic.push("https://ossweb-img.qq.com/images/lol/web201310/skin/big1000" + (j + 1) + ".jpg"); //测试，上行为正式
        }

        data.data[i].timeDisp = data.data[i].create_time.substring(5);
        data.data[i].head_image = data.img_url + data.data[i].head_image;
        data.data[i].titleDisp = data.data[i].house_type_room + '室' + data.data[i].house_type_office + '厅' + data.data[i].house_type_defend+'卫.'+data.data[i].name;
        data.data[i].create_time = data.data[i].create_time.substring(5);
      }
      this.setData({
        paperList: Object.assign([], this.data.paperList, data.data),
      });
      console.log(JSON.stringify(this.data.paperList));

    } else {
      console.log("data isEmpty");
    }
  },

  detailPaper:function(res){
    var id = res.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../paper_detail/paper_detail?id=' + id,
    })
  }


})