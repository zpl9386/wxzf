// pages/paper_write/paper_write.js
var app = getApp();
Page({

  data: {

    formData: [],
    popSel: {}

  },

  // 保存 提交事件
  submitHandler: null,
  // 默认 弹出确认框
  needShowCmfModal: true,

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    app.page.onLoadHandler(this, '编辑');
    this.initOpinionForm();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  // 获取说明
  bindGetDescription: function (e) {

    let t = e.currentTarget.dataset.t
    app.navigateTo('/pages/utility/richText/richText', { src: 'description', id: t })

  },

  // 返回并刷新
  backWithFresh: function () {

    let options = this.data.options || {}, cb = options.cb || '';
    (cb == 'back') ? app.back() : app.backWithFresh();

  },

  // 错误返回
  backWithError: function (msg) {

    app.showInfoModal('', msg || '数据加载失败!', function () {
      app.back()
    })

  },

  // 远程图片解析->form
  updateFormImages: function (name, urls) {

    let formData = this.data.formData, idx = app.http.form.getFormItemIndex(formData, name),
      formItem = (idx >= 0) ? formData[idx] : null, files = [], length = app.http.getWebUrl().length,
      str = 'formData[' + idx + '].files', param = {}

    if (formItem) {

      urls.forEach(function (url) {

        // value 为相对地址
        files.push({ url: url, value: url.substr(length) })

      })

      param[str] = files
      this.setData(param)

    }

  },

  // 远程文件解析->form
  updateFormAttachments: function (name, urls) {

    let formData = this.data.formData, idx = app.http.form.getFormItemIndex(formData, name),
      formItem = (idx >= 0) ? formData[idx] : null, length = app.http.getWebUrl().length,
      files = [], value = []

    if (formItem) {
      for (var i in urls) {
        let url = urls[i], value = url.subStr(length)
        files.push({ name: wx.util.lastPathComponent(value), ext: wx.util.fileExtension(value) })
        value.push(value)
      }
      var str = 'formData[' + idx + '].files', param = {}
      param[str] = files
      str = 'formData[' + idx + '].value'
      param[str] = value
      this.setData(param)
    }

  },


  //// 评一评 ////////////////////////////////////////////
  initRatingForm: function () {

    let options = this.data.options || {}, imgUrl = options.img || '', cid = options.cid || 0, inalterable = options.inalterable || 0,
      formData = [
        { title: '用户', name: 'contactIds', type: 'contacts', value: [], contacts: [], group: 'student', inalterable: inalterable, empty: 0 },
        { title: '评价项', name: 'items', type: 'ratingItems', value: [], items: [], empty: 0 },
        { title: '评分', name: 'rating', type: 'rating', value: 0, stars: [1, 2, 3, 4, 5], multiplier: 1, empty: 1 },
        { title: '内容', name: 'content', type: 'editor', value: '', hideTitle: true, empty: 1 },
        { title: '图片', name: 'images', type: 'images', value: [], files: [], hideTitle: true, empty: 1 },
      ]

    wx.wrap.setNavigationBarTitle('评一评')

    // 初始数据
    let contactIdx = app.http.form.getFormItemIndex(formData, 'contactIds'),
      imgsIdx = app.http.form.getFormItemIndex(formData, 'images'),
      tagsIdx = app.http.form.getFormItemIndex(formData, 'items')
    if (cid && contactIdx >= 0) { // 已选择一个联系人
      if (formData[contactIdx].type == 'contact') {
        formData[contactIdx].value = cid
      } else {
        formData[contactIdx].value.push(cid)
      }
    }
    if (imgUrl && imgUrl.length > 0 && imgsIdx >= 0) { // 已选择图片
      formData[imgsIdx].files.push({ url: decodeURI(imgUrl), value: '' })
    }

    this.setData({ formData: formData })
    // 获取联系人信息
    if (cid && contactIdx >= 0) {
      this.getContactDetail(contactIdx)
    }
    // 提交事件
    this.submitHandler = this.addRating

  },

  // 提交评一评
  addRating: function (formData, e) {
    let that = this
    app.http.ajxAddGrowth(formData, e, function () {
      // app.back()
      that.backWithFresh()
    })
  },

  // 评一评图片增加
  ratingImagesAdded: function (idx, len) {

    let that = this
    console.log(`ratingImagesAdd: ${idx} -- ${len}`)

  },

  // 评一评图片减少
  ratingImagesDeleted: function (idx, subIdx) {

    let that = this
    console.log(`ratingImagesDeleted: ${idx} --- ${subIdx}`)

  },

  //// 评一评 ////////////////////////////////////////////
  initOpinionForm: function () {

    let options = this.data.options || {}, imgUrl = options.img || '', cid = options.cid || 0, inalterable = options.inalterable || 0,
      formData = [
        { title: '用户', name: 'contactIds', type: 'contacts', value: [], contacts: [], group: 'student', inalterable: inalterable, empty: 0 },
        { title: '印象', name: 'tags', type: 'tags', value: [], tags: [], editable: true, maxCount: 3, deleteTags: 'deleteTags', theme: this.data.theme || 'green', empty: 0 },
        { title: '内容', name: 'content', type: 'editor', value: '', hideTitle: true, empty: 1 },
        { title: '图片', name: 'images', type: 'images', value: [], files: [], hideTitle: true, empty: 1 },
        // 需要删除的标签
        { title: '', name: 'deleteTags', type: 'hidden', value: [], empty: 1 },
      ]

    wx.wrap.setNavigationBarTitle('评一评')
    let contactIdx = app.http.form.getFormItemIndex(formData, 'contactIds'),
      imgsIdx = app.http.form.getFormItemIndex(formData, 'images'),
      tagsIdx = app.http.form.getFormItemIndex(formData, 'tags')
    if (imgUrl && imgUrl.length > 0 && imgsIdx >= 0) { // 已选择图片
      formData[imgsIdx].files.push({ url: decodeURI(imgUrl), value: '' })
    }

    this.setData({ formData: formData })
    if (tagsIdx >= 0) {
      this.getTagsList(tagsIdx)
    }
    // 提交事件
    this.submitHandler = this.addOpinion

  },

  // 提交评一评
  addOpinion: function (formData, e) {
    let that = this
    app.http.ajxAddGrowthByTeacher(formData, e, function () {
      // app.back()
      that.backWithFresh()
    })
  },

})