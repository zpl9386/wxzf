// pages/paper_write/paper_write.js

var app = getApp();
Page({

  data: {

    formData: [],
    popSel: {}

  },

  // 保存 提交事件
  submitHandler: null,
  // 默认 弹出确认框
  needShowCmfModal: true,

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    app.page.onLoadHandler(this, '编辑');

    var formType = options && options.type || ''

    // page初始化
    app.page.onInitHandler(this)
    this.setData({
      options: options || {},
      formStyle: 'normal', submitFixed: true, submitText: '保存',
      popSel: { hideButtons: true, index: 0, cur: 0, values: [] }
    })
    this.initRatingForm();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  // 获取说明
  bindGetDescription: function (e) {

    let t = e.currentTarget.dataset.t
    app.navigateTo('/pages/utility/richText/richText', { src: 'description', id: t })

  },

  // 返回并刷新
  backWithFresh: function () {

    let options = this.data.options || {}, cb = options.cb || '';
    (cb == 'back') ? app.back() : app.backWithFresh();

  },

  // 错误返回
  backWithError: function (msg) {

    app.showInfoModal('', msg || '数据加载失败!', function () {
      app.back()
    })

  },

  // 远程图片解析->form
  updateFormImages: function (name, urls) {

    let formData = this.data.formData, idx = app.http.form.getFormItemIndex(formData, name),
      formItem = (idx >= 0) ? formData[idx] : null, files = [], length = app.http.getWebUrl().length,
      str = 'formData[' + idx + '].files', param = {}

    if (formItem) {

      urls.forEach(function (url) {

        // value 为相对地址
        files.push({ url: url, value: url.substr(length) })

      })

      param[str] = files
      this.setData(param)

    }

  },

  // 远程文件解析->form
  updateFormAttachments: function (name, urls) {

    let formData = this.data.formData, idx = app.http.form.getFormItemIndex(formData, name),
      formItem = (idx >= 0) ? formData[idx] : null, length = app.http.getWebUrl().length,
      files = [], value = []

    if (formItem) {
      for (var i in urls) {
        let url = urls[i], value = url.subStr(length)
        files.push({ name: wx.util.lastPathComponent(value), ext: wx.util.fileExtension(value) })
        value.push(value)
      }
      var str = 'formData[' + idx + '].files', param = {}
      param[str] = files
      str = 'formData[' + idx + '].value'
      param[str] = value
      this.setData(param)
    }

  },


  //// 评一评 ////////////////////////////////////////////
  initRatingForm: function () {


    let options = this.data.options || {}, imgUrl = options.img || '', cid = options.cid || 0, inalterable = options.inalterable || 0,
      formData = [
        { title: '标题', name: 'title', type: 'text', value: '', placeholder:'简述下房子特点', multiplier: 1, empty: 1 },
        { title: '租金', name: 'price', type: 'text', value: '', placeholder: '最大5位数', multiplier: 1, empty: 1 },
        { title: '位置', name: 'location', type: 'text', value: '', placeholder: '房子所在区域', multiplier: 1, empty: 1 },
        { title: '限制', name: 'restrict', type: 'text', value: '', placeholder: '对房客性别等方面要求', multiplier: 1, empty: 1 },
        { title: '描述', name: 'descript', type: 'editor', value: '', hideTitle: false, empty: 1 },
        { title: '图片', name: 'images', type: 'images', value: [], files: [], hideTitle: true, empty: 1 },
      ]


    let a = app.http.baseUrl;

    let imgsIdx = app.http.form.getFormItemIndex(formData, 'images'),
      tagsIdx = app.http.form.getFormItemIndex(formData, 'items')

    if (imgUrl && imgUrl.length > 0 && imgsIdx >= 0) { // 已选择图片
      formData[imgsIdx].files.push({ url: decodeURI(imgUrl), value: '' })
    }

    this.setData({ formData: formData })
    // 获取联系人信息
    // if (cid && contactIdx >= 0) {
    //   this.getContactDetail(contactIdx)
    // }
    // 提交事件
    this.submitHandler = this.addRating

  },

  // 提交评一评
  addRating: function (formData, e) {
    let that = this
    app.http.ajxAddGrowth(formData, e, function () {
      // app.back()
      that.backWithFresh()
    })
  },

  // 评一评图片增加
  ratingImagesAdded: function (idx, len) {

    let that = this
    console.log(`ratingImagesAdd: ${idx} -- ${len}`)

  },

  // 评一评图片减少
  ratingImagesDeleted: function (idx, subIdx) {

    let that = this
    console.log(`ratingImagesDeleted: ${idx} --- ${subIdx}`)

  },

 

  // 提交评一评
  addOpinion: function (formData, e) {
    let that = this
    app.http.ajxAddGrowthByTeacher(formData, e, function () {
      // app.back()
      that.backWithFresh()
    })
  },


  //// 提交 ////////////////////////////////////////////
  formSubmit: function (e) {

    let that = this, options = this.data.options || {}, formType = options.type || '', formData = this.data.formData,
      submitHandler = this.submitHandler

    if (this.needShowCmfModal) {

      app.showCfmModal('提示', '确定提交吗？', function () {

        // app.http.form.deserialize(e, formData)
        submitHandler && submitHandler(formData, e)

      })

    } else {

      // app.http.form.deserialize(e, formData)
      submitHandler && submitHandler(formData, e)

    }

  },

  writePaper:function(){
var req={
  user_id:'',
  title:'',
  invitation_type:'',
  type:'',
  rental:'',
  latitude:'',
  longitude:'',
  describe:'',
  pictures:'',
  province:'',
  city:'',
  district:'',
  namme:'',
  area:'',
  house_type_room:'',
  house_type_office:'',
  house_type_defend:'',
  payment_method:'',
  orientation:'',
}
  },

  // 取消
  bindCancel: function () {
    app.back()
  },

  /********** input ********************************************/
  bindFormInput: function (e) {

    let idx = e.currentTarget.dataset.idx, formItem = this.data.formData[idx]
    formItem.value = e.detail.value

  },

  /********** phones ********************************************/
  bindPhoneInput: function (e) {

    let idx = e.currentTarget.dataset.idx, sub = e.currentTarget.dataset.sub, formItem = this.data.formData[idx]

    formItem.phones[sub] = e.detail.value
    formItem.value = formItem.phones.join(',')

  },


  /********** file ********************************************/
  addFiles: function (e) {
    app.http.form.addFiles(e)
  },
  deleteFile: function (e) {
    app.http.form.deleteFile(e, this)
  },

  /********** images ********************************************/
  addImage: function (e) {

    let that = this, idx = e.currentTarget.dataset.idx
    app.http.form.addImage(idx, this, function (len) {
      that.imageAddHookHandler && that.imageAddHookHandler(idx, len)
    })

  },
  deleteImage: function (e) {

    let that = this, idx = e.currentTarget.dataset.idx, subIdx = e.currentTarget.dataset.sub
    app.http.form.deleteImage(idx, subIdx, this, function () {
      that.imageDeleteHookHandler && that.imageDeleteHookHandler(idx, subIdx)
    })

  },
  previewImg: function (e) {

    let idx = e.currentTarget.dataset.idx, sub = e.currentTarget.dataset.sub, formItem = this.data.formData[idx], images = []

    if (formItem.type == 'image' || formItem.type == 'images') {
      let files = formItem.files
      for (var i in files) {
        images.push(files[i].url)
      }
    }

    (images.length > 0) && wx.wrap.previewImg(sub < images.length ? images[sub] : images[0], images)

  },
  /********** image cover ********************************************/
  addImageCover: function (e) {

    let that = this, idx = e.currentTarget.dataset.idx
    app.http.form.addImage(idx, this)

  },
  /********** image handler *******************************************/
  // 图片添加
  imageAddHookHandler: function (idx, len) {

    let options = this.data.options || {}, formType = options.type || ''

    if (formType == 'rating') {
      this.ratingImagesAdded(idx, len)
    }

  },
  // 图片删除
  imageDeleteHookHandler: function (idx, subIdx) {

    let options = this.data.options || {}, formType = options.type || ''

    if (formType == 'rating') {
      this.ratingImagesDeleted(idx, subIdx)
    }

  }


})