
var _config = {

  /**
   * base url
   */
  baseUrl: 'https://api.chinaanno.com',  // .cn 测试环境， .com正式环境

  /**
   * 版本号：主版本号.次版本号.更新版本号
   */
  version: '0.1.0',

  /**
   * api 版本
   */
  apiVersion: '2.0',

  /**
   * 最低基础库
   */
  miniSDK: '1.7',

  /**
   * 设置tabBar颜色 - 目前只针对红色
   * theme: red or other
   */
  setTabBarTheme: function (theme) {

    if (wx.setTabBarItem && theme == 'red') {

      this.setTabBarThemeWith(theme, { "selectedColor": "#c6010c" });

    }

  },

  /**
   * 设置tabBar
   * theme: 颜色
   * option: tabBar items
   */
  setTabBarThemeWith: function (theme, option) {

    let configs = { "color": "#a0a0a0", "selectedColor": "#5ebc44", "backgroundColor": "#fff", "borderStyle": "black" },
      suffix = theme == 'red' ? '_red' : '',
      items = [
        { "index": 0, text: "首页", "selectedIconPath": "images/home_hilight" + suffix + ".png" },
        { "index": 1, text: "直租", "selectedIconPath": "images/contacts_hilight" + suffix + ".png" },
        { "index": 2, text: "消息", "selectedIconPath": "images/forum_hilight" + suffix + ".png" },
        { "index": 3, text: "我", "selectedIconPath": "images/mine_hilight" + suffix + ".png" }
      ];

    // 更新颜色
    if (wx.setTabBarStyle) {
      wx.util.extend(configs, option);
      wx.setTabBarStyle(configs);
    }

    // 更新选中图片
    wx.setTabBarItem && items.forEach(function (item) {

      wx.setTabBarItem(item);

    })

  },

  // 隐藏tabBar
  hideTabBar: function () {

    wx.hideTabBar && wx.hideTabBar({});

  },

  // 显示tabBar
  showTabBar: function () {

    wx.showTabBar && wx.showTabBar({});

  },

  /**
   * 设置tabBar角标
   * index: tabBar item index
   * badge: badge
   */
  setTabBarBadge: function (index, badge) {

    wx.setTabBarBadge && wx.setTabBarBadge({ 'index': index, 'text': badge > 99 ? '99+' : String(badge) });

  },

  /**
   * 清除tabBar角标
   * index: tabBar item index
   */
  removeTabBarBadge: function (index) {

    wx.removeTabBarBadge && wx.removeTabBarBadge({ 'index': index });

  },

  /**
   * 设置tabBar 圆点
   * index: tabBar item index
   */
  showTabBarRedDot: function (index) {

    wx.showTabBarRedDot && wx.showTabBarRedDot({ 'index': index });

  },

  /**
   * 清除tabBar 圆点
   * index: tabBar item index
   */
  hideTabBarRedDot: function (index) {

    wx.hideTabBarRedDot && wx.hideTabBarRedDot({ 'index': index });

  },

  /**
   * 设置首页圆点
   * badges: 消息未读提醒列表
   */
  setHomePageBadge: function (badges) {

    let num = 0;

    for (var i in badges) {

      num += badges[i];

    }

    if (num > 0) {

      this.showTabBarRedDot(0);

    } else {

      this.hideTabBarRedDot(0);

    }

  },

  /**
   * 设置班级圈圆点
   * article: 班级圈动态
   */
  setArticlePageBadge: function (article) {

    if (article.noReadNum || article.article) {

      this.showTabBarRedDot(1);

    } else {

      this.hideTabBarRedDot(1);

    }

  },

  /**
   * 设置我的页面圆点
   * sysNotice: 系统消息
   */
  setMinePageBadge: function (sysNotice) {

    if (sysNotice.noReadNum) {

      this.showTabBarRedDot(3);

    } else {

      this.hideTabBarRedDot(3);

    }

  },
};

module.exports = {

  config: _config

};