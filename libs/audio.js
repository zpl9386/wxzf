(function () {

  var __mgr = wx.getRecorderManager ? wx.getRecorderManager() : null, __interval = null, __duration = 0,
    // __innerAudioCtx = wx.createInnerAudioContext ? wx.createInnerAudioContext() : null,
    __innerAudioCtx = null, __audioCtx = null,
    __options = {
      opt: {
        duration: 60000,      // 时长
        sampleRate: 8000,    // 采样率
        numberOfChannels: 1,  // 通道数
        encodeBitRate: 16000, // 编码码率
        format: 'mp3'         // 音频格式 aac/mp3
      }
    };

  // 扩展
  function extend() {

    var args = arguments || [], length = args.length, target = length > 0 ? args[0] : '', src = length > 1 ? args[1] : '';

    if (target && src) {
      for (var i in src) {
        target[i] = src[i];
      }
    }

  }

  // 开始计时
  function __startTimming() {

    __duration = 0;
    __clearInterval();

    __interval = setInterval(function () {

      __duration += 1;
      typeof __options.onRecordProgress == "function" && __options.onRecordProgress((__duration / 2).toFixed(1));
      if (__duration * 2 >= __options.opt.duration) {
        clearInterval(__interval);
        __interval = null;
      }

    }, 500);

  }

  // 停止计时
  function __clearInterval() {

    __interval && clearInterval(__interval);
    __interval = null;

  }

  // recorder manager
  var manager = {

    __debug: 0,

    // 初始化录音
    initRecorder: function (options, onStart, onStop, onProgress, onErr) {

      extend(__options.opt, options);
      __options.onRecordStart = onStart;
      __options.onRecordStop = onStop;
      __options.onRecordProgress = onProgress;
      __options.onRecordErr = onErr;

      if (__mgr && !this.__debug) {

        // 结束录音
        __mgr.onStop((res) => {

          __clearInterval();
          typeof __options.onRecordStop == "function" && __options.onRecordStop(res.tempFilePath);

        })
        // 录音异常
        __mgr.onError((res) => {

          __clearInterval();
          typeof __options.onRecordErr == "function" && __options.onRecordErr(res && res.errMsg);

        })

      }

    },

    // 反初始化录音
    deinitRecorder: function () {

      __clearInterval();
      this.stopRecord();

    },

    // 开始录音
    startRecord: function () {

      __startTimming();
      typeof __options.onRecordStart == "function" && __options.onRecordStart();

      if (__mgr && !this.__debug) {

        __mgr.start(__options.opt);

      }
      else {

        wx.startRecord({

          success: function (res) {
            __clearInterval();
            typeof __options.onRecordStop == "function" && __options.onRecordStop(res.tempFilePath);
          },
          fail: function (res) {
            __clearInterval();
            typeof __options.onRecordErr == "function" && __options.onRecordErr(res && res.errMsg);
          }

        });

      }

    },

    // 结束录音
    stopRecord: function () {

      if (__mgr && !this.__debug) {

        __mgr.stop();

      }
      else {

        wx.stopRecord();

      }

    },

    // 语音播放初始化
    initPlayer: function (id, onPlay, onPause, onEnd, onErr) {

      __options.onAudioPlay = onPlay;
      __options.onAudioPause = onPause;
      __options.onAudioEnd = onEnd;
      __options.onAudioErr = onErr;

      __innerAudioCtx = wx.createInnerAudioContext ? wx.createInnerAudioContext() : null;
      if (__innerAudioCtx) {

        __innerAudioCtx.onPlay((res) => {

          typeof __options.onAudioPlay == "function" && __options.onAudioPlay();

        })
        __innerAudioCtx.onPause((res) => {

          typeof __options.onAudioPause == "function" && __options.onAudioPause();

        })
        __innerAudioCtx.onStop((res) => {

          typeof __options.onAudioEnd == "function" && __options.onAudioEnd();

        })
        __innerAudioCtx.onEnded((res) => {

          typeof __options.onAudioEnd == "function" && __options.onAudioEnd();

        })
        __innerAudioCtx.onError((res) => {

          console.log(res);
          typeof __options.onAudioErr == "function" && __options.onAudioErr(res && res.errMsg);

        })

      } else {

        __audioCtx = wx.createAudioContext('audio');

      }

    },

    // 反初始化 语音播放
    deinitPlayer: function () {

      if (__innerAudioCtx) {

        __innerAudioCtx.stop();
        __innerAudioCtx.destroy();
        __innerAudioCtx = null;

      } else {

        __audioCtx.pause();
        __audioCtx = null;

      }

    },

    // 播放声音
    playAudio: function (url) {

      if (__innerAudioCtx) {

        __innerAudioCtx.src = url;
        __innerAudioCtx.seek(0);
        __innerAudioCtx.play();

      } else {

        __audioCtx.setSrc(url);
        __audioCtx.seek(0);
        __audioCtx.play();

      }

    },
    // 停止播放
    stopAudio: function () {

      if (__innerAudioCtx) {

        __innerAudioCtx.stop();

      } else {

        __audioCtx.pause();

      }

    }

  }

  module.exports.manager = manager;

})();